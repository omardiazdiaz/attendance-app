CREATE OR REPLACE PACKAGE BODY BANINST1.BWSKUTILS
IS
   /******************************************************************************
      NAME:       BWSKUTILS
      PURPOSE:    utils for some PRCC appilcations.
                 --Attendance Application

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        01/12/2018     odiaz         1. Created this package body.
   ******************************************************************************/


     /* Global type and variable declarations for package */

  




-----------------------------------------------------------------------------
-- Cursors Section
-----------------------------------------------------------------------------
    
    CURSOR cu_terms
    IS
    SELECT	gtvsdax_external_code term_code,gtvsdax_desc term_desc,gtvsdax_external_code external_code
      FROM	GENERAL.GTVSDAX
     WHERE	gtvsdax_internal_code = 'SSBALUMNI'
       AND  gtvsdax_internal_code_group = 'ACTIVE TERM'
     ORDER BY gtvsdax_external_code	
    ;
    
    
    
 
-----------------------------------------------------------------------------
-- Fully defined subprograms specified in package
-----------------------------------------------------------------------------

    FUNCTION f_validTerm(term_in IN VARCHAR2)
        RETURN BOOLEAN
    IS
        lv_answer		BOOLEAN DEFAULT FALSE;
    BEGIN
        IF term_in IS NULL
        THEN
            RETURN FALSE;
        end if;

        FOR lv_rec IN cu_terms
        LOOP
            IF lv_rec.term_code = term_in
            THEN
                lv_answer := TRUE;
            END IF;
        END LOOP;
	
        RETURN lv_answer;
    END f_validTerm;
    
    
    
   


    /*Basic HTML Page*/
    PROCEDURE P_BasicHTML
      IS
      BEGIN
        IF NOT twbkwbis.f_validuser (global_pidm)
        THEN   
            RETURN;
        END IF;  
        HTP.htmlOpen;
        HTP.headOpen;
        HTP.title('Test Application');
        HTP.header(1, 'Test Application');
        HTP.headClose;
        HTP.bodyOpen;
        HTP.bodyClose;
        HTP.htmlClose;
    END P_BasicHTML;
    
    
    PROCEDURE p_tableRowOpen
    IS BEGIN
        twbkfrmt.p_tableRowOpen;
    END p_tableRowOpen;
    
    PROCEDURE p_tableRowClose
    IS BEGIN
        twbkfrmt.p_tableRowClose;
    END p_tableRowClose;
    
    PROCEDURE p_tableDataLabel(label_in IN VARCHAR2, colspan_in IN VARCHAR2 DEFAULT NULL)
    IS BEGIN
        twbkfrmt.p_tableDataLabel(label_in, ccolspan => colspan_in);
    END p_tableDataLabel;
    
    PROCEDURE p_tableDataOpen
    IS BEGIN
        twbkfrmt.p_tableDataOpen;
    END p_tableDataOpen;
    
    PROCEDURE p_tableDataClose
    IS BEGIN
        twbkfrmt.p_tableDataClose;
    END p_tableDataClose;
    
    PROCEDURE p_tableOpen(type_in IN VARCHAR2 DEFAULT NULL)
    IS BEGIN
        twbkfrmt.p_tableOpen(type_in);
    END p_tableOpen;
    
    PROCEDURE p_dispHeaderLabel(   label_in      IN VARCHAR2,
                            	   required_in   IN VARCHAR2 DEFAULT NULL)
    IS
    BEGIN
        BWSKUTILS.p_tableRowOpen();
        BWSKUTILS.p_tableDataLabel(label_in, '3');
        BWSKUTILS.p_tableRowClose;
    END p_dispHeaderLabel;
    
    PROCEDURE p_tableData(text_in IN VARCHAR2, colspan_in IN VARCHAR2 DEFAULT NULL)
    IS
    BEGIN
        twbkfrmt.p_tableData(cvalue => text_in, ccolspan => colspan_in);
    END p_tableData;
    
    PROCEDURE p_tableClose
    IS BEGIN
        twbkfrmt.p_tableClose;
    END p_tableClose;
    
    PROCEDURE p_formOpen(doc_name_in IN VARCHAR2, type_in IN VARCHAR2,form_attributes IN VARCHAR2 DEFAULT NULL)
    IS BEGIN
        twbkfrmt.p_formOpen(doc_name_in, type_in,cattributes=>form_attributes);
    END p_formOpen;
    
    PROCEDURE p_printText(text_in in varchar2)
    IS 
    BEGIN
        twbkfrmt.p_printText(text_in);
    END p_printText;
    
    PROCEDURE p_printError(msg_in IN VARCHAR2)
	IS 
	BEGIN
		IF msg_in IS NOT NULL
		THEN
		twbkfrmt.p_printmessage ( '<div style="color:#800000;">' ||  msg_in || '</div>', 'ERROR');
		END IF;
	END p_printError;
    
    
    PROCEDURE p_printInfo(msg_in IN VARCHAR2)
	IS 
	BEGIN
		IF msg_in IS NOT NULL
		THEN
		twbkfrmt.p_printmessage ( '<div style="color:#300000;">' ||  msg_in || '</div>', 'ERROR');
		END IF;
	END p_printInfo;
    
    
    PROCEDURE p_inputNumber(label_in IN VARCHAR2, field_in IN VARCHAR2
        , size_in IN VARCHAR2, maxlen_in IN VARCHAR2, value_in IN VARCHAR2 DEFAULT NULL
        , required_in IN VARCHAR2 DEFAULT NULL)
        -- Add a label and text box
    IS
    BEGIN
        BWSKUTILS.p_tableRowOpen();
        IF required_in IS NULL
        THEN
            BWSKUTILS.p_tableData(' ');
        ELSE
            BWSKUTILS.p_tableDataLabel('*');
        END IF;

        BWSKUTILS.p_tableDataLabel(label_in);
        BWSKUTILS.p_tableDataOpen;
        htp.p(
            '<INPUT TYPE="text" onkeyup="this.value=this.value.replace(/[^\d]/,'''')" NAME="' || field_in 
            || '" SIZE="' || size_in 
            || '" MAXLENGTH="' || maxlen_in || '"> '
        );
        BWSKUTILS.p_tableDataClose;
        BWSKUTILS.p_tableRowClose;
    END p_inputNumber;
    
    
    PROCEDURE p_dispLabelAndText(label_in IN VARCHAR2, text_in IN VARCHAR2, required_in IN VARCHAR2 DEFAULT NULL)
        -- Display label and text in a data entry table
    IS
    BEGIN
        BWSKUTILS.p_tableRowOpen();
        IF required_in IS NULL
        THEN
            BWSKUTILS.p_tableData(' ');
        ELSE
            BWSKUTILS.p_tableDataLabel('*');
        END IF;
        BWSKUTILS.p_tableDataLabel(label_in);
        BWSKUTILS.p_tableData(text_in);
        BWSKUTILS.p_tableRowClose;
    END p_dispLabelAndText;
    
    
    
    PROCEDURE p_dispSectionTitle(title_in IN VARCHAR2, extraWhiteSpace_in IN VARCHAR2 DEFAULT NULL)
     IS BEGIN
        IF extraWhiteSpace_in IS NOT NULL
        THEN
            htp.p('<p><br><br><p>');
        END IF;
        --
        htp.p('<p><h3>');
        htp.p('<TABLE style="width:100%; background-color:#CCCCCC;" >');
        htp.p('<TR>');
        htp.p('<th>' || title_in || '</th>');
        htp.p('</TR>');
        htp.p('</TABLE>');
        htp.p('</h3><p>');

    END p_dispSectionTitle;
    
   

    
    
    
    
    
    
   
    
    
    
    
    PROCEDURE P_CreatetableHeader (p_column_name   VARCHAR2, nowrap VARCHAR2 DEFAULT 'TRUE')
    IS
    BEGIN
        twbkfrmt.p_tabledataheader (
             G$_NLS.Get ('BWGKOEM1-0052', 'SQL', p_column_name),
             NULL,
             NULL,
             nowrap, --nowrap = TRUE ELSE NULL
             NULL
             );
    END;
    
    
    
END;
/