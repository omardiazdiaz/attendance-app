CREATE OR REPLACE PACKAGE BANINST1.BWSKUTILS IS
/******************************************************************************
   NAME:       BWSKUTILS
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        01/12/2018      Odiaz         1. Created this package.
******************************************************************************/

-- DESCRIPTION:
-- This package contains PROCEDUREs and Functions required for:
-- ATTENDANCE APPLICATION
--
-- DESCRIPTION END
--
  /* Declare externally visible constant,type,cursor,variable and exception. */

-- CONSTANTS


    MESSAGE_LEVEL_INFO          CONSTANT VARCHAR2(4):='INFO';
    MESSAGE_LEVEL_WARNING       CONSTANT VARCHAR2(7):='WARNING';
    MESSAGE_LEVEL_ERROR         CONSTANT VARCHAR2(5):='ERROR';
    MESSAGE_LEVEL_DEBUG         CONSTANT VARCHAR2(5):='DEBUG';



-- VARIABLES
    --GLOBAL
     global_pidm           NUMBER;    


--FUNCTION
    FUNCTION f_validTerm               (   term_in                   IN VARCHAR2) RETURN BOOLEAN;
    
   
    
    
--PROCEDURES
	--Just for testing purposes
    PROCEDURE P_BasicHTML;
    
    PROCEDURE p_tableRowOpen;
    
    PROCEDURE p_tableRowClose;
    
    PROCEDURE p_tableDataLabel         (   label_in                  IN VARCHAR2,
                                           colspan_in                IN VARCHAR2 DEFAULT NULL);
                                           
    PROCEDURE p_tableDataOpen;
    
    PROCEDURE p_tableDataClose;
    
    PROCEDURE p_tableOpen              (   type_in                   IN VARCHAR2 DEFAULT NULL);
    
    PROCEDURE p_dispHeaderLabel        (   label_in                  IN VARCHAR2,
                            	           required_in               IN VARCHAR2 DEFAULT NULL);
    
    PROCEDURE p_tableData               (  text_in                   IN VARCHAR2, 
	                                       colspan_in                IN VARCHAR2 DEFAULT NULL);
                                           
    PROCEDURE p_tableClose;
    
    PROCEDURE p_formOpen               (   doc_name_in               IN VARCHAR2, 
	                                       type_in                   IN VARCHAR2,
                                           form_attributes           IN VARCHAR2 DEFAULT NULL);
    
    PROCEDURE p_printText				(  text_in                   IN VARCHAR2);
    
    PROCEDURE p_printError             (   msg_in                    IN VARCHAR2);

	PROCEDURE p_printInfo              (   msg_in                    IN VARCHAR2);
    
    PROCEDURE p_inputNumber             (  label_in                  IN VARCHAR2, 
	                                       field_in                  IN VARCHAR2, 
										   size_in                   IN VARCHAR2,
										   maxlen_in                 IN VARCHAR2, 
										   value_in                  IN VARCHAR2 DEFAULT NULL, 
										   required_in               IN VARCHAR2 DEFAULT NULL);
                                           
    PROCEDURE p_dispLabelAndText        (  label_in                  IN VARCHAR2, 
	                                       text_in                   IN VARCHAR2,  
										   required_in               IN VARCHAR2 DEFAULT NULL);                                       
                    
    /*
    PROCEDURE P_Insert_Log              (   p_app_name                VARCHAR2,
                                            p_package_name            VARCHAR2,
                                            p_procedure_name 		  VARCHAR2,
										    p_message				  VARCHAR2,
                                            p_message_level           VARCHAR2,
                                            p_session_step_number     IN OUT NUMBER
                                          );
   */
                                          
    PROCEDURE p_dispSectionTitle        (  title_in                  IN VARCHAR2, 
										   extraWhiteSpace_in        IN VARCHAR2 DEFAULT NULL);
                                           
                                   

          
    
      
    
   
    
    PROCEDURE P_CreatetableHeader       (p_column_name              IN VARCHAR2,nowrap VARCHAR2 DEFAULT 'TRUE'); --nowrap = TRUE ELSE NULL
     
    


END BWSKUTILS;
/