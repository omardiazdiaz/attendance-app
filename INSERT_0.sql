
DELETE FROM twgrinfo
  WHERE TWGRINFO_NAME LIKE 'bwskattendanceapp%';
  
 COMMIT;

DELETE FROM TWGRMENU
 WHERE TWGRMENU_URL LIKE 'bwskattendanceapp%';
 
 DELETE FROM TWGRWMRL
 WHERE TWGRWMRL_NAME LIKE 'bwskattendanceapp%';
  
 DELETE FROM TWGBWMNU
 WHERE TWGBWMNU_NAME LIKE 'bwskattendanceapp%';
    
 COMMIT;
 
 --==============================================================================================================================================================
--==========================================================================================================================================================
/* Formatted on 9/18/2014 3:41:03 PM (QP5 v5.267.14150.38599) */
--verified
INSERT INTO TWGBWMNU
     VALUES ('bwskattendanceapp.P_AttendanceApplication',
             'Class Attendance',
             'Class Attendance',
             'Class Attendance',
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             'bmenu.P_FacMainMnu',  
             'Return to Student Menu',
             NULL,
             'Y',
             'STU',
             'Y',
             'N',
             SYSDATE,
             NULL,
             NULL,
             'S',
             NULL,
             NULL,
             'L',  --'L' for Local as oppsed to 'B' for baseline
             NULL,
             'N',
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL);
--verified

INSERT INTO TWGRMENU
     VALUES ('bmenu.P_FacMainMnu',
            (SELECT MAX(TWGRMENU_SEQUENCE)+1 FROM TWGRMENU),
             'Class Attendance',
             'bwskattendanceapp.P_AttendanceApplication',
             NULL,
             NULL,
             'Y',
             'Y',
             'N',
             NULL,
             NULL,
             SYSDATE,
             NULL,
             'L',
             NULL,
             NULL,
             NULL,
             NULL,
             NULL);		 
			 
			 
INSERT INTO TWGRWMRL
     VALUES ('bwskattendanceapp.P_AttendanceApplication',
             'WEBUSER',
             SYSDATE,
             'L',
             NULL,
             NULL,
             NULL,
             NULL,
             NULL);

INSERT INTO TWGRWMRL
     VALUES ('bwskattendanceapp.P_AttendanceApplication',
             'WTAILORADMIN',
             SYSDATE,
             'L',
             NULL,
             NULL,
             NULL,
             NULL,
             NULL);
   

 INSERT INTO TWGBWMNU
     VALUES ('bwskattendanceapp.P_AttendanceDetail',
             'Attendance Detail',
             'Attendance Detail',
             'Attendance Detail',
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             'bmenu.P_FacMainMnu',  
             'Return to Student Menu',
             NULL,
             'Y',
             'STU',
             'Y',
             'N',
             SYSDATE,
             NULL,
             NULL,
             'S',
             NULL,
             NULL,
             'L',  --'L' for Local as oppsed to 'B' for baseline
             NULL,
             'N',
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL);

   
 INSERT INTO TWGRWMRL
     VALUES ('bwskattendanceapp.P_AttendanceDetail',
             'WEBUSER',
             SYSDATE,
             'L',
             NULL,
             NULL,
             NULL,
             NULL,
             NULL);
	 
			 
INSERT INTO TWGBWMNU
     VALUES ('bwskattendanceapp.P_AttendanceRoster',
             'Attendance Roster',
             'Attendance Roster',
             'Attendance Roster',
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             'bmenu.P_FacMainMnu',  
             'Return to Student Menu',
             NULL,
             'Y',
             'STU',
             'Y',
             'N',
             SYSDATE,
             NULL,
             NULL,
             'S',
             NULL,
             NULL,
             'L',  --'L' for Local as oppsed to 'B' for baseline
             NULL,
             'N',
             NULL,
             NULL,
             NULL,
             NULL,
             NULL,
             NULL);

 INSERT INTO TWGRMENU
     VALUES ('bmenu.P_FacMainMnu',
             (SELECT MAX(TWGRMENU_SEQUENCE)+1 FROM TWGRMENU),
             'Attendance Roster',
             'bwskattendanceapp.P_AttendanceRoster',
             NULL,
             NULL,
             'Y',
             'Y',
             'N',
             NULL,
             NULL,
             SYSDATE,
             NULL,
             'L',
             NULL,
             NULL,
             NULL,
             NULL,
             NULL);
			 
 INSERT INTO TWGRWMRL
     VALUES ('bwskattendanceapp.P_AttendanceRoster',
             'WEBUSER',
             SYSDATE,
             'L',
             NULL,
             NULL,
             NULL,
             NULL,
             NULL);

INSERT INTO TWGRWMRL
     VALUES ('bwskattendanceapp.P_AttendanceRoster',
             'WTAILORADMIN',
             SYSDATE,
             'L',
             NULL,
             NULL,
             NULL,
             NULL,
             NULL);

			 
INSERT INTO TWGRWMRL
     VALUES ('bwskattendanceapp.P_ShowRosterPage',
             'WEBUSER',
             SYSDATE,
             'L',
             NULL,
             NULL,
             NULL,
             NULL,
             NULL);


Insert into TWGRINFO (TWGRINFO_NAME,TWGRINFO_SEQUENCE,TWGRINFO_LABEL,TWGRINFO_IMAGE,TWGRINFO_TEXT,TWGRINFO_COMMENT,TWGRINFO_ACTIVITY_DATE,TWGRINFO_SOURCE_IND,TWGRINFO_SURROGATE_ID,TWGRINFO_VERSION,TWGRINFO_USER_ID,TWGRINFO_DATA_ORIGIN,TWGRINFO_VPDI_CODE) values ('bwskattendanceapp.P_AttendanceApplication',1,'CONTACT','Info','If you have any issues with Attendance, please contact the Tevin Noel in the Office of Information Technology at 601-403-1149.','contact',to_date('08-FEB-18','DD-MON-RR'),'L',null,null,null,null,null);
Insert into TWGRINFO (TWGRINFO_NAME,TWGRINFO_SEQUENCE,TWGRINFO_LABEL,TWGRINFO_IMAGE,TWGRINFO_TEXT,TWGRINFO_COMMENT,TWGRINFO_ACTIVITY_DATE,TWGRINFO_SOURCE_IND,TWGRINFO_SURROGATE_ID,TWGRINFO_VERSION,TWGRINFO_USER_ID,TWGRINFO_DATA_ORIGIN,TWGRINFO_VPDI_CODE) values ('bwskattendanceapp.P_AttendanceApplication',1,'NOTICE','Info','Please select the date for which you would like to enter class attendance.',null,to_date('08-FEB-18','DD-MON-RR'),'L',null,null,null,null,null);
Insert into TWGRINFO (TWGRINFO_NAME,TWGRINFO_SEQUENCE,TWGRINFO_LABEL,TWGRINFO_IMAGE,TWGRINFO_TEXT,TWGRINFO_COMMENT,TWGRINFO_ACTIVITY_DATE,TWGRINFO_SOURCE_IND,TWGRINFO_SURROGATE_ID,TWGRINFO_VERSION,TWGRINFO_USER_ID,TWGRINFO_DATA_ORIGIN,TWGRINFO_VPDI_CODE) values ('bwskattendanceapp.P_AttendanceRoster',1,'CONTACT','Info','If you have any issues with Attendance, please contact the Tevin Noel in the Office of Information Technology at 601-403-1149.',null,to_date('08-FEB-18','DD-MON-RR'),'L',null,null,null,null,null);
Insert into TWGRINFO (TWGRINFO_NAME,TWGRINFO_SEQUENCE,TWGRINFO_LABEL,TWGRINFO_IMAGE,TWGRINFO_TEXT,TWGRINFO_COMMENT,TWGRINFO_ACTIVITY_DATE,TWGRINFO_SOURCE_IND,TWGRINFO_SURROGATE_ID,TWGRINFO_VERSION,TWGRINFO_USER_ID,TWGRINFO_DATA_ORIGIN,TWGRINFO_VPDI_CODE) values ('bwskattendanceapp.P_AttendanceRoster',1,'NOTICE','Info','If you have missing attendance data or need to change student attendance for specific date(s), please go back to the Class Attendance page and enter class attendance for the specific date(s).',null,to_date('08-FEB-18','DD-MON-RR'),'L',null,null,null,null,null);
COMMIT;
 