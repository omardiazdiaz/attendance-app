DROP TABLE "PRCCUSR"."SZRATTLDA" ;
--------------------------------------------------------
--  DDL for Table SZRATTLDA
--------------------------------------------------------

  CREATE TABLE "PRCCUSR"."SZRATTLDA" 
   ("SZRATTLDA_SPRIDEN_ID" VARCHAR2(22 BYTE), 
	"SZRATTLDA_TERM_CODE" VARCHAR2(6 BYTE), 
	"SZRATTLDA_CRN" VARCHAR2(5 BYTE), 
	"SZRATTLDA_ATTENDANCE_DATE" DATE, 
	"SZRATTLDA_INSERT_DATE" DATE,
	"SZRATTLDA_INSERTED_BY" VARCHAR2(30),
	"SZRATTLDA_UPDATE_DATE" DATE,
	"SZRATTLDA_UPDATED_BY" VARCHAR2(30)
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "DEVELOPMENT" ;

   COMMENT ON COLUMN "PRCCUSR"."SZRATTLDA"."SZRATTLDA_TERM_CODE" IS 'This field identifies the term code referenced in the Catalog, Recruiting, Admissions, Gen. Student, Registration, Student Billing and Acad. Hist. Modules. Reqd. value: 999999 - End of Time.';
   COMMENT ON COLUMN "PRCCUSR"."SZRATTLDA"."SZRATTLDA_CRN" IS 'This field is not displayed on the form (page 0).  It will display the Course Reference Number (CRN) assigned to this course section when it was initially added.';
--------------------------------------------------------
--  Constraints for Table SZRATTLDA
--------------------------------------------------------

  ALTER TABLE "PRCCUSR"."SZRATTLDA" MODIFY ("SZRATTLDA_SPRIDEN_ID" NOT NULL ENABLE);
  ALTER TABLE "PRCCUSR"."SZRATTLDA" MODIFY ("SZRATTLDA_TERM_CODE" NOT NULL ENABLE);
  ALTER TABLE "PRCCUSR"."SZRATTLDA" MODIFY ("SZRATTLDA_CRN" NOT NULL ENABLE);
   ALTER TABLE "PRCCUSR"."SZRATTLDA" MODIFY ("SZRATTLDA_INSERT_DATE" NOT NULL ENABLE);
  
  CREATE OR REPLACE PUBLIC SYNONYM SZRATTLDA FOR PRCCUSR.SZRATTLDA;
  GRANT SELECT, DELETE, INSERT, UPDATE ON "PRCCUSR"."SZRATTLDA" to "PUBLIC" ;