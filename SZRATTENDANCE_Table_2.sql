  DROP TABLE "PRCCUSR"."SZRATTENDANCE" ;
--------------------------------------------------------
--  DDL for Table SZRATTENDANCE
--------------------------------------------------------

  CREATE TABLE "PRCCUSR"."SZRATTENDANCE" 
   ("SZRATTENDANCE_SPRIDEN_ID"            VARCHAR2(22 BYTE), 
	"SZRATTENDANCE_PIDM"                  NUMBER(8)NOT NULL,     
	"SZRATTENDANCE_TERM_CODE"             VARCHAR2(6 BYTE), 
	"SZRATTENDANCE_CRN"                   VARCHAR2(5 BYTE), 
	"SZRATTENDANCE_ATTENDANCE_DATE"       DATE, 
	"SZRATTENDANCE_REGULAR_MEETING"       VARCHAR2(1) NOT NULL,
	"SZRATTENDANCE_ATTENDED"              VARCHAR2(1)NOT NULL, 
	"SZRATTENDANCE_CREATION_DATE"         DATE,
	"SZRATTENDANCE_UPDATE_DATE"           DATE
   ) SEGMENT CREATION DEFERRED 
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 
 NOCOMPRESS LOGGING
  TABLESPACE "DEVELOPMENT" ;

   COMMENT ON COLUMN "PRCCUSR"."SZRATTENDANCE"."SZRATTENDANCE_TERM_CODE" IS 'This field identifies the term code referenced in the Catalog, Recruiting, Admissions, Gen. Student, Registration, Student Billing and Acad. Hist. Modules. Reqd. value: 999999 - End of Time.';
   COMMENT ON COLUMN "PRCCUSR"."SZRATTENDANCE"."SZRATTENDANCE_CRN" IS 'This field is not displayed on the form (page 0).  It will display the Course Reference Number (CRN) assigned to this course section when it was initially added.';
--------------------------------------------------------
--  Constraints for Table SZRATTENDANCE
--------------------------------------------------------

  ALTER TABLE "PRCCUSR"."SZRATTENDANCE" MODIFY ("SZRATTENDANCE_SPRIDEN_ID" NOT NULL ENABLE);
  ALTER TABLE "PRCCUSR"."SZRATTENDANCE" MODIFY ("SZRATTENDANCE_TERM_CODE" NOT NULL ENABLE);
  ALTER TABLE "PRCCUSR"."SZRATTENDANCE" MODIFY ("SZRATTENDANCE_CRN" NOT NULL ENABLE);
  ALTER TABLE "PRCCUSR"."SZRATTENDANCE" MODIFY ("SZRATTENDANCE_ATTENDANCE_DATE" NOT NULL ENABLE);
  ALTER TABLE "PRCCUSR"."SZRATTENDANCE" MODIFY ("SZRATTENDANCE_CREATION_DATE" NOT NULL ENABLE);
  
  CREATE OR REPLACE PUBLIC SYNONYM SZRATTENDANCE FOR PRCCUSR.SZRATTENDANCE;
  GRANT SELECT, DELETE, INSERT, UPDATE ON "PRCCUSR"."SZRATTENDANCE" to "PUBLIC" ;
