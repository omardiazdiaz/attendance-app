CREATE OR REPLACE PACKAGE BODY BANINST1.bwskattendanceapp
IS
   /******************************************************************************
      NAME:       bwskattendanceapp
      PURPOSE:    Attendance Applications (Pearl River Community College)

      REVISIONS:
      Ver        Date        Author           Description
      ---------  ----------  ---------------  ------------------------------------
      1.0        01/29/2018     odiaz         1. Created this package body.
      
   ******************************************************************************/


     /* Global type and variable declarations for package */


    gv_curr_release		             VARCHAR2(30) DEFAULT '8.8 (v 1.0.02)';
    gv_doc_attendance_application    VARCHAR2(50) DEFAULT 'bwskattendanceapp.P_AttendanceApplication';
    gv_doc_attendance_roster         VARCHAR2(50) DEFAULT 'bwskattendanceapp.P_AttendanceRoster';
    gv_app_name                      VARCHAR2(50) DEFAULT 'attendanceapp';
    gv_attendance_detail             VARCHAR2(50) DEFAULT 'bwskattendanceapp.P_AttendanceDetail';
    local_term_code                  VARCHAR2(20) DEFAULT NULL;
    gv_application_log_level         NUMBER:=MESSAGE_LEVEL_INFO;
    gv_local_style                   VARCHAR2(400):='style="background-color: #605e5e; color:white; border-radius: 3px; font-size: 13px; font-family: normal 100% Arial, Helvetica, sans-serif;  padding: 4px 16px 4px 16px; border: solid #1f628d 0px; text-shadow: 0px 0px 0px #f5f0f5; cursor: pointer; "';
    
   



-----------------------------------------------------------------------------
-- Cursors Section
-----------------------------------------------------------------------------

    --Term Code to show in the application
    CURSOR cu_terms
    IS
    SELECT	gtvsdax_external_code term_code,gtvsdax_desc term_desc,gtvsdax_external_code external_code
      FROM	GENERAL.GTVSDAX
     WHERE	gtvsdax_internal_code = 'SSBALUMNI'
       AND  gtvsdax_internal_code_group = 'ACTIVE TERM'
     ORDER BY gtvsdax_external_code	
    ;
    

    CURSOR cu_terms_by_spriden_id
    IS
    SELECT DISTINCT sfrstcr_term_code term_code, STVTERM.STVTERM_DESC term_desc
       FROM sfrstcr, ssbsect, scbcrse a, ssrmeet, sirasgn, spriden i, spriden s,STVTERM
      WHERE sfrstcr_rsts_code in ('RE','AU')
        AND EXTRACT (YEAR FROM  STVTERM.STVTERM_start_date)>=EXTRACT (YEAR FROM SYSDATE)-1 
        --AND sfrstcr_term_code = '201801'
        AND sirasgn_pidm = global_pidm --GLOBAL_PIDM
        AND STVTERM.STVTERM_CODE=ssbsect_term_code
        --AND sfrstcr_crn='15082'
        AND sfrstcr_term_code = ssbsect_term_code
        AND sfrstcr_crn = ssbsect_crn
        AND ssbsect_subj_code = a.scbcrse_subj_code
        AND ssbsect_crse_numb = a.scbcrse_crse_numb
        AND a.scbcrse_eff_term = (SELECT MAX(scbcrse_eff_term)
                                    FROM scbcrse b
                                    WHERE b.scbcrse_subj_code = a.scbcrse_subj_code
                                      AND b.scbcrse_crse_numb = a.scbcrse_crse_numb)
        AND ssrmeet_term_code = sfrstcr_term_code
        AND ssrmeet_crn = sfrstcr_crn
        AND sirasgn_term_code = sfrstcr_term_code
        AND sirasgn_crn = sfrstcr_crn
        --AND sirasgn_primary_ind = 'Y'  Asked by Tevin Noel to allow any non primary teacher select the course
        AND i.spriden_pidm(+) = sirasgn_pidm
        AND i.spriden_change_ind is null
        AND s.spriden_pidm(+) = sfrstcr_pidm
        AND s.spriden_change_ind is null
        ORDER BY sfrstcr_term_code DESC;
        
     
        
        
-----------------------------------------------------------------------------
-- Fully defined subprograms specified in package
-----------------------------------------------------------------------------
    
    PROCEDURE P_AttendanceRoster (p_term_code                     IN general.gtvsdax.gtvsdax_external_code%TYPE DEFAULT NULL,
                                  p_crn                           IN PRCCUSR.SZRATTENDANCE.SZRATTENDANCE_CRN%TYPE DEFAULT NULL,
                                  p_day                           IN NUMBER DEFAULT NULL,
                                  p_month                         IN NUMBER DEFAULT NULL,
                                  p_year                          IN NUMBER DEFAULT NULL,
                                  p_submit_button                 IN VARCHAR2 DEFAULT NULL,
                                  p_save_roster                   IN VARCHAR2 DEFAULT NULL,
                                  P_HIDE_COLUMNS_NUMBER           IN VARCHAR2 DEFAULT NULL,
                                  P_HIDE_COLUMNS_WEEK             IN VARCHAR2 DEFAULT NULL,
                                  p_LAD                           IN VARCHAR2 DEFAULT NULL,
                                  p_list_for_update               IN VARCHAR2 DEFAULT NULL,
                                  P_FIRST_REGULAR_MEETING_DATE    IN VARCHAR2 DEFAULT NULL)
    IS
    BEGIN
        IF NOT twbkwbis.f_validuser (global_pidm)
        THEN   
            RETURN;
        END IF;   
       
        P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','P_AttendanceRoster',global_pidm ||'-Entering Method term_code:' || p_term_code || ' crn: ' || p_crn || ' p_submit_button: ' || p_submit_button,MESSAGE_LEVEL_INFO,gv_session_step_number) ;
        IF p_submit_button = 'Submit'
        THEN
            P_ShowRosterPage(p_term_code,p_crn,FALSE);
        ELSIF p_save_roster='Submit'  --Saving LDA fields
        THEN
            P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','P_AttendanceRoster',global_pidm ||'-Saving LDA values. p_list_for_update:' || p_list_for_update ,MESSAGE_LEVEL_DEBUG,gv_session_step_number) ;
            p_update_LDA(p_list_for_update,p_term_code,p_crn);
            P_ShowRosterPage(p_term_code,p_crn,TRUE);
        ELSE
            IF NOT f_local_validTerm(p_term_code)
            THEN
                P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','P_AttendanceRoster',global_pidm ||'-Displaying Term Code Drop Down List',MESSAGE_LEVEL_DEBUG,gv_session_step_number) ;
                p_displayBoxesRoster(p_form_origin=>gv_doc_attendance_roster);
            ELSE
      
                IF p_crn IS NULL
                THEN
                    P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','P_AttendanceRoster',global_pidm ||'-Displaying CRN Drop Down List term_code:' || p_term_code,MESSAGE_LEVEL_DEBUG,gv_session_step_number) ;
                    p_displayBoxesRoster(p_term_code,p_form_origin=>gv_doc_attendance_roster);
                END IF;
            END IF;
        END IF;
                
    END P_AttendanceRoster;  
    
    
    PROCEDURE P_ShowRosterPage (p_term_code      IN general.gtvsdax.gtvsdax_external_code%TYPE,
                                p_crn            IN PRCCUSR.SZRATTENDANCE.SZRATTENDANCE_CRN%TYPE,
                                p_after_update   IN BOOLEAN )
    IS 
        CURSOR dates_cursor
        IS
            SELECT  DISTINCT SZRATTENDANCE_ATTENDANCE_DATE att_date, SZRATTENDANCE_REGULAR_MEETING
              FROM  PRCCUSR.SZRATTENDANCE
             WHERE SZRATTENDANCE_TERM_CODE=p_term_code
               AND SZRATTENDANCE_CRN=p_crn
              -- AND SZRATTENDANCE_REGULAR_MEETING=REGULAR_MEETING
             ORDER BY ATT_DATE;
               
        CURSOR students_cursor
        IS 
            --SELECT DISTINCT ATT.SZRATTENDANCE_SPRIDEN_ID,TO_CHAR(LDA.SZRATTLDA_ATTENDANCE_DATE,'MM/DD/YYYY'), spriden_first_name ||	decode(spriden_mi, null, ' ', ' ' || spriden_mi || ', ')||spriden_last_name FULL_NAME,SPRIDEN_PIDM
            SELECT DISTINCT ATT.SZRATTENDANCE_SPRIDEN_ID,TO_CHAR(LDA.SZRATTLDA_ATTENDANCE_DATE,'MM/DD/YYYY'), spriden_first_name ||' '||decode(spriden_mi, null, '', '' || spriden_mi || ' ') || spriden_last_name FULL_NAME,SPRIDEN_PIDM  
              FROM PRCCUSR.SZRATTENDANCE ATT, SATURN.SPRIDEN SP, PRCCUSR.SZRATTLDA LDA
             WHERE SP.SPRIDEN_ID               = ATT.SZRATTENDANCE_SPRIDEN_ID
               AND LDA.SZRATTLDA_SPRIDEN_ID    = ATT.SZRATTENDANCE_SPRIDEN_ID
               AND ATT.SZRATTENDANCE_TERM_CODE = p_term_code
               AND ATT.SZRATTENDANCE_CRN       = p_crn
               AND ATT.SZRATTENDANCE_TERM_CODE = LDA.SZRATTLDA_TERM_CODE
               AND ATT.SZRATTENDANCE_CRN       = LDA.SZRATTLDA_CRN 
               --AND ATT.SZRATTENDANCE_REGULAR_MEETING=REGULAR_MEETING
               AND SPRIDEN_CHANGE_IND          IS NULL
             ORDER BY FULL_NAME;
        
       -- attendance_rec  PRCCUSR.SZRATTENDANCE%ROWTYPE;
        attendent_value                         VARCHAR2(1);
        field_in                                VARCHAR2(20);
        has_data                                BOOLEAN;
        enabled_property                        VARCHAR2(15);
        l_first_regular_meeting_date            VARCHAR2(10);
    BEGIN
        
        
        bwckfrmt.p_open_doc(gv_doc_attendance_roster);
        IF(p_after_update=TRUE)
        THEN
            twbkfrmt.p_printmessage ('Data was saved succesfully.', 3);
            twbkfrmt.p_paragraph(3);
        END IF;
        BWSKUTILS.p_formOpen(gv_doc_attendance_roster, 'POST','id="roster_form" ');  --, 'onsubmit="showAlert();"' onsubmit="return getInputTextValues(event);"
        
        
            
            htp.script('function hideColumns(option){
                    //alert(option);
                    if(option==0){
                        start=1;
                        end  =100;
                    }
                    if(option==1){
                        start=1;
                        end  =10;
                    }
                    if(option==2){
                        start=10;
                        end  =20;
                    }
                    if(option==3){
                        start=20;
                        end  =30;
                    }
                    if(option==4){
                        start=30;
                        end  =40;
                    }
                    if(option==5){
                        start=40;
                        end  =50;
                    }
                    if(option==6){
                        start=50;
                        end  =60;
                    }
                    if(option==7){
                        start=60;
                        end  =70;
                    }
                    if(option==8){
                        start=70;
                        end  =80;
                    }
                    if(option==9){
                        start=80;
                        end  =90;
                    }
                    if(option==10){
                        start=90;
                        end  =100;
                    }
                    var rows=document.getElementById(''table_attendance'').getElementsByTagName(''tr'');
                    var columns =document.getElementById(''table_attendance'').rows[0].cells.length;
                    //alert(columns);
                    for(i=1;i<columns;i++){
                        for(j=0;j<rows.length;j++){
                            rows[j].getElementsByTagName(''td'')[i].style.display=''none'';
                        }
                    }
                    
                    for(i=start;i<end;i++){
                        for(j=0;j<rows.length;j++){
                            rows[j].getElementsByTagName(''td'')[i].style.display='''';
                        }
                    }                                                
                   }','JavaScript');
                   
            htp.script('// Returns an array with values of the INPUT type text of the from looking for the LDA''S
                    function getInputTextValues(event) {
                        var frm=document.getElementById("roster_form");
                        var selectinputtext = [];                             // array that will store the value of inputtext      
                        var inpfields = frm.getElementsByTagName(''input'');  // gets all the input tags in frm, and their values
                        var nr_inpfields = inpfields.length;
                        // traverse the inpfields elements, and adds the value of textboxes in selectinputtext array
                        var str_id="";
                        var str_date="";
                        var isInvalid = false;
                                   
                                             
                        for(var i=0; i<nr_inpfields; i++) {
                            if(inpfields[i].type == ''text'' && inpfields[i].value != '''') {
                                if(isValidDate(inpfields[i].value)==true){
                                    str_id=inpfields[i].id.concat(''='').concat(inpfields[i].value);
                                    selectinputtext.push(str_id);
                                    inpfields[i].style.backgroundColor="";
                                }else{
                                    isInvalid=true;
                                    inpfields[i].style.backgroundColor="#ffff99";
                                    event.preventDefault(); 
                                }
                                   
                            }
                        }
                       
                        if(isInvalid == true){
                            alert(''Invalid entries highligted in Yellow'');
                            event.preventDefault(); 
                        }else{
                            document.getElementById("p_list_for_update").value = selectinputtext; //hidden object
                            document.getElementById("p_save_roster").value = ''Submit''; //hidden object
                                                        
                            if (confirm("Are you sure you want to save changes?")) {
                                frm.submit();
                            }else{
                                return;
                            }                           
                        }
                 
                 
                    return selectinputtext;
                    }','JavaScript');
                    
            htp.script('// Returns an array with values of the INPUT type text of the form
                    function getInputText() {
                        alert(''hi there'');
                        var x=document.getElementById("roster_form").action;
                        //alert(x);
                        event.preventDefault();
                    
                    }','JavaScript');
                    
            htp.script('// validate the input date for every LDA in the table
                   function isValidDate(date){
                        var temp=date.split(''/'');
                        //alert (temp);
                        //var d = new Date(2018+''/''+12+''/''+1);
                        var d = new Date(temp[2]+''/''+temp[0]+''/''+temp[1]);
                        //alert (d);
                        var isValid = (d && d.getMonth()+1 ==temp[0] && d.getDate()==Number(temp[1]) && d.getFullYear()== Number(temp[2]) );
                        //alert (isValid);
                        return isValid;
                    }','JavaScript');
                    
            htp.script('function hideColumnsByWeek(option){
                            var date; //type Date 
                            var firstDayNumber=0; //1 Monday 2 Tuesday 3 ...
                            var countOcurrenceDay=0;  //int
                            var columnLimitNumber;  //Last column to show
                            var columnStartLimitNumber;  //First column to show
                                            
                            var rows=document.getElementById(''table_attendance'').getElementsByTagName(''tr'');
                            var columns =document.getElementById(''table_attendance'').rows[0].cells.length; 
                            
                            //===========================================
                            //Show all the rows
                            if(option==0){
                                for(i=0;i<columns;i++){
                                    for(j=0;j<rows.length;j++){
                                       rows[j].cells[i].style.display='''';
                                    }
                                }  
                                return;
                            }
                            
                            //===========================================
                            //date = new Date(stringToDate(rows[0].cells[3].innerHTML,"mm-dd-yyyy","-"));  
                            date = new Date(stringToDate(document.getElementById(''p_first_regular_meeting_date'').value,"mm-dd-yyyy","-"));
                            //alert(document.getElementById(''p_first_regular_meeting_date'').value);
                            
                            firstDayNumber = date.getDay();
                            //alert(date);
                            
                            if(option==1){
                                columnStartLimitNumber=0;
                                countOcurrenceDay=0;
                                limitCount=5; // Limit of initial days to found
                                for(i=3;i<columns;i++){
                                    date = new Date(stringToDate(rows[0].cells[i].innerHTML,"mm-dd-yyyy","-")); //Searching in the header 
                                    if (date.getDay() == firstDayNumber){
                                        countOcurrenceDay = countOcurrenceDay + 1;
                                        if (countOcurrenceDay == limitCount ){
                                            columnLimitNumber = i;
                                        }
                                    }
                                }
                                if (countOcurrenceDay < limitCount ){
                                    columnLimitNumber = columns;
                                }
                            }
                            
                            if(option==2){
                                countOcurrenceDay=0;
                                startCount = 5;
                                limitCount=9; // Limit of initial days to found
                            }		
                                
                            if(option==3){
                                countOcurrenceDay=0;
                                startCount = 9;
                                limitCount=13; // Limit of initial days to found
                            }

                            if(option==4){
                                countOcurrenceDay=0;
                                startCount = 13;
                                limitCount=17; // Limit of initial days to found
                            }	
                            
                            if(option==5){
                                countOcurrenceDay=0;
                                startCount = 17;
                                limitCount=21; // Limit of initial days to found
                            }
                            
                            //===========================================
	
                            if( option==2 || option==3 || option==4 || option==5){
                                for(i=0;i<columns;i++){
                                    date = new Date(stringToDate(rows[0].cells[i].innerHTML,"mm-dd-yyyy","-")); //Searching in the header 
                                    if (date.getDay() == firstDayNumber){
                                        countOcurrenceDay = countOcurrenceDay + 1;
                                        if (countOcurrenceDay == limitCount ){
                                            columnLimitNumber = i;
                                        }
                                        if(countOcurrenceDay==startCount){
                                            columnStartLimitNumber=i;
                                        }
                                    }
                                }
                                if (countOcurrenceDay < limitCount ){
                                    columnLimitNumber = columns;
                                }
                            }
                            //===========================================	
                            
                            //Hiding all the table
                            for(i=3;i<columns;i++){
                                for(j=0;j<rows.length;j++){
                                   rows[j].cells[i].style.display=''none'';
                                }
                            }    
	
                            //Showing the week range
                            for(i=columnStartLimitNumber;i<columnLimitNumber;i++){
                                for(j=0;j<rows.length;j++){
                                   rows[j].cells[i].style.display='''';
                                }
                            }  
                            //alert(columns); 
                        }','JavaScript');        
            
            htp.script('// return the formatted date
                   function stringToDate(_date,_format,_delimiter){
                        var formatLowerCase=_format.toLowerCase();
                        var formatItems=formatLowerCase.split(_delimiter);
                        var dateItems=_date.split(_delimiter);
                        var monthIndex=formatItems.indexOf("mm");
                        var dayIndex=formatItems.indexOf("dd");
                        var yearIndex=formatItems.indexOf("yyyy");
                        var month=parseInt(dateItems[monthIndex]);
                        month-=1;
                        var formatedDate = new Date(dateItems[yearIndex],month,dateItems[dayIndex]);
                        return formatedDate;		
                        /*
                        stringToDate("17/9/2014","dd/MM/yyyy","/");
                        stringToDate("9/17/2014","mm/dd/yyyy","/")
                        stringToDate("9-17-2014","mm-dd-yyyy","-")
                        */
            }','JavaScript');
            
            htp.script('
                        function exportTableToCSV(event) {
                            
                            filename ="Roster.csv";
                           
                            var rows=document.getElementById(''table_attendance'').getElementsByTagName(''tr'');
                            var columns =document.getElementById(''table_attendance'').rows[0].cells; 
                            var csv = [];
                                                       
                            for (var rowNum = 0; rowNum < rows.length; rowNum++) {
                                var row = [];
                                for (var columNum = 0; columNum < columns.length; columNum++) {
                                    if(columNum==1 && rowNum>0){      //This is for extract the LDA
                                        var text = rows[rowNum].cells[columNum].getElementsByTagName("input")[0].value;
                                        row.push(trim(text));   
                                    }else if (columNum==2 && rowNum>0){  //this is for extract ADM Status
                                            var text=rows[rowNum].cells[columNum].innerText;
                                            if(text.length>2){
                                                row.push(trim(text)); 
                                            }else{
                                                row.push('''');
                                            }
                                    }else{
                                            var text=rows[rowNum].cells[columNum].innerText;
                                            if(text.length==1){
                                                if(text==''Y'' || text==''N'' ){
                                                    row.push(trim(text)); 
                                                } else {
                                                    row.push('''');
                                                }
                                            }else{
                                                row.push(trim(text)); 
                                            }
                                        }
                                    }
                                csv.push(row.join(",")); 
                            }
                            downloadCSV(csv.join("\n"), filename);                           
                       }','JavaScript');
                       
            htp.script('
                       function downloadCSV(csv, filename) {
                                var csvFile;
                                var downloadLink;
                                var localnavigator;
                                
                                // CSV file
                                csvFile = new Blob([csv], {type: "text/csv"});
                               
                                // Download link
                                downloadLink = document.createElement("a");
                               
                                // File name
                                downloadLink.download = filename;

                                // Create a link to the file
                                downloadLink.href = window.URL.createObjectURL(csvFile);                         

                                // Hide download link
                                downloadLink.style.display = "none";

                                // Add the link to DOM
                                document.body.appendChild(downloadLink);
                                
                                localnavigator = getNavigator();
                                                               
                                // Click download link
                                if(localnavigator == ''ie''){
                                    //alert(''Internet Explorer'');
                                    var blob = new Blob([csv], {"type": "text/csv;charset=utf8;"});
                                    navigator.msSaveBlob(blob, filename);
                                }else if (localnavigator==''firefox''){
                                    downloadLink.click();
                                }else if (localnavigator==''chrome''){
                                    //alert(csv);
                                    var csvContent = "test1,test2,test3,,,\ntest4,,";
                                    var blob = new Blob([csv], {"type": "text/csv;charset=utf8;"});
                                    var link = document.createElement("a");
                                    link.setAttribute("href", window.URL.createObjectURL(blob));
                                    link.setAttribute("download", "Data.csv");
                                    document.body.appendChild(link);
                                    link.click();
                                } else if (localnavigator==''opera''){
                                    downloadLink.click();
                                }                           
                            }
                       ','JavaScript');
                       
            htp.script('  function getNavigator() {
                            var navigator_name;
                            //alert(navigator.userAgent);
                            if(navigator.userAgent.indexOf(''OPR'') != -1 ) 
                                {
                                    //alert(''Opera'');
                                    navigator_name=''opera'';
                                }
                             else if(navigator.userAgent.indexOf("Chrome") != -1 )
                                {
                                    navigator_name=''chrome'';
                                    //alert(''Chrome'');
                                }   
                             else if(navigator.userAgent.indexOf("Safari") != -1)
                                {
                                    navigator_name=''safari'';
                                    //alert(''Safari'');
                                }
                            else if(navigator.userAgent.indexOf("Firefox") != -1 ) 
                                {   
                                    navigator_name=''firefox'';
                                    alert(''Firefox'');
                                }
                            else if((navigator.userAgent.indexOf("MSIE") != -1 ) || (!!document.documentMode == true )) //IF IE > 10
                                {
                                    navigator_name=''ie'';
                                    //alert(''IE''); 
                                }  
                            else 
                                {
                                    navigator_name=''unknown'';
                                    //alert(''unknown'');
                                }
                            //alert(''getNavigator'');
                            return navigator_name;
                       }','JavaScript');  
            
            
            
          --It's working but it's hide from the user
          /*
         htp.formSelectOpen('p_hide_columns_number','Show range of columns',NULL,'id="id_hide_columns_number" onchange="hideColumns(this.value);"'); 
            twbkwbis.p_formSelectOption('All','0','SELECTED');
            twbkwbis.p_formSelectOption('1-9','1'); 
            twbkwbis.p_formSelectOption('10-19','2'); 
            twbkwbis.p_formSelectOption('20-29','3'); 
            twbkwbis.p_formSelectOption('30-39','4'); 
            twbkwbis.p_formSelectOption('40-49','5'); 
            twbkwbis.p_formSelectOption('50-59','6'); 
            twbkwbis.p_formSelectOption('60-69','7'); 
            twbkwbis.p_formSelectOption('70-79','8'); 
            twbkwbis.p_formSelectOption('80-89','9');
            twbkwbis.p_formSelectOption('90-100','10');
         htp.formSelectClose;
        */
        p_create_table_params(p_term_code,p_crn,NULL); 
        twbkfrmt.p_paragraph(2);
        
        htp.formSelectOpen('p_hide_columns_week','Show range of weeks',NULL,'id="id_hide_weeks" onchange="hideColumnsByWeek(this.value);"'); 
            twbkwbis.p_formSelectOption('All weeks','0','SELECTED');
            twbkwbis.p_formSelectOption('Show week 1-4','1'); 
            twbkwbis.p_formSelectOption('Show week 5-8','2'); 
            twbkwbis.p_formSelectOption('Show week 9-12','3'); 
            twbkwbis.p_formSelectOption('Show week 13-16','4'); 
            twbkwbis.p_formSelectOption('Show week 17-20','5'); 
        htp.formSelectClose;
         
           
            twbkfrmt.p_tableopen ('DATAWITHBORDER',ccaption=> G$_NLS.Get ('BWGKOEM1-0050', 'SQL', 'Attendance Roster'),
                                          cattributes=> G$_NLS.Get ('BWGKOEM1-0051','SQL','SUMMARY="This table shows the student information."  id="table_attendance" ' ));
                                     
            twbkfrmt.p_tablerowopen;   
            
            BWSKUTILS.P_CreatetableHeader ('Student Name','NOWRAP');
            BWSKUTILS.P_CreatetableHeader ('LDA','NOWRAP');
            BWSKUTILS.P_CreatetableHeader ('ADM STATUS','NOWRAP');   
                               
            --Get the first REGULAR meeting day. 
            SELECT  TO_CHAR(MIN(SZRATTENDANCE_ATTENDANCE_DATE ),'mm-dd-yyyy')
              INTO  l_first_regular_meeting_date
              FROM  PRCCUSR.SZRATTENDANCE
             WHERE  SZRATTENDANCE_TERM_CODE=p_term_code
               AND  SZRATTENDANCE_CRN=p_crn
               AND SZRATTENDANCE_REGULAR_MEETING=REGULAR_MEETING;
               
          
            
            OPEN dates_cursor;
            LOOP
                FETCH dates_cursor INTO dates_rec;
                EXIT WHEN dates_cursor%NOTFOUND;
                    BWSKUTILS.P_CreatetableHeader (TO_CHAR(dates_rec.date_attendance,'mm-dd-yyyy'));
            END LOOP;
            
            IF dates_cursor%ISOPEN
            THEN
                CLOSE dates_cursor; 
            END IF;
                       
            twbkfrmt.p_tablerowclose;
            
            
            OPEN students_cursor;
            LOOP
                FETCH students_cursor INTO students_detail_rec;
                EXIT WHEN students_cursor%NOTFOUND;
                    twbkfrmt.p_tablerowopen;
                        IF students_detail_rec.full_name IS NOT NULL
                        THEN
                           -- twbkfrmt.p_tabledata ( students_detail_rec.full_name ||' '|| students_detail_rec.student_id , calign=>'LEFT');            
                            twbkfrmt.p_tabledata(twbkfrmt.f_printanchor (gv_attendance_detail ||'?p_term_code=' || p_term_code ||'&p_crn='||p_crn || '&p_pidm='||students_detail_rec.student_id, students_detail_rec.full_name ||' '|| students_detail_rec.student_id, NULL ,cattributes=>'TARGET=_blank'),calign=>'LEFT',cnowrap=>'NOWRAP');
                            twbkfrmt.p_tabledata ( htf.formText('p_LAD','10','10',students_detail_rec.student_lda,cattributes      => 'placeholder="mm/dd/yyyy" ENABLED  id="' ||students_detail_rec.student_id||'"'  ),cnowrap=>'NOWRAP');
                            twbkfrmt.p_tabledata ( f_retrieve_adm_status(p_term_code,students_detail_rec.student_pidm,p_crn),cnowrap=>'NOWRAP');
                        END IF;
                        
                        --FOR 
                           
                        OPEN dates_cursor;
                        LOOP
                        FETCH dates_cursor INTO dates_rec;
                        EXIT WHEN dates_cursor%NOTFOUND;
                            FOR item_value IN (SELECT DECODE(SZRATTENDANCE_ATTENDED,'Y','Y','N','N','T','') date_value
                                                     FROM  PRCCUSR.SZRATTENDANCE
                                                    WHERE SZRATTENDANCE_TERM_CODE=p_term_code
                                                      AND SZRATTENDANCE_CRN=p_crn
                                                      AND SZRATTENDANCE_SPRIDEN_ID=students_detail_rec.student_id 
                                                      AND SZRATTENDANCE_ATTENDANCE_DATE=dates_rec.date_attendance
                                ) 
                                    LOOP
                                        twbkfrmt.p_tabledata ( item_value.date_value, calign=>'CENTER');
                                        /*
                                        IF (item_value.date_value='Y')
                                        THEN
                                           twbkfrmt.p_paragraph(twbkfrmt.p_printmessage ('aq', 3));
                                        ELSE
                                            twbkfrmt.p_tabledata ( twbkfrmt.p_printmessage ('', 3));
                                        END IF;
                                        */
                                    END LOOP;
                        END LOOP;
                        CLOSE dates_cursor;      
                    twbkfrmt.p_tablerowclose;  
                EXIT WHEN students_cursor%NOTFOUND;
            END LOOP;
            CLOSE students_cursor; 
            
        twbkfrmt.p_tableclose;  
        twbkfrmt.P_FormHidden ('p_list_for_update', '','id="p_list_for_update"');
        twbkfrmt.P_FormHidden ('p_term_code', p_term_code);    
        twbkfrmt.P_FormHidden ('p_crn', p_crn);     
        twbkfrmt.P_FormHidden ('p_first_regular_meeting_date', l_first_regular_meeting_date,'id="p_first_regular_meeting_date"');
        twbkfrmt.P_FormHidden ('p_save_roster','NONE','id="p_save_roster"');
        
       
        BWSKUTILS.p_TableOpen('DATAENTRY');
            BWSKUTILS.p_tableDataOpen;            
            --twbkfrmt.p_formsubmit('p_submit_save_roster','Submit','onclick="getInputTextValues(event)" ');
            HTP.P('<INPUT class="mybutton" '  || gv_local_style || ' TYPE="button"  NAME="p_submit_save_roster" value="Submit" onclick="getInputTextValues(event);">');
            BWSKUTILS.p_tableDataClose;
            BWSKUTILS.p_tableDataOpen;            
            --twbkfrmt.p_formsubmit('p_export_roster','Export','onclick="exportTableToCSV(event)"' );
            --HTP.P('<INPUT class="mybutton" '  || gv_local_style || ' TYPE="button"  NAME="p_export_roster" value="Export" onclick="exportTableToCSV(event);">');
            HTP.P('<INPUT class="mybutton" '  || gv_local_style || ' TYPE="button"  NAME="p_export_roster" value="Export" onclick="exportTableToCSV(event);">');
            BWSKUTILS.p_tableDataClose;
     	BWSKUTILS.p_tableClose;
		
		
        twbkfrmt.p_paragraph(3);
        twbkfrmt.p_printanchor('twbkwbis.P_GenMenu?name=bmenu.P_FacMainMnu','Return To Menu',cattributes=>'onclick="toggle(''not_table_attendance'');"');
                   
                
    END P_ShowRosterPage;                           
    
    PROCEDURE P_AttendanceDetail (p_term_code IN general.gtvsdax.gtvsdax_external_code%TYPE,
                                  p_crn       IN PRCCUSR.SZRATTENDANCE.SZRATTENDANCE_CRN%TYPE,
                                  p_pidm      IN VARCHAR2 DEFAULT NULL)
    IS
        CURSOR attendance_cursor
        IS
          SELECT sfrstcr_pidm,
            s.spriden_first_name ||' '|| s.spriden_last_name  student_name,
            s.spriden_id,
            prccusr.student_info.f_get_street_line1(sfrstcr_pidm) street1,
            prccusr.student_info.f_get_street_line2(sfrstcr_pidm) street1_1,
            prccusr.student_info.f_get_city(sfrstcr_pidm) city,
            prccusr.student_info.f_get_state_code(sfrstcr_pidm) state_code,
            prccusr.student_info.f_get_zip(sfrstcr_pidm) zip,
            sfrstcr_term_code,
            ssbsect_ptrm_code,
            sfrstcr_crn,
            ssbsect_subj_code,
            ssbsect_crse_numb,
            SSBSECT_SEQ_NUMB section,
            ssbsect_camp_code,
            nvl(ssbsect_crse_title, a.scbcrse_title) title,
            sfrstcr_credit_hr credit,
            sfrstcr_levl_code level_code,
            sfrstcr_rsts_code status,
            --
            trunc(ssrmeet_start_date) start_date,
            trunc(ssrmeet_end_date) end_date,
            ssrmeet_begin_time,
            ssrmeet_end_time,
            ssrmeet_days_code,
            nvl(case when ssrmeet_bldg_code = 'ONLINE' then 'ONLINE' else
            ssrmeet_sun_day||ssrmeet_mon_day||ssrmeet_tue_day||ssrmeet_wed_day||ssrmeet_thu_day||ssrmeet_fri_day||ssrmeet_sat_day end, 'TBA') meet_days,
            ssrmeet_bldg_code,
            ssrmeet_room_code,
            i.spriden_last_name||', '|| substr(i.spriden_first_name,1,1) instructor,
            prccusr.student_info.f_get_cred_hrs(sfrstcr_pidm, sfrstcr_term_code) cred_hrs,
            ssrmeet_mtyp_code mtyp
            
            from sfrstcr, ssbsect, scbcrse a, ssrmeet, sirasgn, spriden i, spriden s
            where sfrstcr_rsts_code in ('RE','AU')
            and sfrstcr_term_code = p_term_code
            --and sfrstcr_pidm = p_pidm
            and s.spriden_id = p_pidm
            and sfrstcr_term_code = ssbsect_term_code
            and sfrstcr_crn = ssbsect_crn
            and ssbsect_subj_code = a.scbcrse_subj_code
            and ssbsect_crse_numb = a.scbcrse_crse_numb
            and a.scbcrse_eff_term = (select max(scbcrse_eff_term)
                                        from scbcrse b
                                        where b.scbcrse_subj_code = a.scbcrse_subj_code
                                          and b.scbcrse_crse_numb = a.scbcrse_crse_numb)
            and ssrmeet_term_code = sfrstcr_term_code
            and ssrmeet_crn = sfrstcr_crn
            and sirasgn_term_code = sfrstcr_term_code
            and sirasgn_crn = sfrstcr_crn
            --and sirasgn_primary_ind = 'Y'
            and i.spriden_pidm(+) = sirasgn_pidm
            and i.spriden_change_ind is null
            and s.spriden_pidm(+) = sfrstcr_pidm
            and s.spriden_change_ind is null
            order by ssbsect_ptrm_code, sfrstcr_crn;

        address                         VARCHAR2(50);
        previous_detail_rec             detail_row;
        total_hours                     NUMBER:=0;
    BEGIN
        IF NOT twbkwbis.f_validuser (global_pidm)
        THEN   
            RETURN;
        END IF;   
        bwckfrmt.p_open_doc(gv_attendance_detail);
        P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','P_AttendanceDetail',global_pidm ||'-Showing Student Scheduled Details: ' || ' pidm:' || p_pidm ||' crn:' || p_crn || ' term_code: '|| p_term_code ,MESSAGE_LEVEL_INFO,gv_session_step_number) ;
	          
        OPEN attendance_cursor;
        FETCH attendance_cursor INTO detail_rec;
        CLOSE attendance_cursor;  
        
        address:=detail_rec.street1 ||', ' || detail_rec.city ||', ' ||detail_rec.state_code || ' ' || detail_rec.zip ;
        p_create_table_student_details(p_term_code,detail_rec.pidm,detail_rec.student_name,detail_rec.spriden_id,address,detail_rec.cred_hrs);    
            
            twbkfrmt.p_tableopen (
            'DATAWITHBORDER',
            ccaption=> G$_NLS.Get ('BWGKOEM1-0050', 'SQL', 'Student''s schedule'),
            cattributes=> G$_NLS.Get ('BWGKOEM1-0051', 'SQL', 'SUMMARY="This table shows the student information."  id="table_attendance" WIDTH="100%" ' )  
            );
            
            --HTP.tableopen (1) ;
            twbkfrmt.p_tablerowopen;
            --BWSKUTILS.P_CreatetableHeader ('PIDM');
            --BWSKUTILS.P_CreatetableHeader ('STUDENT NAME');
            --BWSKUTILS.P_CreatetableHeader ('SPRIDEN ID');	
            --BWSKUTILS.P_CreatetableHeader ('STREET1');	
            --BWSKUTILS.P_CreatetableHeader ('STREET1_1');
            --BWSKUTILS.P_CreatetableHeader ('CITY');		 
			--BWSKUTILS.P_CreatetableHeader ('STATE CODE');
			--BWSKUTILS.P_CreatetableHeader ('ZIP');
			--BWSKUTILS.P_CreatetableHeader ('TERM CODE');
			BWSKUTILS.P_CreatetableHeader ('PTRM CODE',NULL);
            BWSKUTILS.P_CreatetableHeader ('CRN',NULL);
            BWSKUTILS.P_CreatetableHeader ('SUBJ CODE',NULL);
            BWSKUTILS.P_CreatetableHeader ('COURSE NUM',NULL);
            BWSKUTILS.P_CreatetableHeader ('SECTION',NULL);
            BWSKUTILS.P_CreatetableHeader ('CAMP CODE',NULL);
            BWSKUTILS.P_CreatetableHeader ('TITLE',NULL);
            BWSKUTILS.P_CreatetableHeader ('CREDIT',NULL);
            BWSKUTILS.P_CreatetableHeader ('LEVEL CODE',NULL);
            BWSKUTILS.P_CreatetableHeader ('STATUS',NULL);
            BWSKUTILS.P_CreatetableHeader ('START DATE','TRUE');
            BWSKUTILS.P_CreatetableHeader ('END &nbspDATE','TRUE');
            BWSKUTILS.P_CreatetableHeader ('BEGIN TIME',NULL);
            BWSKUTILS.P_CreatetableHeader ('END TIME',NULL);
            --BWSKUTILS.P_CreatetableHeader ('DAYS CODE',NULL); 
            BWSKUTILS.P_CreatetableHeader ('MEET DAYS',NULL);
            BWSKUTILS.P_CreatetableHeader ('BLDG CODE',NULL);
            BWSKUTILS.P_CreatetableHeader ('ROOM CODE',NULL);
            BWSKUTILS.P_CreatetableHeader ('INSTRUCTOR',NULL);
            --BWSKUTILS.P_CreatetableHeader ('CRED HOURS',NULL);
            BWSKUTILS.P_CreatetableHeader ('MTYP',NULL);
            
            
        twbkfrmt.p_tablerowclose;
		OPEN attendance_cursor;
        LOOP
        FETCH attendance_cursor INTO detail_rec;
        EXIT WHEN attendance_cursor%NOTFOUND;
            twbkfrmt.p_tablerowopen;
                /*
                IF detail_rec.pidm IS NOT NULL
                THEN
                    twbkfrmt.p_tabledata ( detail_rec.pidm, 'CENTER');
                END IF;
                
                IF detail_rec.student_name IS NOT NULL
                THEN
                    twbkfrmt.p_tabledata ( detail_rec.student_name, 'CENTER');
                END IF;
                
                IF detail_rec.spriden_id IS NOT NULL
                THEN
                    twbkfrmt.p_tabledata ( detail_rec.spriden_id, NULL);
                END IF;
                
                IF detail_rec.street1 IS NOT NULL
                THEN
                    twbkfrmt.p_tabledata ( detail_rec.street1, NULL);
                END IF;
                
                
                IF detail_rec.street1_1 IS NOT NULL
                THEN
                    twbkfrmt.p_tabledata ( detail_rec.street1_1, NULL);
                ELSE
                    twbkfrmt.p_tabledata ( ' ', NULL);
                END IF;
                
                IF detail_rec.city IS NOT NULL
                THEN
                    twbkfrmt.p_tabledata ( detail_rec.city, NULL);
                END IF;
                
                IF detail_rec.state_code IS NOT NULL
                THEN
                    twbkfrmt.p_tabledata ( detail_rec.state_code, NULL);
                ELSE
                    twbkfrmt.p_tabledata ( '-', NULL);
                END IF;
                
                IF detail_rec.zip IS NOT NULL
                THEN
                    twbkfrmt.p_tabledata ( detail_rec.zip, NULL);
                END IF;
                
                IF detail_rec.term_code IS NOT NULL
                THEN
                    twbkfrmt.p_tabledata ( detail_rec.term_code, 'CENTER');
                END IF;
                */
                IF detail_rec.ptrm_code IS NOT NULL
                THEN
                    /*
                    IF previous_detail_rec.ptrm_code = detail_rec.ptrm_code
                    THEN
                        twbkfrmt.p_tabledata ( '', 'CENTER');
                    ELSE
                        twbkfrmt.p_tabledata ( detail_rec.ptrm_code, 'CENTER');   
                    END IF;
                    */
                    twbkfrmt.p_tabledata ( detail_rec.ptrm_code, 'CENTER');
                ELSE
                    twbkfrmt.p_tabledata ( '', NULL);
                END IF;
                
                IF detail_rec.sfrstcr_crn IS NOT NULL
                THEN
                    IF previous_detail_rec.sfrstcr_crn = detail_rec.sfrstcr_crn
                    THEN
                        twbkfrmt.p_tabledata ( '', 'CENTER');
                    ELSE
                        twbkfrmt.p_tabledata ( detail_rec.sfrstcr_crn, 'CENTER');   
                    END IF;
                ELSE
                    twbkfrmt.p_tabledata ( '', NULL);
                END IF;
                
                IF detail_rec.ssbsect_subj_code IS NOT NULL
                THEN
                    IF previous_detail_rec.ssbsect_subj_code = detail_rec.ssbsect_subj_code
                    THEN
                        twbkfrmt.p_tabledata ( '', 'CENTER');
                    ELSE
                         twbkfrmt.p_tabledata ( detail_rec.ssbsect_subj_code, 'CENTER');
                    END IF;
                ELSE
                    twbkfrmt.p_tabledata ( ' ', NULL);
                END IF;

                IF detail_rec.ssbsect_crse_numb IS NOT NULL
                THEN
                    IF previous_detail_rec.ssbsect_crse_numb = detail_rec.ssbsect_crse_numb
                    THEN
                        twbkfrmt.p_tabledata ( '', 'CENTER');
                    ELSE
                         twbkfrmt.p_tabledata ( detail_rec.ssbsect_crse_numb, 'CENTER');
                    END IF;
                ELSE
                    twbkfrmt.p_tabledata ( ' ', NULL);
                END IF;
                
                
                IF detail_rec.section IS NOT NULL
                THEN
                    /*
                    IF previous_detail_rec.section = detail_rec.section
                    THEN
                        twbkfrmt.p_tabledata ( '', 'CENTER');
                    ELSE
                        twbkfrmt.p_tabledata ( detail_rec.section, 'CENTER');
                    END IF;
                    */
                     twbkfrmt.p_tabledata ( detail_rec.section, 'CENTER');
                ELSE
                    twbkfrmt.p_tabledata ( ' ', NULL);
                END IF;
                
                IF detail_rec.ssbsect_camp_code IS NOT NULL
                THEN
                    /*
                    IF previous_detail_rec.ssbsect_camp_code = detail_rec.ssbsect_camp_code
                    THEN
                        twbkfrmt.p_tabledata ( '', 'CENTER');
                    ELSE
                        twbkfrmt.p_tabledata ( detail_rec.ssbsect_camp_code, 'CENTER');
                    END IF;
                    */
                    twbkfrmt.p_tabledata ( detail_rec.ssbsect_camp_code, 'CENTER');
                ELSE
                    twbkfrmt.p_tabledata ( ' ', NULL);
                END IF;
                
                IF detail_rec.title IS NOT NULL
                THEN
                    IF previous_detail_rec.title = detail_rec.title
                    THEN
                        twbkfrmt.p_tabledata ( '', 'CENTER');
                    ELSE
                        twbkfrmt.p_tabledata ( detail_rec.title, 'CENTER');
                    END IF; 
                ELSE
                    twbkfrmt.p_tabledata ( ' ', NULL);
                END IF;
                
                
                IF detail_rec.credit IS NOT NULL
                THEN
                    twbkfrmt.p_tabledata ( detail_rec.credit, 'CENTER');
                ELSE
                    twbkfrmt.p_tabledata ( ' ', NULL);
                END IF;
                
                
                IF detail_rec.level_code IS NOT NULL
                THEN
                    twbkfrmt.p_tabledata ( detail_rec.level_code, 'CENTER');
                ELSE
                    twbkfrmt.p_tabledata ( ' ', NULL);
                END IF;
                
                IF detail_rec.status IS NOT NULL
                THEN
                   twbkfrmt.p_tabledata ( detail_rec.status, 'CENTER');
                ELSE
                    twbkfrmt.p_tabledata ( ' ', NULL);
                END IF;
                
                IF detail_rec.start_date IS NOT NULL
                THEN
                     twbkfrmt.p_tabledata ( detail_rec.start_date, 'LEFT');
                ELSE
                    twbkfrmt.p_tabledata ( ' ', NULL);
                END IF;

                IF detail_rec.end_date IS NOT NULL
                THEN
                    twbkfrmt.p_tabledata ( detail_rec.end_date, 'LEFT');
                ELSE
                    twbkfrmt.p_tabledata ( ' ', NULL);
                END IF;
                
                IF detail_rec.ssrmeet_begin_time IS NOT NULL
                THEN
                    IF previous_detail_rec.ssrmeet_begin_time = detail_rec.ssrmeet_begin_time
                    THEN
                        twbkfrmt.p_tabledata ( '', 'CENTER');
                    ELSE
                        twbkfrmt.p_tabledata ( detail_rec.ssrmeet_begin_time, 'CENTER');
                    END IF; 
                ELSE
                    twbkfrmt.p_tabledata ( ' ', NULL);
                END IF;
                
                IF detail_rec.ssrmeet_end_time IS NOT NULL
                THEN
                    IF previous_detail_rec.ssrmeet_end_time = detail_rec.ssrmeet_end_time
                    THEN
                        twbkfrmt.p_tabledata ( '', 'CENTER');
                    ELSE
                        twbkfrmt.p_tabledata ( detail_rec.ssrmeet_end_time, 'CENTER');
                    END IF;
                ELSE
                    twbkfrmt.p_tabledata ( ' ', NULL);
                END IF;

                /*
                IF detail_rec.ssrmeet_days_code IS NOT NULL
                THEN
                    IF previous_detail_rec.ssrmeet_days_code = detail_rec.ssrmeet_days_code
                    THEN
                        twbkfrmt.p_tabledata ( '', 'CENTER');
                    ELSE
                        twbkfrmt.p_tabledata ( detail_rec.ssrmeet_days_code, 'CENTER');
                    END IF;
                ELSE
                    twbkfrmt.p_tabledata ( '', NULL);
                END IF;
                */
                
                
                IF detail_rec.meet_days IS NOT NULL
                THEN
                    IF previous_detail_rec.meet_days = detail_rec.meet_days
                    THEN
                        twbkfrmt.p_tabledata ( '', 'CENTER');
                    ELSE
                        twbkfrmt.p_tabledata ( detail_rec.meet_days, 'CENTER');
                    END IF;
                ELSE
                    twbkfrmt.p_tabledata ( ' ', NULL);
                END IF;
                
                
                IF detail_rec.ssrmeet_bldg_code IS NOT NULL
                THEN
                    IF previous_detail_rec.ssrmeet_bldg_code = detail_rec.ssrmeet_bldg_code
                    THEN
                        twbkfrmt.p_tabledata ( '', 'CENTER');
                    ELSE
                        twbkfrmt.p_tabledata ( detail_rec.ssrmeet_bldg_code, 'CENTER');
                    END IF;
                ELSE
                    twbkfrmt.p_tabledata ( ' ', NULL);
                END IF;
                
                
                IF detail_rec.ssrmeet_room_code IS NOT NULL
                THEN
                    IF previous_detail_rec.ssrmeet_room_code = detail_rec.ssrmeet_room_code
                    THEN
                        twbkfrmt.p_tabledata ( '', 'CENTER');
                    ELSE
                        twbkfrmt.p_tabledata ( detail_rec.ssrmeet_room_code, 'CENTER');
                    END IF;
                ELSE
                    twbkfrmt.p_tabledata ( ' ', NULL);
                END IF;
                
                
                IF detail_rec.instructor IS NOT NULL
                THEN
                    twbkfrmt.p_tabledata ( detail_rec.instructor, NULL);
                ELSE
                    twbkfrmt.p_tabledata ( ' ', NULL);
                END IF;
                
                /*
                IF detail_rec.cred_hrs IS NOT NULL
                THEN
                    twbkfrmt.p_tabledata ( detail_rec.cred_hrs, 'CENTER');
                ELSE
                    twbkfrmt.p_tabledata ( ' ', NULL);
                END IF;
                */
                
                IF detail_rec.mtyp IS NOT NULL
                THEN
                    /*
                    IF previous_detail_rec.mtyp = detail_rec.mtyp
                    THEN
                        twbkfrmt.p_tabledata ( '', 'CENTER');
                    ELSE
                        twbkfrmt.p_tabledata ( detail_rec.mtyp, NULL);
                    END IF;
                    */
                    twbkfrmt.p_tabledata ( detail_rec.mtyp, NULL);
                ELSE
                    twbkfrmt.p_tabledata ( ' ', NULL);
                END IF;
                
                previous_detail_rec := detail_rec;
            twbkfrmt.p_tablerowclose;
            END LOOP;
            twbkfrmt.p_tableclose;   
        CLOSE attendance_cursor;  
		
    END P_AttendanceDetail;
    
    PROCEDURE P_AttendanceApplication ( p_term_code		     IN GENERAL.GTVSDAX.GTVSDAX_EXTERNAL_CODE%TYPE DEFAULT NULL,
                                        p_crn                IN SSBSECT.ssbsect_crn%TYPE DEFAULT NULL,
                                        p_submit_in          IN VARCHAR2 DEFAULT NULL,
                                        p_month              IN VARCHAR2 DEFAULT NULL,
                                        p_year               IN VARCHAR2 DEFAULT NULL,
                                        p_day                IN VARCHAR2 DEFAULT NULL,
                                        p_submit_button      IN VARCHAR2 DEFAULT NULL,
                                        p_term_code_selected IN GENERAL.GTVSDAX.GTVSDAX_EXTERNAL_CODE%TYPE DEFAULT NULL,
                                        p_not_regular_date   IN VARCHAR2 DEFAULT NULL, 
                                        p_save_button        IN VARCHAR2 DEFAULT NULL,
                                        p_check              IN VARCHAR2 DEFAULT NULL,
                                        p_button_check       IN VARCHAR2 DEFAULT NULL,
                                        p_button_uncheck     IN VARCHAR2 DEFAULT NULL,
                                        p_spriden_list       IN VARCHAR2 DEFAULT NULL,
                                        p_date               IN DATE DEFAULT NULL
                                        )
    IS
        local_month        NUMBER;
        attendance_date    DATE;
    BEGIN
        IF NOT twbkwbis.f_validuser (global_pidm)
        THEN   
            RETURN;
        END IF;   
        P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','P_AttendanceApplication',global_pidm ||'-Starting Application. Entering method. - Params received -  term_code:'|| p_term_code || ' term_code_selected:'|| p_term_code_selected || ' crn:' || p_crn || ' month:' || p_month || ' day:'|| p_day ||' submit_button:' ||p_submit_button || ' p_not_regular_date:'|| p_not_regular_date,MESSAGE_LEVEL_INFO,gv_session_step_number) ;
        IF p_submit_button = 'Submit'
        THEN
            local_month:=p_month+1;
            IF f_validate_parameters(global_pidm,p_term_code,p_crn,p_year,local_month,p_day) = TRUE
                THEN
                    attendance_date :=TO_DATE(local_month||'-'||p_day||'-'||p_year,'mm-dd-yyyy');
                    IF p_not_regular_date ='ON' --CheckBox on Attendace Form
                    THEN 
                        P_Insert_Attendance(p_term_code,p_crn,attendance_date);
                        p_displayListForUpdate(gv_doc_attendance_application,p_term_code,p_crn,attendance_date);
                    ELSE
                        IF f_validate_attendant_date (p_term_code,p_crn,attendance_date)=TRUE
                        THEN
                            P_CreateRoster(p_term_code,p_crn);
                            p_displayListForUpdate(gv_doc_attendance_application,p_term_code,p_crn,attendance_date);
                        ELSE
                            p_displayBoxes(p_term_code,p_crn,TRUE,attendance_date);
                        END IF;
                        RETURN;
                    END IF;
                END IF;   
                
        ELSIF p_save_button = 'Submit'
        THEN
            P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','P_AttendanceApplication',global_pidm ||'-Updating Values: ' || substr(p_spriden_list,1,30) || '... crn:' || p_crn || ' term_code: '|| p_term_code ,MESSAGE_LEVEL_INFO,gv_session_step_number) ;
            p_update_rows(p_spriden_list,p_term_code,p_crn,p_date);
            p_displayListAttendance(gv_doc_attendance_application,p_term_code,p_crn,p_date);
        ELSE
            BEGIN
                P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','P_AttendanceApplication',global_pidm ||'-p_term_code ' || p_term_code || ' CRN: ' || p_crn,MESSAGE_LEVEL_INFO,gv_session_step_number) ;
                IF NOT f_Local_validTerm(p_term_code)
                THEN
                    P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','P_AttendanceApplication',global_pidm ||'-Displaying Term Code Drop Down List without parameters.',MESSAGE_LEVEL_DEBUG,gv_session_step_number) ;
                    p_displayBoxes;
                ELSE
                    P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','P_AttendanceApplication',global_pidm ||'-Displaying CRN Drop Down List with parameters. term_code: ' || p_term_code,MESSAGE_LEVEL_DEBUG,gv_session_step_number) ;
                    p_displayBoxes(p_term_code);--p_crn
                    
                END IF;
            END;
        END IF;         
    END P_AttendanceApplication;


    

    /*Basic HTML Page*/
    PROCEDURE P_BasicHTML
      IS
      BEGIN
         IF NOT twbkwbis.f_validuser (global_pidm)
            THEN   
                RETURN;
        END IF;   

        HTP.htmlOpen;
        HTP.headOpen;
        HTP.title('Foundation Scholarship');
        HTP.header(1, 'Foundation');
        HTP.headClose;
        HTP.bodyOpen;
        HTP.bodyClose;
        HTP.htmlClose;
    END P_BasicHTML;


    
    PROCEDURE P_displaySelectTerm ( p_term_code                IN general.gtvsdax.gtvsdax_external_code%TYPE DEFAULT NULL)
    IS
    BEGIN
        BWSKUTILS.p_tableRowOpen;
                BWSKUTILS.p_tableDataLabel('Select a Term: ');
                BWSKUTILS.p_tableDataOpen;
                    htp.formSelectOpen('p_term_code',NULL,NULL,'id="id_term_code" onchange="this.form.submit()"');    
                        IF p_term_code IS NULL 
                        THEN
                            twbkwbis.p_formSelectOption('Choose',NULL);
                        --ELSE
                         --   twbkwbis.p_formSelectOption(f_get_term_description(p_term_code),p_term_code);
                        END IF;
                                        
                        FOR term IN cu_terms_by_spriden_id
                        LOOP
                            -- value term code; displays term description
                            IF p_term_code = term.term_code 
                            THEN
                                twbkwbis.p_formSelectOption(term.term_desc, term.term_code,'SELECTED');
                            ELSE
                                twbkwbis.p_formSelectOption(term.term_desc, term.term_code);
                            END IF;
                            
                        END LOOP;
                        --CLOSE cu_terms_by_spriden_id;
                    htp.formSelectClose;
                BWSKUTILS.p_tableDataClose;  
            BWSKUTILS.p_tableRowClose;
    END P_displaySelectTerm;
    
    PROCEDURE P_displaySelectCRN ( p_term_code                IN general.gtvsdax.gtvsdax_external_code%TYPE DEFAULT NULL,
                                   p_crn                      IN SSBSECT.ssbsect_crn%TYPE DEFAULT NULL)
    IS
        CURSOR cur_course_numbers
        IS
            SELECT	DISTINCT ssbsect_crn crn
                            ,NVL(ssbsect_crse_title,scbcrse_title) crse_title
                
        	  FROM 	saturn.ssbsect
		       	   ,saturn.ssrmeet
			       ,saturn.scbcrse
			       ,saturn.sirasgn
			   --		JIONS:
	         WHERE	ssrmeet_crn = ssbsect_crn
	    	   AND 	ssrmeet_term_code = ssbsect_term_code
		       AND 	ssbsect_subj_code = scbcrse_subj_code
	    	   AND 	ssbsect_crse_numb = scbcrse_crse_numb
	    	   AND 	sirasgn_crn(+) = ssrmeet_crn
		       AND 	sirasgn_term_code(+) = ssrmeet_term_code
		       AND	sirasgn_category(+) = ssrmeet_catagory
		       AND 	scbcrse_eff_term =
				  (	SELECT	MAX(b.scbcrse_eff_term) 
					  FROM	saturn.scbcrse b 
                     WHERE	b.scbcrse_eff_term <= ssbsect_term_code
					   AND 	b.scbcrse_crse_numb = ssbsect_crse_numb 
					   AND	b.scbcrse_subj_code = ssbsect_subj_code
				  )
			   --		FILTERS
			   --AND	sirasgn_primary_ind(+) = 'Y'
			   AND 	ssbsect_term_code = p_term_code
			   and  sirasgn_pidm = global_pidm   --global_pidm
			   AND 	ssbsect_subj_code <> 'CEU'	-- exclude continuing education
			   AND	ssbsect_sess_code != 'E'	-- exclude dual enrollment
			   AND	ssbsect_ssts_code = 'A'		-- only list active section
			   ORDER BY crse_title;
       
        --is_empty              BOOLEAN:=TRUE;
    BEGIN
        P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','P_displaySelectCRN',global_pidm ||' Entering method. - Params received -  term_code:'|| p_term_code ||  ' crn: ' || p_crn ,MESSAGE_LEVEL_DEBUG,gv_session_step_number) ;
         -- create CRN select drop-down
            BWSKUTILS.p_tableRowOpen;
                BWSKUTILS.p_tableDataLabel('Select a CRN: ');
                BWSKUTILS.p_tableDataOpen;
                      
                        IF p_term_code IS NULL 
                        THEN
                            htp.formSelectOpen('p_crn',NULL,NULL,'disabled id="id_crn" onclick="enableMonth()"'); 
                            twbkwbis.p_formSelectOption('Choose',NULL);
                        ELSE
                            htp.formSelectOpen('p_crn',NULL,NULL,'id="id_crn" onclick="enableMonth()"');
                        END IF;
                        
                        IF p_crn IS NULL
                        THEN
                            twbkwbis.p_formSelectOption('Choose', 'Choose','SELECTED');
                        END IF;
                        FOR crn_numbers IN cur_course_numbers
                        LOOP
                            -- value term code; displays term description
                            IF p_crn = crn_numbers.crn
                            THEN
                                twbkwbis.p_formSelectOption(crn_numbers.crse_title ||'-'||crn_numbers.crn , crn_numbers.crn,'SELECTED');
                            ELSE
                                twbkwbis.p_formSelectOption(crn_numbers.crse_title ||'-'||crn_numbers.crn , crn_numbers.crn);
                            END IF;
                            
                        END LOOP;
                    htp.formSelectClose;
                BWSKUTILS.p_tableDataClose;  
            BWSKUTILS.p_tableRowClose;

    END;
    
    
    PROCEDURE P_Display_Meet_Error
    IS
    BEGIN
        bwckfrmt.p_open_doc(gv_doc_attendance_application);
		twbkwbis.P_DispInfo (gv_doc_attendance_application, 'DEFAULT');
		p_dispInfoText('Notice','MEET_ERROR_HEADER');
		-- Open the form
		--BWSKUTILS.p_formOpen(gv_doc_attendance_application, 'POST');       
		BWSKUTILS.p_dispSectionTitle('Select Term, CRN and Start Date', extraWhiteSpace_in => 'YES');
		-- Start a data entry table
		BWSKUTILS.p_TableOpen('DATAENTRY');
            -- create term select drop-down
            p_displayDateBox;
        	
        -- Close the data entry table
		BWSKUTILS.p_tableClose;
		HTP.formSubmit ('p_submit_button', 'Submit','disabled id="id_submit_button"onclick="showAlert()"' );
		-- Add some white space
		twbkfrmt.p_paragraph(3);
    END P_Display_Meet_Error;
    
    PROCEDURE P_displayDateBox (p_enabled                IN BOOLEAN DEFAULT NULL,
                                p_attendance_date        IN DATE DEFAULT NULL)
    IS
    BEGIN
        P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','P_displayDateBox','p_attendance_date: '|| p_attendance_date ,MESSAGE_LEVEL_DEBUG,gv_session_step_number) ;
        BWSKUTILS.p_tableRowOpen;
                BWSKUTILS.p_tableDataLabel('Attendance Date: ');
                BWSKUTILS.p_tableDataOpen;
                    P_displayDropBoxMonth(p_enabled,p_attendance_date);
                    P_displayDropBoxDay(p_attendance_date);
                    P_displayDropBoxYear(p_attendance_date);  
                   
                    
                BWSKUTILS.p_tableDataClose;  
            BWSKUTILS.p_tableRowClose;
    END;
    
    
    PROCEDURE P_displayDropBoxYear (p_attendance_date        IN DATE DEFAULT NULL)
    IS BEGIN
        P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','P_displayDropBoxYear','p_attendance_date: '|| p_attendance_date ,MESSAGE_LEVEL_DEBUG,gv_session_step_number) ;
        IF p_attendance_date IS NULL
        THEN
             htp.formSelectOpen('p_year',NULL,NULL,'id="id_year" disabled onchange="validateLeapYear()"');
        ELSE 
            htp.formSelectOpen('p_year',NULL,NULL,'id="id_year" onchange="validateLeapYear()"');
        END IF;
        
        twbkwbis.p_formSelectOption('2017','2017',f_get_selected_value('2017',p_attendance_date,'YEAR'));
        twbkwbis.p_formSelectOption('2018','2018','SELECTED',f_get_selected_value('2018',p_attendance_date,'YEAR'));
        twbkwbis.p_formSelectOption('2019','2019',f_get_selected_value('2019',p_attendance_date,'YEAR'));
        twbkwbis.p_formSelectOption('2020','2020',f_get_selected_value('2020',p_attendance_date,'YEAR'));
        twbkwbis.p_formSelectOption('2021','2021',f_get_selected_value('2021',p_attendance_date,'YEAR'));
        twbkwbis.p_formSelectOption('2022','2022',f_get_selected_value('2022',p_attendance_date,'YEAR'));
        htp.formSelectClose;
    END P_displayDropBoxYear;
    
    PROCEDURE P_displayDropBoxDay ( p_attendance_date          IN DATE DEFAULT NULL)
    IS 
         local_last_day        NUMBER;
         --local_last_day_number NUMBER;
         --i                   NUMBER;
         local_sel_day         NUMBER;
    BEGIN
         
        htp.formSelectOpen('p_day',NULL,NULL,'id="id_day" onchange="enableButton()"'); 
        IF p_attendance_date IS NULL
        THEN
            twbkwbis.p_formSelectOption('1','1');
        ELSE
            P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','P_displayDropBoxDay','p_attendance_date: '|| p_attendance_date ,MESSAGE_LEVEL_DEBUG,gv_session_step_number) ;
           
            SELECT TO_NUMBER(SUBSTR(TO_CHAR(LAST_DAY(p_attendance_date),'mm-dd-yyyy'),4,2))
              INTO local_last_day
              FROM DUAL;
            
            local_sel_day:=TO_NUMBER(f_get_selected_value(NULL,p_attendance_date,'DAY'));  
            FOR i IN 1..local_last_day LOOP
                IF(i=local_sel_day)
                THEN
                    twbkwbis.p_formSelectOption(local_sel_day,local_sel_day,'SELECTED');
                ELSE
                     twbkwbis.p_formSelectOption(i,i);
                END IF;
            END LOOP;
            
        END IF;
        htp.formSelectClose;
    END P_displayDropBoxDay;
    

        /**
        Display the drop down list in the main webpage.
        */
	PROCEDURE p_displayBoxes (p_term_code           IN GENERAL.GTVSDAX.GTVSDAX_EXTERNAL_CODE%TYPE DEFAULT NULL,
                              p_crn                 IN SSBSECT.ssbsect_crn%TYPE DEFAULT NULL,
                              p_error_date          IN BOOLEAN DEFAULT FALSE,
                              p_attendance_date     IN DATE DEFAULT NULL
                              --p_form_origin         IN VARCHAR2 DEFAULT NULL
                             )
	IS
           escape       VARCHAR2(50):='"<option value="+i+">"+i+"</option>";';  
	BEGIN

		bwckfrmt.p_open_doc(gv_doc_attendance_application);
		twbkwbis.P_DispInfo (gv_doc_attendance_application, 'DEFAULT');
		--htp.script('function showAlert(){alert("I am an alert");}','JavaScript');
		htp.script('function showAlert(){
                    var day=document.getElementById("id_day").value;
                    alert(day);
                    if (isNaN(day)){
                        alert(''Not valid Month'');
                        event.preventDefault();
                    }    
                                                         
                   }','JavaScript');
                   
        htp.script('function validateParams(event){
                   
                     var year=document.getElementById("id_year").value;
                    //alert(year);
                    if (isNaN(document.getElementById("id_year").value)){
                        alert(''Not a valid Year'');
                        event.preventDefault();
                    } 
                    
                    if (isNaN(document.getElementById("id_day").value)){
                        alert(''Not a valid Day'');
                        event.preventDefault();
                    }    
                    
                    if (isNaN(document.getElementById("id_month").value)){
                        alert(''Not a valid Month'');
                        event.preventDefault();
                    }           
                    
                   
                    if (document.getElementById("id_crn").value==''Choose''){
                        alert(''Not a valid CRN'');
                        event.preventDefault();
                    }   
                    
                    return true;                                
                   }','JavaScript');
                   
		htp.script('function enableMonth(){document.getElementById("id_month").disabled=false;}','JavaScript'); 
		htp.script('function enableButton(){document.getElementById("id_submit_button").disabled=false;}','JavaScript'); 
		htp.script('function enableYear(){
                        document.getElementById("id_year").disabled=false;
                        //document.getElementById("id_year").selectedIndex =0;
                        document.getElementById("id_day").selectedIndex =0;
                        getDaysFromMonth();
                    }','JavaScript');
		htp.script('function getNumberOfDays(year, month) {
                        var isLeap = ((year % 4) == 0 && ((year % 100) != 0 || (year % 400) == 0));
                        return [31, (isLeap ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
                    }','JavaScript');
            

        htp.script('function getDaysFromMonth(){
                        var month = document.getElementById("id_month").value;
                        var year = document.getElementById("id_year").value;
                        var days=getNumberOfDays(year,month);
                        document.getElementById("id_day").innerHTML=""; //Very important Line do not delete
                                                                                             
                        for (i = 1; i <= days; i++) {
                             document.getElementById("id_day").innerHTML += '|| escape ||'
                        }
                     }','JavaScript');
                     
        htp.script('function validateLeapYear(){
                        var month = document.getElementById("id_month").value;
                        var year = document.getElementById("id_year").value;
                        var comboday=document.getElementById("id_day");
                        var days=getNumberOfDays(year,month);
                        
                        if((month==1)  && (days==29)) { //leap year
                            //alert(year);
                            var option = document.createElement("option");
                            option.text=''29'';
                            comboday.add(option); 
                        }
                        
                        if((month==1)  && (days==28)) { //NOT leap year
                            var options = comboday.getElementsByTagName(''OPTION'');
                            comboday.removeChild(options[28]);
                        }
                                                 
                     }','JavaScript');
		
		-- Display some header matter
		p_dispInfoText('Notice','NOTICE');
		p_dispInfoText('Contact Us','CONTACT', extraWhiteSpace_in => 'YES');

		-- Open the form
		BWSKUTILS.p_formOpen(gv_doc_attendance_application, 'POST');  --, 'onsubmit="showAlert();"'
		BWSKUTILS.p_dispSectionTitle('Select Term, CRN and Start Date', extraWhiteSpace_in => 'YES');
		IF p_error_date = TRUE
		THEN
            BWSKUTILS.p_printError('Class Does not meet On ' || f_get_formatted_date(p_attendance_date));
            BWSKUTILS.p_printError('Please check NOT REGULAR MEETING DATE and press Submit or Change Attendance Date and press Submit');
            IF(f_is_valid_unscheduled_date(p_term_code,p_crn,p_attendance_date)<>TRUE)
            THEN
                BWSKUTILS.p_printError('Please Select a date between ' || f_get_course_start_date(p_term_code,p_crn)|| ' and '  ||f_get_course_end_date(p_term_code,p_crn));
            END IF;
		END IF;
		
		-- Start a data entry table
		BWSKUTILS.p_TableOpen('DATAENTRY');
            -- create term select drop-down
            P_displaySelectTerm(p_term_code);
            P_displaySelectCRN(p_term_code,p_crn);
            
            IF p_error_date = TRUE
            THEN
                p_displayDateBox(TRUE,p_attendance_date);
                BWSKUTILS.p_tableDataLabel('Not regular or unscheduled meeting date: ');
                BWSKUTILS.p_tableDataOpen;
                htp.formCheckbox('p_not_regular_date','ON',NULL,NULL);
                BWSKUTILS.p_tableDataClose;
                
            ELSE
                p_displayDateBox;
            END IF;
            
        twbkfrmt.P_FormHidden ('p_term_code', local_term_code,'id="id_term_code_selected"');
		-- Close the data entry table
		BWSKUTILS.p_tableClose;
		--HTP.formSubmit ('p_submit_button', 'Submit','disabled id="id_submit_button"onclick="showAlert()"' );
		IF p_error_date = TRUE
		THEN
            HTP.formSubmit ('p_submit_button', 'Submit','id="id_submit_button" onclick="validateParams(event);"' );
		ELSE
            HTP.formSubmit ('p_submit_button', 'Submit','disabled id="id_submit_button" onclick="validateParams(event);"' );
		END IF;
		

		-- Add some white space
		twbkfrmt.p_paragraph(3);
	END p_displayBoxes;
	
	
	
	PROCEDURE p_displayBoxesRoster (p_term_code           IN GENERAL.GTVSDAX.GTVSDAX_EXTERNAL_CODE%TYPE DEFAULT NULL,
                              p_crn                 IN SSBSECT.ssbsect_crn%TYPE DEFAULT NULL,
                              p_error_date          IN BOOLEAN DEFAULT FALSE,
                              p_attendance_date     IN DATE DEFAULT NULL,
                              p_form_origin         IN VARCHAR2 DEFAULT NULL
                             )
	IS
           escape       VARCHAR2(50):='"<option value="+i+">"+i+"</option>";';  
	BEGIN

		bwckfrmt.p_open_doc(gv_doc_attendance_application);
		twbkwbis.P_DispInfo (gv_doc_attendance_application, 'DEFAULT');
		--htp.script('function showAlert(){alert("I am an alert");}','JavaScript');
		htp.script('function showAlert(){
                    alert(''hola'');
                   
                                                         
                   }','JavaScript');
                   
        htp.script('function validateParams(event){
                    var crn=document.getElementById("id_crn").value;
                    if (crn==''Choose''){
                        alert(''Not a valid CRN'');
                        event.preventDefault();
                        return false;
                    } else{
                        return true;       
                    }                    
                   }','JavaScript');
                 
		-- Display some header matter
		p_dispInfoTextRoster('Notice','NOTICE');
		p_dispInfoTextRoster('Contact Us','CONTACT', extraWhiteSpace_in => 'YES');

		-- Open the form
		BWSKUTILS.p_formOpen(p_form_origin, 'POST');       
		BWSKUTILS.p_dispSectionTitle('Select Term, CRN and Start Date', extraWhiteSpace_in => 'YES');
			
		-- Start a data entry table
		BWSKUTILS.p_TableOpen('DATAENTRY');
            -- create term select drop-down
            P_displaySelectTerm(p_term_code);
            P_displaySelectCRN(p_term_code,p_crn);
               
        twbkfrmt.P_FormHidden ('p_term_code', local_term_code,'id="id_term_code_selected"');
		-- Close the data entry table
		BWSKUTILS.p_tableClose;
		--HTP.formSubmit ('p_submit_button', 'Submit','disabled id="id_submit_button"onclick="showAlert()"' );
		HTP.formSubmit ('p_submit_button', 'Submit','enabled id="id_submit_button" onclick="validateParams(event)"' );

		-- Add some white space
		twbkfrmt.p_paragraph(3);
	END p_displayBoxesRoster;
	
	
	PROCEDURE p_displayListAttendance (doc_name    VARCHAR2 DEFAULT NULL,
                                  p_term_code IN general.gtvsdax.gtvsdax_external_code%TYPE,
                                  p_crn       IN PRCCUSR.SZRATTENDANCE.SZRATTENDANCE_CRN%TYPE,
                                  p_date      IN DATE)
    IS
         CURSOR attendance_cursor
         IS
            --SELECT rownum,ATT.SZRATTENDANCE_ATTENDED, spriden_first_name ||	decode(spriden_mi, null, ' ', ' ' || spriden_mi || ', ')||spriden_last_name full_name,sp.spriden_id
            SELECT ATT.SZRATTENDANCE_ATTENDED, spriden_first_name ||' '|| decode(spriden_mi, null, '', '' || spriden_mi || ' ') || spriden_last_name full_name,sp.spriden_id,''
            FROM PRCCUSR.SZRATTENDANCE ATT, SATURN.SPRIDEN SP
             WHERE SP.spriden_id= ATT.SZRATTENDANCE_SPRIDEN_ID
               AND ATT.SZRATTENDANCE_TERM_CODE=p_term_code
               AND ATT.SZRATTENDANCE_CRN=p_crn
               AND ATT.SZRATTENDANCE_ATTENDED='Y'
               AND ATT.SZRATTENDANCE_ATTENDANCE_DATE=p_date
               AND spriden_change_ind IS NULL
             ORDER BY full_name;
               
         CURSOR not_attendance_cursor
         IS
            --SELECT rownum,ATT.SZRATTENDANCE_ATTENDED, spriden_first_name ||	decode(spriden_mi, null, ' ', ' ' || spriden_mi || ', ')||spriden_last_name full_name,sp.spriden_id
            SELECT ATT.SZRATTENDANCE_ATTENDED, spriden_first_name ||' '|| decode(spriden_mi, null, '', '' || spriden_mi || ' ') || spriden_last_name full_name,sp.spriden_id,''
              FROM PRCCUSR.SZRATTENDANCE ATT, SATURN.SPRIDEN SP
             WHERE SP.spriden_id= ATT.SZRATTENDANCE_SPRIDEN_ID
               AND ATT.SZRATTENDANCE_TERM_CODE=p_term_code
               AND ATT.SZRATTENDANCE_CRN=p_crn
               AND ATT.SZRATTENDANCE_ATTENDED='N'
               AND ATT.SZRATTENDANCE_ATTENDANCE_DATE=p_date
               AND spriden_change_ind IS NULL
             ORDER BY full_name;
               
        roster_position        NUMBER:=0;
    BEGIN
        bwckfrmt.p_open_doc(doc_name); 
        twbkwbis.P_DispInfo (gv_doc_attendance_application, 'DEFAULT');
        htp.script('function toggle(elementId) {
                        var ele = document.getElementById(elementId);
                        if(ele.style.display == "block") {
                                ele.style.display = "none";
                        } else {
                            ele.style.display = "block";
                        }
		} ','JavaScript');
		
		htp.script('function showAlert(){alert(''test'');}','JavaScript');
        
        p_create_table_params(p_term_code,p_crn,p_date);
        
        twbkfrmt.p_paragraph(3);
        twbkfrmt.p_printanchor('#','Hide/show Table',cattributes=>'onclick="toggle(''table_attendance'');"');
        
        p_create_attendance_table;
        OPEN attendance_cursor;
            LOOP
            FETCH attendance_cursor INTO attendance_rec;
            EXIT WHEN attendance_cursor%NOTFOUND;
            
             twbkfrmt.p_tablerowopen;
                    roster_position:=roster_position+1;           
                    twbkfrmt.p_tabledata (roster_position, NULL);
                     /*
                    IF attendance_rec.attendent IS NOT NULL
                    THEN
                        --twbkfrmt.p_tabledata (attendance_rec.attendent, NULL);
                       
                        IF attendance_rec.attendent ='N'
                        THEN 
                       
                            BWSKUTILS.p_tableDataOpen;
                            htp.formCheckbox('p_check','N-' || attendance_rec.spriden_id,NULL,'id="'||attendance_rec.spriden_id||'"');
                            BWSKUTILS.p_tableDataClose;
                        ELSE
                            BWSKUTILS.p_tableDataOpen;
                            htp.formCheckbox('p_check','Y-' || attendance_rec.spriden_id,'Checked','id="'||attendance_rec.spriden_id||'"');
                            BWSKUTILS.p_tableDataClose;
                        END IF;
                        
                    END IF;
                    */
                    
                    IF attendance_rec.full_name IS NOT NULL
                    THEN       
                        --twbkfrmt.p_tabledata(twbkfrmt.f_printanchor (gv_attendance_detail, attendance_rec.full_name, NULL ,cattributes=>'"window.open(''https://172.28.254.35:9000/TEST/bwskattendanceapp.P_AttendanceApplication'',''popup'',''width=600,height=600'')'));   
                        twbkfrmt.p_tabledata(twbkfrmt.f_printanchor (gv_attendance_detail ||'?p_term_code=' || p_term_code ||'&p_crn='||p_crn || '&p_pidm='||attendance_rec.spriden_id, attendance_rec.full_name, NULL ,cattributes=>'TARGET=_blank'));
                    END IF;
                    
                    IF attendance_rec.spriden_id IS NOT NULL
                    THEN
                         twbkfrmt.p_tabledata (attendance_rec.spriden_id, NULL);
                    END IF;
                     twbkfrmt.p_tablerowclose;
                     
                END LOOP;
               
                CLOSE attendance_cursor;  
            twbkfrmt.p_tableclose;   
            twbkfrmt.p_paragraph(3);
            
            twbkfrmt.p_printanchor('#','Hide/show Table',cattributes=>'onclick="toggle(''not_table_attendance'');"');
            
            p_create_not_attendance_table;
            roster_position:=0;
            OPEN not_attendance_cursor;
            LOOP
            FETCH not_attendance_cursor INTO attendance_rec;
            EXIT WHEN not_attendance_cursor%NOTFOUND;
            
             twbkfrmt.p_tablerowopen;
                    roster_position:=roster_position+1;               
                    twbkfrmt.p_tabledata (roster_position, NULL);
                    /*
                    IF attendance_rec.attendent IS NOT NULL
                    THEN
                        --twbkfrmt.p_tabledata (attendance_rec.attendent, NULL);
                        IF attendance_rec.attendent ='N'
                        THEN 
                       
                            BWSKUTILS.p_tableDataOpen;
                            htp.formCheckbox('p_check','N-' || attendance_rec.spriden_id,NULL,'id="'||attendance_rec.spriden_id||'"');
                            BWSKUTILS.p_tableDataClose;
                        ELSE
                            BWSKUTILS.p_tableDataOpen;
                            htp.formCheckbox('p_check','Y-' || attendance_rec.spriden_id,'Checked','id="'||attendance_rec.spriden_id||'"');
                            BWSKUTILS.p_tableDataClose;
                        END IF;
                        
                    END IF;
                    */
                    
                    IF attendance_rec.full_name IS NOT NULL
                    THEN
                         --twbkfrmt.p_tabledata (attendance_rec.full_name, NULL);
                         twbkfrmt.p_tabledata(twbkfrmt.f_printanchor (gv_attendance_detail ||'?p_term_code=' || p_term_code ||'&p_crn='||p_crn || '&p_pidm='||attendance_rec.spriden_id, attendance_rec.full_name, NULL ,cattributes=>'TARGET=_blank'));
                    END IF;
                    
                    IF attendance_rec.spriden_id IS NOT NULL
                    THEN
                         twbkfrmt.p_tabledata (attendance_rec.spriden_id, NULL);
                    END IF;
                    twbkfrmt.p_tablerowclose;
                END LOOP;
                
                twbkfrmt.p_tableclose;   
                CLOSE not_attendance_cursor;  
                twbkfrmt.p_paragraph(3);
                twbkfrmt.p_printanchor('twbkwbis.P_GenMenu?name=bmenu.P_FacMainMnu','Return To Menu',cattributes=>'onclick="toggle(''not_table_attendance'');"');
                
           htp.formClose;
    END p_displayListAttendance;                          
                                  
    PROCEDURE p_create_table_params   ( p_term_code                  IN general.gtvsdax.gtvsdax_external_code%TYPE,
                                        p_crn                        IN PRCCUSR.SZRATTENDANCE.SZRATTENDANCE_CRN%TYPE,
                                        p_date                       IN DATE)
    IS
    BEGIN
        BWSKUTILS.p_TableOpen('DATAENTRY');
        BWSKUTILS.p_tableRowOpen;
        BWSKUTILS.p_tableDataLabel('Term Code:&nbsp&nbsp&nbsp&nbsp');
        BWSKUTILS.p_tableDataLabel(p_term_code);
        BWSKUTILS.p_tableRowClose;
        
        BWSKUTILS.p_tableRowOpen;
        BWSKUTILS.p_tableDataLabel('CRN: ' );
        BWSKUTILS.p_tableDataLabel(p_crn || '-' || f_get_course_title(p_term_code,p_crn));
        BWSKUTILS.p_tableRowClose;
        
        IF p_date IS NOT NULL --comes from attendance app (Not from roster app)
        THEN       
            BWSKUTILS.p_tableRowOpen;
            BWSKUTILS.p_tableDataLabel('Date:  ');
            BWSKUTILS.p_tableDataLabel(TO_CHAR(p_date,'mm-dd-yyyy'));
            BWSKUTILS.p_tableRowClose;
            
            BWSKUTILS.p_tableRowOpen;
            BWSKUTILS.p_tableDataLabel('Unscheduled Meeting Date:  ');
            IF f_validate_attendant_date(p_term_code,p_crn,p_date)=TRUE
            THEN
                BWSKUTILS.p_tableDataLabel('No');
            ELSE
                BWSKUTILS.p_tableDataLabel('Yes');
            END IF;
        END IF;

        
        
        BWSKUTILS.p_tableRowClose;
        
        BWSKUTILS.p_TableClose;
    END p_create_table_params;    
    
    
    PROCEDURE p_create_table_student_details   ( p_term_code                  IN general.gtvsdax.gtvsdax_external_code%TYPE,
                                                 p_student_pidm               VARCHAR2,
                                                 p_student_name               VARCHAR2,
                                                 p_spriden_id                 VARCHAR2,
                                                 p_address                    VARCHAR2,
                                                 p_cred_hours                 VARCHAR2)
    IS
    BEGIN
        BWSKUTILS.p_TableOpen('DATAENTRY');
        BWSKUTILS.p_tableRowOpen;
        BWSKUTILS.p_tableDataLabel('Term Code:&nbsp&nbsp&nbsp&nbsp');
        BWSKUTILS.p_tableDataLabel(p_term_code);
        BWSKUTILS.p_tableRowClose;
              
        /*
        BWSKUTILS.p_tableRowOpen;
        BWSKUTILS.p_tableDataLabel('PIDM:  ');
        BWSKUTILS.p_tableDataLabel(p_student_pidm);
        BWSKUTILS.p_tableRowClose;
        */
        
        BWSKUTILS.p_tableRowOpen;
        BWSKUTILS.p_tableDataLabel('Student Name:  ');
        BWSKUTILS.p_tableDataLabel(p_student_name);
        BWSKUTILS.p_tableRowClose;
        
        BWSKUTILS.p_tableRowOpen;
        BWSKUTILS.p_tableDataLabel('Student ID:  ');
        BWSKUTILS.p_tableDataLabel(p_spriden_id);
        BWSKUTILS.p_tableRowClose;
        
        BWSKUTILS.p_tableRowOpen;
        BWSKUTILS.p_tableDataLabel('Address:  ');
        BWSKUTILS.p_tableDataLabel(p_address);
        BWSKUTILS.p_tableRowClose;
        
        BWSKUTILS.p_tableRowOpen;
        BWSKUTILS.p_tableDataLabel('Total Hours:  ');
        BWSKUTILS.p_tableDataLabel(p_cred_hours);
        BWSKUTILS.p_tableRowClose;
 
        BWSKUTILS.p_TableClose;
    END p_create_table_student_details;    
    
                   

    PROCEDURE p_displayListForUpdate (doc_name    VARCHAR2 DEFAULT NULL,
                                  p_term_code IN general.gtvsdax.gtvsdax_external_code%TYPE,
                                  p_crn       IN PRCCUSR.SZRATTENDANCE.SZRATTENDANCE_CRN%TYPE,
                                  p_date      IN DATE)
    IS
         CURSOR attendance_cursor
         IS
            --SELECT rownum,ATT.SZRATTENDANCE_ATTENDED, spriden_first_name ||	decode(spriden_mi, null, ' ', ' ' || spriden_mi || ', ')||spriden_last_name full_name,sp.spriden_id
            SELECT DISTINCT ATT.SZRATTENDANCE_ATTENDED, spriden_first_name ||' '|| decode(spriden_mi, null, '', '' || spriden_mi || ' ') || spriden_last_name full_name,sp.spriden_id,TO_CHAR(LDA.SZRATTLDA_ATTENDANCE_DATE,'mm-dd-yyyy')
             FROM PRCCUSR.SZRATTENDANCE ATT, SATURN.SPRIDEN SP, PRCCUSR.SZRATTLDA  LDA
             WHERE SP.spriden_ID                     = ATT.SZRATTENDANCE_SPRIDEN_ID
               AND ATT.SZRATTENDANCE_TERM_CODE       = p_term_code
               AND ATT.SZRATTENDANCE_CRN             = p_crn
               AND ATT.SZRATTENDANCE_ATTENDANCE_DATE = p_date
               AND LDA.SZRATTLDA_SPRIDEN_ID          = ATT.SZRATTENDANCE_SPRIDEN_ID
               AND LDA.SZRATTLDA_TERM_CODE           = ATT.SZRATTENDANCE_TERM_CODE  
               AND LDA.SZRATTLDA_CRN                 = ATT.SZRATTENDANCE_CRN  
               AND spriden_change_ind IS NULL
             ORDER BY full_name;
       roster_position        NUMBER:=0;
       
    BEGIN
        bwckfrmt.p_open_doc(doc_name);
        
        IF(f_is_valid_unscheduled_date(p_term_code,p_crn,p_date)<>TRUE)
        THEN
             --twbkfrmt.p_paragraph(2);
             BWSKUTILS.p_printError('Please Select the proper CRN or Please Select a date between ' || f_get_course_start_date(p_term_code,p_crn)|| ' and '  ||f_get_course_end_date(p_term_code,p_crn) );
             twbkfrmt.p_tableopen; 
             twbkfrmt.p_tablerowopen;
             twbkfrmt.p_tabledata(twbkfrmt.f_printanchor (gv_doc_attendance_application ||'?p_term_code=' || p_term_code ||'&p_crn='||p_crn,'GO BACK'  , NULL ));
             twbkfrmt.p_tablerowclose;
             twbkfrmt.p_tableclose; 
        ELSE
        

       

            htp.script('function toggleCheckBoxes(FieldName, CheckValue,event){
                           
                            if(!document.forms[0]){
                                alert(''Wrong Form Name'');
                                return;
                            }
                          
                            var objCheckBoxes = document.forms[0].elements[FieldName];
                            if(!objCheckBoxes)
                                return;
                            var countCheckBoxes = objCheckBoxes.length;
                            if(!countCheckBoxes)
                                objCheckBoxes.checked = CheckValue;
                            else
                                for(var i = 0; i < countCheckBoxes; i++){
                                     objCheckBoxes[i].checked = CheckValue;
                                }
                                   
                             event.preventDefault();
                            
                           
                            
                        }','JavaScript');
                        
                        
             htp.script('// Returns an array with values of the selected (checked) checkboxes in "frm"
                        function getSelectedChbox(event) {
                        // JavaScript & jQuery Course - http://courseswebnet/javascript/
                        var selchbox = [];        // array that will store the value of selected checkboxes

                        // gets all the input tags in frm, and their number
                        var inpfields = document.getElementById("frm_roster_update").getElementsByTagName(''input'');
                        var nr_inpfields = inpfields.length;
                        // traverse the inpfields elements, and adds the value of selected (checked) checkbox in selchbox
                        for(var i=0; i<nr_inpfields; i++) {
                            if(inpfields[i].type == ''checkbox'' && inpfields[i].checked == true) selchbox.push(inpfields[i].value);
                        }
                        //alert(selchbox);
                        document.getElementById("p_spriden_list").value = selchbox;
                        //alert(document.getElementById("p_spriden_list").value);
                        var text="Are you sure you want to mark ".concat(selchbox.length);
                        if(selchbox.length>1){
                            text=text.concat(" students present?");
                        }else{
                            text=text.concat(" student present?");
                        }    
                        
                        if (confirm(text)){
                           return true;
                        }else{
                            event.preventDefault(); 
                            return false;
                        }
                        return selchbox;
                        }','JavaScript');
             /*           
             htp.p(' 
                <style>
                mybutton:hover, h1:hover, a:hover {
                background-color: yellow;
                }</style>
                ');
            */           
                        
            htp.p  ('Please check the attended box for students who were present in class and '  );
            htp.p  ('uncheck the attended box for absent students and press SUBMIT. '  );
         
            twbkfrmt.p_paragraph(2);
            BWSKUTILS.p_formOpen(gv_doc_attendance_application, 'POST' ,'id="frm_roster_update" name="frm_roster_update" ');  
           
            twbkfrmt.P_FormHidden ('p_spriden_list', '','id="p_spriden_list"');
            twbkfrmt.P_FormHidden ('p_term_code', p_term_code);    
            twbkfrmt.P_FormHidden ('p_crn', p_crn);     
            twbkfrmt.P_FormHidden ('p_date', p_date);   
            --BWSKUTILS.p_dispSectionTitle('Select Term, CRN and Start Date', extraWhiteSpace_in => 'YES');
            P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','p_displayListForUpdate','Before create Table Params Term_code: '||p_term_code || ' crn: ' || p_crn || ' date:' || p_date  ,MESSAGE_LEVEL_INFO,gv_session_step_number) ;
            p_create_table_params(p_term_code,p_crn,p_date);
            p_create_table;
            
            OPEN attendance_cursor;
            
            LOOP
                FETCH attendance_cursor INTO attendance_rec;
                EXIT WHEN attendance_cursor%NOTFOUND;
                        IF (f_is_valid_withdraw_date(p_term_code,p_crn,attendance_rec.spriden_id,p_date)) = TRUE
                        THEN
                            twbkfrmt.p_tablerowopen;
                                roster_position:=roster_position+1;           
                                twbkfrmt.p_tabledata (roster_position, NULL);
                                IF attendance_rec.attendent IS NOT NULL
                                THEN
                                    --twbkfrmt.p_tabledata (attendance_rec.attendent, NULL);
                                    
                                    IF attendance_rec.attendent <>STATUS_TYPE_ATTENDANT --TEMPORAL OR NOT ATTENDEND
                                    THEN 
                                   
                                        BWSKUTILS.p_tableDataOpen;
                                        htp.formCheckbox('p_check',STATUS_TYPE_NOT_ATTENDANT||'-' || attendance_rec.spriden_id,NULL,'id="'||attendance_rec.spriden_id||'"');
                                        BWSKUTILS.p_tableDataClose;
                                    ELSE
                                        BWSKUTILS.p_tableDataOpen;
                                        htp.formCheckbox('p_check',STATUS_TYPE_ATTENDANT ||'-' || attendance_rec.spriden_id,'Checked','id="'||attendance_rec.spriden_id||'"');
                                        BWSKUTILS.p_tableDataClose;
                                    END IF;
                                                           
                                END IF;
                                
                                IF attendance_rec.full_name IS NOT NULL
                                THEN
                                     --twbkfrmt.p_tabledata (attendance_rec.full_name, NULL);
                                       twbkfrmt.p_tabledata(twbkfrmt.f_printanchor (gv_attendance_detail ||'?p_term_code=' || p_term_code ||'&p_crn='||p_crn || '&p_pidm='||attendance_rec.spriden_id, attendance_rec.full_name, NULL ,cattributes=>'TARGET=_blank'));
                                END IF;
                                
                                IF attendance_rec.spriden_id IS NOT NULL
                                THEN
                                     twbkfrmt.p_tabledata (attendance_rec.spriden_id, NULL);
                                END IF;
                                
                                IF attendance_rec.lda IS NOT NULL
                                THEN
                                     twbkfrmt.p_tabledata (attendance_rec.lda, NULL);
                                END IF;
                                
                            twbkfrmt.p_tablerowclose;
                        ELSE
                            P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','p_displayListForUpdate','Preventing Inserting student id: '||attendance_rec.spriden_id || ' Invalid Withdraw Date '  ,MESSAGE_LEVEL_DEBUG,gv_session_step_number) ;        
                        END IF;
                        
                   
               END LOOP;
            
               CLOSE attendance_cursor;
               BWSKUTILS.p_TableClose;
               
              -- class="whitespace2"
              -- HTP.formSubmit ('p_button_uncheck', 'Unselect All','onclick="toggleCheckBoxes(''p_check'',false,event);" id="id_button_uncheck"' );
               --url(images/default-button-sprites-small.png) no-repeat left 0px
               --style="display: none;" 
               --background-image: url("images/default-button-sprites-small.png"
               
               --button_style := button_style || ' button:hover {background-color: yellow;} ';

               --HTP.P('<p class="whitespace2">');
               HTP.P('<INPUT class="mybutton" '  || gv_local_style || ' TYPE="button"  NAME="p_button_uncheck" value="Unselect All" onclick="toggleCheckBoxes(''p_check'',false,event);" id="id_button_uncheck">');
               HTP.P('<INPUT '  || gv_local_style || ' TYPE="button"  NAME="p_button_check"   value="Select All" onclick="toggleCheckBoxes(''p_check'',true,event);" id="id_button_check">');    
               --HTP.P('</p>');
               -- HTP.formSubmit ('p_button_check', 'Select All','onclick="toggleCheckBoxes(''p_check'',true,event);" id="id_button_check"' );
               twbkfrmt.p_paragraph(2);
               HTP.formSubmit ('p_save_button', 'Submit','onclick="getSelectedChbox(event);" id="id_button_save"' );
               --HTP.P('<INPUT TYPE="SUBMIT"  NAME="p_save_button"   value="SubmitDDD" onclick="getSelectedChbox(event);" id="id_button_save">');
              
        END IF;
    END p_displayListForUpdate;

   
    PROCEDURE p_create_table 
    IS
    BEGIN
         twbkfrmt.p_tableopen (
             'DATADISPLAY',
             ccaption=> G$_NLS.Get ('BWGKOEM1-0050', 'SQL', 'Table Name'),
             cattributes=> G$_NLS.Get ('BWGKOEM1-0051',
              'SQL',
              'SUMMARY="This table shows the student information."')
             );
             twbkfrmt.p_tablerowopen;
             twbkfrmt.p_tabledataheader (
             G$_NLS.Get ('BWGKOEM1-0052', 'SQL', 'Seq'),
             NULL,
             NULL,
             'TRUE',
             NULL
             );
             
             twbkfrmt.p_tabledataheader (
             G$_NLS.Get ('BWGKOEM1-0052', 'SQL', 'Attended'),
             NULL,
             NULL,
             'TRUE',
             NULL
             );
             
             twbkfrmt.p_tabledataheader (
             G$_NLS.Get ('BWGKOEM1-0052', 'SQL', 'Name'),
             NULL,
             NULL,
             'TRUE',
             NULL
             );
             twbkfrmt.p_tabledataheader (
             G$_NLS.Get ('BWGKOEM1-0052', 'SQL', 'Id'),
             NULL,
             NULL,
             'TRUE',
             NULL
             );
             
             twbkfrmt.p_tabledataheader (
             G$_NLS.Get ('BWGKOEM1-0053', 'SQL', 'LDA'),
             NULL,
             NULL,
             'TRUE',
             NULL
             );
             
        twbkfrmt.p_tablerowclose;
    END;
    
    
    PROCEDURE p_create_attendance_table 
    IS 
       
    BEGIN
        
         
         twbkfrmt.p_tableopen (
             'DATADISPLAY',
             ccaption=> G$_NLS.Get ('BWGKOEM1-0050', 'SQL', 'Students Present in Class'),
             cattributes=> G$_NLS.Get ('BWGKOEM1-0051',
              'SQL',
              'SUMMARY="This table shows the student information."  id="table_attendance" ' )  
             );
             twbkfrmt.p_tablerowopen;
             twbkfrmt.p_tabledataheader (
             G$_NLS.Get ('BWGKOEM1-0052', 'SQL', 'SEQ'),
             NULL,
             NULL,
             'TRUE',
             NULL
             );
            /*
            twbkfrmt.p_tabledataheader (
             G$_NLS.Get ('BWGKOEM1-0052', 'SQL', 'ATTENDED'),
             NULL,
             NULL,
             'TRUE',
             NULL
             );
             */
             
             twbkfrmt.p_tabledataheader (
             G$_NLS.Get ('BWGKOEM1-0052', 'SQL', 'NAME'),
             NULL,
             NULL,
             'TRUE',
             NULL
             );
             
             twbkfrmt.p_tabledataheader (
             G$_NLS.Get ('BWGKOEM1-0052', 'SQL', 'ID'),
             NULL,
             NULL,
             'TRUE',
             NULL
             );
            
        twbkfrmt.p_tablerowclose;
    END;
    
    
    PROCEDURE p_create_not_attendance_table 
    IS 
       
    BEGIN
        
        
         twbkfrmt.p_tableopen (
             'DATADISPLAY',
             ccaption=> G$_NLS.Get ('BWGKOEM1-0050', 'SQL', 'Students Not Present in Class' ),
             cattributes=> G$_NLS.Get ('BWGKOEM1-0051' ,
              'SQL',
              'SUMMARY="This table shows the student information."  id="not_table_attendance" ' )  
             );
             twbkfrmt.p_tablerowopen;
             twbkfrmt.p_tabledataheader (
             G$_NLS.Get ('BWGKOEM1-0052', 'SQL', 'SEQ'),
             NULL,
             NULL,
             'TRUE',
             NULL
             );
            /*
             twbkfrmt.p_tabledataheader (
             G$_NLS.Get ('BWGKOEM1-0052', 'SQL', 'ATTENDED'),
             NULL,
             NULL,
             'TRUE',
             NULL
             );
             */
             twbkfrmt.p_tabledataheader (
             G$_NLS.Get ('BWGKOEM1-0052', 'SQL', 'NAME'),
             NULL,
             NULL,
             'TRUE',
             NULL
             );
             
             twbkfrmt.p_tabledataheader (
             G$_NLS.Get ('BWGKOEM1-0052', 'SQL', 'ID'),
             NULL,
             NULL,
             'TRUE',
             NULL
             );
            
        twbkfrmt.p_tablerowclose;
    END;
    

    PROCEDURE p_dispInfoText
        (	header_text_in in varchar2
        ,	label_in in varchar2
        ,	scrollable_in boolean default false
        ,	extraWhiteSpace_in in varchar2 default null
          )
    IS
        CURSOR cu_infoText(label_in varchar2)
        IS
            SELECT	twgrinfo_text info_text
              FROM	wtailor.twgrinfo
             WHERE	twgrinfo_name = gv_doc_attendance_application
               AND  UPPER(twgrinfo_label) = UPPER(label_in)
               AND	twgrinfo_source_ind = 'L'
             ORDER	BY twgrinfo_sequence
        ;
    BEGIN
        BWSKUTILS.p_dispSectionTitle(header_text_in, extraWhiteSpace_in => extraWhiteSpace_in);

        if scrollable_in then
            htp.p('<div style="height:149px;border:1px solid #ccc; overflow-y:scroll">');
        end if;
        for lv_rec in cu_infoText(label_in)
        loop
            BWSKUTILS.p_printText(lv_rec.info_text);
        end loop;
        if scrollable_in then
            htp.p('</div>');
        end if;
        -- create some space after the text
        htp.p('<p>');

    END p_dispInfoText;


    PROCEDURE p_dispInfoTextRoster
        (	header_text_in in varchar2
        ,	label_in in varchar2
        ,	scrollable_in boolean default false
        ,	extraWhiteSpace_in in varchar2 default null
          )
    IS
        CURSOR cu_infoText(label_in varchar2)
        IS
            SELECT	twgrinfo_text info_text
              FROM	wtailor.twgrinfo
             WHERE	twgrinfo_name = gv_doc_attendance_roster
               AND  UPPER(twgrinfo_label) = UPPER(label_in)
               AND	twgrinfo_source_ind = 'L'
             ORDER	BY twgrinfo_sequence
        ;
    BEGIN
        BWSKUTILS.p_dispSectionTitle(header_text_in, extraWhiteSpace_in => extraWhiteSpace_in);

        if scrollable_in then
            htp.p('<div style="height:149px;border:1px solid #ccc; overflow-y:scroll">');
        end if;
        for lv_rec in cu_infoText(label_in)
        loop
            BWSKUTILS.p_printText(lv_rec.info_text);
        end loop;
        if scrollable_in then
            htp.p('</div>');
        end if;
        -- create some space after the text
        htp.p('<p>');

    END p_dispInfoTextRoster;
    
    /*
    This is for not regular meeting dates
    */
    PROCEDURE P_Insert_Attendance(  p_term_code              IN general.gtvsdax.gtvsdax_external_code%TYPE,
                                    p_crn                    IN PRCCUSR.SZRATTENDANCE.SZRATTENDANCE_crn%TYPE, 
                                    p_attendance_date        IN PRCCUSR.SZRATTENDANCE.SZRATTENDANCE_ATTENDANCE_DATE%TYPE )
    IS
        CURSOR roster_cursor
        IS
            SELECT DISTINCT S.SPRIDEN_ID SPRIDEN_ID,S.SPRIDEN_PIDM PIDM
              FROM SFRSTCR, SSBSECT, SCBCRSE A, SSRMEET, SIRASGN, SPRIDEN I, SPRIDEN S
             WHERE SFRSTCR_RSTS_CODE IN ('RE','AU','W')
                AND SFRSTCR_TERM_CODE  = p_term_code
                AND SFRSTCR_CRN        = p_crn
                AND SFRSTCR_TERM_CODE  = SSBSECT_TERM_CODE
                AND SFRSTCR_CRN        = SSBSECT_CRN
                AND SSBSECT_SUBJ_CODE  = A.SCBCRSE_SUBJ_CODE
                AND SSBSECT_CRSE_NUMB  = A.SCBCRSE_CRSE_NUMB
                AND A.SCBCRSE_EFF_TERM = (SELECT MAX(SCBCRSE_EFF_TERM)
                                            FROM SCBCRSE B
                                            WHERE B.SCBCRSE_SUBJ_CODE = A.SCBCRSE_SUBJ_CODE
                                              AND B.SCBCRSE_CRSE_NUMB = A.SCBCRSE_CRSE_NUMB)
                AND SSRMEET_TERM_CODE  = SFRSTCR_TERM_CODE
                AND SSRMEET_CRN        = SFRSTCR_CRN
                AND SIRASGN_TERM_CODE  = SFRSTCR_TERM_CODE
                AND SIRASGN_CRN        = SFRSTCR_CRN
                AND I.SPRIDEN_PIDM(+)  = SIRASGN_PIDM
                AND I.SPRIDEN_CHANGE_IND IS NULL
                AND S.SPRIDEN_PIDM(+)  = SFRSTCR_PIDM
                AND S.SPRIDEN_CHANGE_IND IS NULL
                ORDER BY  S.SPRIDEN_ID;   
        count_exist              NUMBER;
    BEGIN
        P_Insert_Log('Attendance App','BANINST1.bwskattendanceapp','P_Insert_Attendance','Inserting record. term_code:'|| p_term_code ||  ' crn: ' || p_crn || ' p_attendance_date:' || p_attendance_date ,MESSAGE_LEVEL_INFO,gv_session_step_number) ;
        BEGIN
            IF(f_is_valid_unscheduled_date(p_term_code,p_crn,p_attendance_date)=TRUE)
            THEN
                OPEN roster_cursor;
                LOOP
                FETCH roster_cursor INTO students_rec;
                EXIT WHEN roster_cursor%NOTFOUND; 
                    P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','P_Insert_Attendance','Entering Loop..' || students_rec.student_id,MESSAGE_LEVEL_DEBUG,gv_session_step_number) ;
                    SELECT COUNT(*)
                      INTO count_exist
                      FROM PRCCUSR.SZRATTENDANCE
                     WHERE SZRATTENDANCE.SZRATTENDANCE_SPRIDEN_ID =students_rec.student_id
                       AND SZRATTENDANCE_TERM_CODE=p_term_code
                       AND SZRATTENDANCE_CRN=p_crn
                       AND SZRATTENDANCE_ATTENDANCE_DATE =p_attendance_date;
                    
                    IF count_exist = 0 
                    THEN
                        
                        IF (f_is_valid_withdraw_date(p_term_code,p_crn,students_rec.student_id,p_attendance_date)) = TRUE
                        THEN
                            INSERT INTO PRCCUSR.SZRATTENDANCE (SZRATTENDANCE_SPRIDEN_ID,SZRATTENDANCE_PIDM,SZRATTENDANCE_TERM_CODE,SZRATTENDANCE_CRN,SZRATTENDANCE_ATTENDANCE_DATE,SZRATTENDANCE_REGULAR_MEETING,SZRATTENDANCE_ATTENDED,SZRATTENDANCE_CREATION_DATE,SZRATTENDANCE_UPDATE_DATE)
                            VALUES (students_rec.student_id,students_rec.pidm,p_term_code,p_crn,p_attendance_date,NOT_REGULAR_MEETING,STATUS_TYPE_NOT_ATTENDANT,SYSDATE,NULL);
                        ELSE
                            INSERT INTO PRCCUSR.SZRATTENDANCE (SZRATTENDANCE_SPRIDEN_ID,SZRATTENDANCE_PIDM,SZRATTENDANCE_TERM_CODE,SZRATTENDANCE_CRN,SZRATTENDANCE_ATTENDANCE_DATE,SZRATTENDANCE_REGULAR_MEETING,SZRATTENDANCE_ATTENDED,SZRATTENDANCE_CREATION_DATE,SZRATTENDANCE_UPDATE_DATE)
                            VALUES (students_rec.student_id,students_rec.pidm,p_term_code,p_crn,p_attendance_date,NOT_REGULAR_MEETING,STATUS_TYPE_WITHDRAW,SYSDATE,NULL);
                        
                        END IF;
                    END IF;
                    
                    /*INSERTING DATA IN RELATED TABLE*/
                    SELECT COUNT(*)
                      INTO count_exist
                      FROM PRCCUSR.SZRATTLDA
                     WHERE SZRATTLDA.SZRATTLDA_SPRIDEN_ID =students_rec.student_id
                       AND SZRATTLDA_TERM_CODE=p_term_code
                       AND SZRATTLDA_CRN=p_crn;
                       
                    IF count_exist = 0 
                    THEN
                        INSERT INTO PRCCUSR.SZRATTLDA(SZRATTLDA_SPRIDEN_ID,SZRATTLDA_TERM_CODE,SZRATTLDA_CRN,SZRATTLDA_ATTENDANCE_DATE,SZRATTLDA_INSERT_DATE,SZRATTLDA_INSERTED_BY,SZRATTLDA_UPDATE_DATE,SZRATTLDA_UPDATED_BY)
                        VALUES(students_rec.student_id,p_term_code,p_crn,NULL,SYSDATE,global_pidm,NULL,NULL);
                    END IF;
                END LOOP;
                CLOSE roster_cursor;
                COMMIT;
            END IF;
            

  
        EXCEPTION 
        WHEN dup_val_on_index THEN
             P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','P_Insert_Attendance',' term_code:'|| p_term_code || ' crn: ' || p_crn  ,MESSAGE_LEVEL_ERROR,gv_session_step_number) ;
        END;
    END P_Insert_Attendance;


    PROCEDURE p_update_LDA (p_spriden_list VARCHAR2,  p_term_code IN GENERAL.GTVSDAX.gtvsdax_external_code%TYPE, p_crn IN SSBSECT.ssbsect_crn%TYPE)
    IS
        CURSOR CUR IS
        WITH qry AS (
                SELECT p_spriden_list p_list
                    FROM dual
        )
         SELECT regexp_substr(p_spriden_list,'[^,]+',1,ROWNUM) list_values
           FROM qry
        CONNECT BY LEVEL <=LENGTH(regexp_replace(p_list,'[^,]+'))+1;
        row_value          VARCHAR2(10);
        row_spriden_id     SPRIDEN.SPRIDEN_ID%TYPE;
    BEGIN
        --P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','p_update_LDA','Updating Spriden_id: ' ||row_spriden_id || ' New value: '||row_value ,MESSAGE_LEVEL_INFO,gv_session_step_number) ;
        FOR rec In cur LOOP
            P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','p_update_LDA','Updating Spriden_id: rec ' || rec.list_values ,MESSAGE_LEVEL_DEBUG,gv_session_step_number) ;
            SELECT SUBSTR(rec.list_values,0,INSTR(rec.list_values,'=',1)-1),SUBSTR(rec.list_values,INSTR(rec.list_values,'=',1)+1,LENGTH(rec.list_values))
              INTO row_spriden_id, row_value
              FROM DUAL;
                        
            P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','p_update_LDA','Updating Spriden_id: ' ||row_spriden_id || ' New value: '||row_value ,MESSAGE_LEVEL_DEBUG,gv_session_step_number) ;
            UPDATE PRCCUSR.SZRATTLDA
               SET SZRATTLDA_ATTENDANCE_DATE=TO_DATE(row_value,'mm/dd/yyyy')
             WHERE SZRATTLDA_SPRIDEN_ID = row_spriden_id
               AND SZRATTLDA_TERM_CODE=p_term_code
               AND SZRATTLDA_CRN=p_crn;
            COMMIT;
            
            P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','p_update_LDA','Updating Spriden_id: ' ||row_spriden_id || ' New value: '||row_value || 'term_code: '||p_term_code || ' crn: ' || p_crn ,MESSAGE_LEVEL_INFO,gv_session_step_number) ;
        END LOOP;
    END p_update_LDA;
    
    PROCEDURE p_update_rows (p_spriden_list VARCHAR2,  
                             p_term_code IN GENERAL.GTVSDAX.gtvsdax_external_code%TYPE, 
                             p_crn IN SSBSECT.ssbsect_crn%TYPE,p_date  DATE)
    IS
        CURSOR CUR IS
        WITH qry AS (
                SELECT p_spriden_list p_list
                    FROM dual
        )
         SELECT regexp_substr(p_spriden_list,'[^,]+',1,ROWNUM) list_values
           FROM qry
        CONNECT BY LEVEL <=LENGTH(regexp_replace(p_list,'[^,]+'))+1;
        row_value          VARCHAR2(1);
        row_spriden_id     SPRIDEN.SPRIDEN_ID%TYPE;
    BEGIN
    
         UPDATE PRCCUSR.SZRATTENDANCE
           SET SZRATTENDANCE_ATTENDED=STATUS_TYPE_NOT_ATTENDANT, --'N'
		       SZRATTENDANCE_UPDATE_DATE=SYSDATE
         WHERE SZRATTENDANCE_TERM_CODE=p_term_code
           AND SZRATTENDANCE_CRN=p_crn
           AND SZRATTENDANCE_ATTENDANCE_DATE=p_date
           AND SZRATTENDANCE_ATTENDED <> STATUS_TYPE_WITHDRAW;
           
        COMMIT;       
        
        FOR rec In cur LOOP
            row_value := substr(rec.list_values,1,1);
            row_spriden_id := substr(rec.list_values,3,length(rec.list_values));
            P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','p_update_rows','Updating Spriden_id: ' ||row_spriden_id || ' New value: '||row_value ,MESSAGE_LEVEL_DEBUG,gv_session_step_number) ;
            UPDATE PRCCUSR.SZRATTENDANCE
               SET SZRATTENDANCE_ATTENDED=STATUS_TYPE_ATTENDANT,  --'Y'
			       SZRATTENDANCE_UPDATE_DATE=SYSDATE
             WHERE SZRATTENDANCE_SPRIDEN_ID = row_spriden_id
               AND SZRATTENDANCE_TERM_CODE=p_term_code
               AND SZRATTENDANCE_CRN=p_crn
               AND SZRATTENDANCE_ATTENDANCE_DATE=p_date
               ;
            COMMIT;
            P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','p_update_rows','Updating Spriden_id: ' ||row_spriden_id || ' New value: '||row_value || 'term_code: '||p_term_code || ' crn: ' || p_crn ,MESSAGE_LEVEL_DEBUG,gv_session_step_number) ;
        END LOOP;
        
    END p_update_rows;
    
    
    
    PROCEDURE p_update_roster_lda (p_spriden_list VARCHAR2,  p_term_code IN GENERAL.GTVSDAX.gtvsdax_external_code%TYPE, p_crn IN SSBSECT.ssbsect_crn%TYPE,p_date  DATE)
    IS
        CURSOR CUR IS
        WITH qry AS (
                SELECT p_spriden_list p_list
                    FROM dual
        )
         SELECT regexp_substr(p_spriden_list,'[^,]+',1,ROWNUM) list_values
           FROM qry
        CONNECT BY LEVEL <=LENGTH(regexp_replace(p_list,'[^,]+'))+1;
        row_value          VARCHAR2(1);
        row_spriden_id     SPRIDEN.SPRIDEN_ID%TYPE;
    BEGIN
    
         UPDATE PRCCUSR.SZRATTENDANCE
           SET SZRATTENDANCE_ATTENDED=STATUS_TYPE_NOT_ATTENDANT,
		       SZRATTENDANCE_UPDATE_DATE=SYSDATE
         WHERE SZRATTENDANCE_TERM_CODE=p_term_code
           AND SZRATTENDANCE_CRN=p_crn
           AND SZRATTENDANCE_ATTENDANCE_DATE=p_date
           ;
        COMMIT;       
        
        FOR rec In cur LOOP
            row_value := substr(rec.list_values,1,1);
            row_spriden_id := substr(rec.list_values,3,length(rec.list_values));
            P_Insert_Log('Attendance App','BANINST1.bwskattendanceapp','p_update_rows','Updating Spriden_id: ' ||row_spriden_id || ' New value: '||row_value ,MESSAGE_LEVEL_DEBUG,gv_session_step_number) ;
            UPDATE PRCCUSR.SZRATTENDANCE
               SET SZRATTENDANCE_ATTENDED=STATUS_TYPE_ATTENDANT,
			       SZRATTENDANCE_UPDATE_DATE=SYSDATE
             WHERE SZRATTENDANCE_SPRIDEN_ID = row_spriden_id
               AND SZRATTENDANCE_TERM_CODE=p_term_code
               AND SZRATTENDANCE_CRN=p_crn
               AND SZRATTENDANCE_ATTENDANCE_DATE=p_date
               ;
            COMMIT;
            P_Insert_Log('Attendance App','BANINST1.bwskattendanceapp','p_update_rows','Updating Spriden_id: ' ||row_spriden_id || ' New value: '||row_value || 'term_code: '||p_term_code || ' crn: ' || p_crn ,MESSAGE_LEVEL_DEBUG,gv_session_step_number) ;
        END LOOP;
        
    END p_update_roster_lda;
   
    /**
    Procedure to retrieve the roster from below tables. 
    */
    
	
    
    PROCEDURE P_CreateRoster   (p_term_code	      IN GENERAL.GTVSDAX.gtvsdax_external_code%TYPE,
                                p_crn             IN SSBSECT.ssbsect_crn%TYPE  
                                )
                                       
    IS
        TYPE hash_map IS TABLE OF   VARCHAR2(100) INDEX BY VARCHAR2(10);
        TYPE DatesCurTyp            IS REF CURSOR;
        v_dates_cursor              DatesCurTyp;
        hmap_days hash_map;
        i                           VARCHAR2(64);
        sql_stmt                    VARCHAR2(1000);
        start_date                  VARCHAR2(15);
        end_date                    VARCHAR2(15);--:='''01/01/2018''';
        str_days                    VARCHAR2(100);
        
        count_exist                 NUMBER;
        TYPE dates_local IS RECORD
        (
           day_name                 VARCHAR2(20),
           day_date                 VARCHAR2(20)
        );
        
        dates_rec       dates_local;
        is_valid_withdraw_date      BOOLEAN:=FALSE;
        local_date                  DATE;
        CURSOR roster_cursor
        IS
            SELECT DISTINCT S.SPRIDEN_ID SPRIDEN_ID,S.SPRIDEN_PIDM PIDM
              FROM SFRSTCR, SSBSECT, SCBCRSE A, SSRMEET, SIRASGN, SPRIDEN I, SPRIDEN S
             WHERE SFRSTCR_RSTS_CODE IN ('RE','AU','W')
                AND SFRSTCR_TERM_CODE  = p_term_code
                AND SFRSTCR_CRN        = p_crn
                AND SFRSTCR_TERM_CODE  = SSBSECT_TERM_CODE
                AND SFRSTCR_CRN        = SSBSECT_CRN
                AND SSBSECT_SUBJ_CODE  = A.SCBCRSE_SUBJ_CODE
                AND SSBSECT_CRSE_NUMB  = A.SCBCRSE_CRSE_NUMB
                AND A.SCBCRSE_EFF_TERM = (SELECT MAX(SCBCRSE_EFF_TERM)
                                            FROM SCBCRSE B
                                            WHERE B.SCBCRSE_SUBJ_CODE = A.SCBCRSE_SUBJ_CODE
                                              AND B.SCBCRSE_CRSE_NUMB = A.SCBCRSE_CRSE_NUMB)
                AND SSRMEET_TERM_CODE  = SFRSTCR_TERM_CODE
                AND SSRMEET_CRN        = SFRSTCR_CRN
                AND SIRASGN_TERM_CODE  = SFRSTCR_TERM_CODE
                AND SIRASGN_CRN        = SFRSTCR_CRN
                AND I.SPRIDEN_PIDM(+)  = SIRASGN_PIDM
                AND I.SPRIDEN_CHANGE_IND IS NULL
                AND S.SPRIDEN_PIDM(+)  = SFRSTCR_PIDM
                AND S.SPRIDEN_CHANGE_IND IS NULL
                ORDER BY  S.SPRIDEN_ID;   
        
    BEGIN
        P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','P_CreateRoster','Retrieving day names.' ,MESSAGE_LEVEL_DEBUG,gv_session_step_number) ;
        hmap_days('SUNDAY')   :='NO';
        hmap_days('MONDAY')   :='NO';
        hmap_days('TUESDAY')  :='NO';
        hmap_days('WEDNESDAY'):='NO';
        hmap_days('THURSDAY') :='NO';
        hmap_days('FRIDAY')   :='NO';
        hmap_days('SATURDAY') :='NO';
        P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','P_retrieve_course_days','HashMap Initialized ' ,MESSAGE_LEVEL_DEBUG,gv_session_step_number) ;
        
        FOR item_day_name IN(SELECT SSRMEET_SUN_DAY,SSRMEET_MON_DAY,SSRMEET_TUE_DAY,SSRMEET_WED_DAY,SSRMEET_THU_DAY,SSRMEET_FRI_DAY,SSRMEET_SAT_DAY
                           FROM SSRMEET
                          WHERE SSRMEET_TERM_CODE=p_term_code
                            AND SSRMEET_CRN=p_crn
                        )
        LOOP
            
            
            IF  hmap_days('SUNDAY') <>'YES'
            THEN
                IF item_day_name.SSRMEET_SUN_DAY IS NOT NULL
                THEN 
                    hmap_days('SUNDAY'):='YES';
                END IF;
            END IF; 
            
            IF  hmap_days('MONDAY') <>'YES'
            THEN
                IF item_day_name.SSRMEET_MON_DAY IS NOT NULL
                THEN
                    hmap_days('MONDAY'):='YES';
                END IF;
            END IF; 
            
            IF  hmap_days('TUESDAY') <>'YES'
            THEN
                IF item_day_name.SSRMEET_TUE_DAY IS NOT NULL
                THEN
                    hmap_days('TUESDAY'):='YES';
                END IF;
            END IF;
            
            IF  hmap_days('WEDNESDAY') <>'YES'
            THEN
                IF item_day_name.SSRMEET_WED_DAY IS NOT NULL
                THEN
                    hmap_days('WEDNESDAY'):='YES';
                END IF;
            END IF;
            
            IF  hmap_days('THURSDAY') <>'YES'
            THEN
                IF item_day_name.SSRMEET_THU_DAY IS NOT NULL
                THEN
                    hmap_days('THURSDAY'):='YES';
                END IF;
            END IF;
            
            IF  hmap_days('FRIDAY') <>'YES'
            THEN
                IF item_day_name.SSRMEET_FRI_DAY IS NOT NULL
                THEN
                    hmap_days('FRIDAY'):='YES';
                END IF;
            END IF;
            
            IF  hmap_days('SATURDAY') <>'YES'
            THEN
                IF item_day_name.SSRMEET_SAT_DAY IS NOT NULL
                THEN
                    hmap_days('SATURDAY'):='YES';
                END IF;
            END IF;
        END LOOP;      
        
        i:=hmap_days.FIRST;
        WHILE (i IS NOT NULL) LOOP
            IF hmap_days(i)='YES'
            THEN
                str_days :=str_days || ''''||i||''''||',';
            END IF;
            --DBMS_OUTPUT.PUT_LINE('Day:' || i|| '-'|| hmap_days(i));
            i:=hmap_days.NEXT(i);
        END LOOP;
        str_days := substr(str_days,0,length(str_days)-1);
        
        --This query retrieves only one start date and one end date for the term code and crn
        SELECT  DISTINCT ''''|| to_char(ssrmeet_start_date,'mm/dd/yyyy')||'''',''''||to_char(ssrmeet_end_date,'mm/dd/yyyy')||''''
          INTO start_date,end_date 
          FROM saturn.ssrmeet
         WHERE ssrmeet_TERM_CODE=p_term_code
           AND ssrmeet_CRN=p_crn;
           
        --This code will be generating the sql statement to retrieve the valid dates.
        sql_stmt :='SELECT dt as day_name,TO_DATE(day_date,''mm-dd-yyyy'') FROM (WITH t AS ( SELECT TO_DATE('|| start_date||',''mm/dd/yyyy'') start_date, TO_DATE ('|| end_date ||',''mm/dd/yyyy'') end_date from dual) SELECT * FROM (SELECT TO_CHAR(START_DATE + (LEVEL-1),''FMDAY'')dt,TO_CHAR(START_DATE + (LEVEL-1),''mm/dd/yyyy'') as day_date' ||
                ' FROM t CONNECT BY LEVEL <=end_date-start_date+1)WHERE dt IN ('|| str_days ||'))';
        
        
        OPEN v_dates_cursor FOR sql_stmt;
        LOOP
            FETCH v_dates_cursor INTO dates_rec;
            EXIT WHEN v_dates_cursor%NOTFOUND;
            
            OPEN roster_cursor;
            P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','P_CreateRoster','Rows in Cursor: ' || roster_cursor%ROWCOUNT ,MESSAGE_LEVEL_DEBUG,gv_session_step_number) ;
            LOOP
                FETCH roster_cursor INTO students_rec;
                EXIT WHEN roster_cursor%NOTFOUND; 
                
                    SELECT COUNT(*)
                      INTO count_exist
                      FROM PRCCUSR.SZRATTENDANCE
                     WHERE SZRATTENDANCE.SZRATTENDANCE_SPRIDEN_ID =students_rec.student_id
                       AND SZRATTENDANCE_TERM_CODE=p_term_code
                       AND SZRATTENDANCE_CRN=p_crn
                       AND SZRATTENDANCE_ATTENDANCE_DATE =dates_rec.day_date;
                    
                    --P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','P_CreateRoster','cHECKING DATE: ' ||TO_CHAR(dates_rec.day_date,'mm-dd-yyyy') ,MESSAGE_LEVEL_INFO,gv_session_step_number) ;
                    IF count_exist = 0 
                    THEN
                        --P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','P_CreateRoster','Retrieving day names.' ||TO_DATE(dates_rec.day_date,'mm-dd-yyyy') ,MESSAGE_LEVEL_DEBUG,gv_session_step_number) ;
                        is_valid_withdraw_date :=f_is_valid_withdraw_date(p_term_code,p_crn,students_rec.student_id,dates_rec.day_date);
                        
                        IF (is_valid_withdraw_date) = TRUE
                        THEN
                            INSERT INTO PRCCUSR.SZRATTENDANCE (SZRATTENDANCE_SPRIDEN_ID,SZRATTENDANCE_PIDM,SZRATTENDANCE_TERM_CODE,SZRATTENDANCE_CRN,SZRATTENDANCE_ATTENDANCE_DATE,SZRATTENDANCE_REGULAR_MEETING,SZRATTENDANCE_ATTENDED,SZRATTENDANCE_CREATION_DATE,SZRATTENDANCE_UPDATE_DATE)
                            VALUES (students_rec.student_id,students_rec.pidm,p_term_code,p_crn,dates_rec.day_date,REGULAR_MEETING,STATUS_TYPE_TEMPORAL,SYSDATE,NULL);
                        ELSE
                            --WITHDRAW STATUS
                            INSERT INTO PRCCUSR.SZRATTENDANCE (SZRATTENDANCE_SPRIDEN_ID,SZRATTENDANCE_PIDM,SZRATTENDANCE_TERM_CODE,SZRATTENDANCE_CRN,SZRATTENDANCE_ATTENDANCE_DATE,SZRATTENDANCE_REGULAR_MEETING,SZRATTENDANCE_ATTENDED,SZRATTENDANCE_CREATION_DATE,SZRATTENDANCE_UPDATE_DATE)
                            VALUES (students_rec.student_id,students_rec.pidm,p_term_code,p_crn,dates_rec.day_date,REGULAR_MEETING,STATUS_TYPE_WITHDRAW,SYSDATE,NULL);
                        END IF;
                    END IF;
                    
                    
                    SELECT COUNT(*)
                      INTO count_exist
                      FROM PRCCUSR.SZRATTLDA
                     WHERE SZRATTLDA.SZRATTLDA_SPRIDEN_ID =students_rec.student_id
                       AND SZRATTLDA_TERM_CODE=p_term_code
                       AND SZRATTLDA_CRN=p_crn;
                    IF count_exist = 0 
                    THEN
                        INSERT INTO PRCCUSR.SZRATTLDA(SZRATTLDA_SPRIDEN_ID,SZRATTLDA_TERM_CODE,SZRATTLDA_CRN,SZRATTLDA_ATTENDANCE_DATE,SZRATTLDA_INSERT_DATE,SZRATTLDA_INSERTED_BY,SZRATTLDA_UPDATE_DATE,SZRATTLDA_UPDATED_BY)
                        VALUES(students_rec.student_id,p_term_code,p_crn,NULL,sysdate,global_pidm,NULL,NULL);
                    END IF;
                    
                    
            END LOOP;
            CLOSE roster_cursor;
        END LOOP;
        CLOSE v_dates_cursor;
        COMMIT;
        
     
    END;
                                        
    
    
    /*
    if the application log level match with the message lewel the message will be saved. 
    */
    PROCEDURE P_Insert_Log(p_app_name VARCHAR2, p_package_name VARCHAR2,p_procedure_name VARCHAR2,p_message VARCHAR2, p_message_level NUMBER, p_session_step_number IN OUT NUMBER)
    IS
        local_message_type                VARCHAR2(20):='UNDEFINED';
    BEGIN
        IF gv_application_log_level >= p_message_level
        THEN
            IF p_message_level = MESSAGE_LEVEL_INFO
            THEN
                local_message_type :='INFO';
            END IF;
            
            IF p_message_level = MESSAGE_LEVEL_WARNING
            THEN
                local_message_type :='WARNING';
            END IF;
            
            IF p_message_level = MESSAGE_LEVEL_ERROR
            THEN
                local_message_type :='ERROR';
            END IF;
            
            IF p_message_level = MESSAGE_LEVEL_DEBUG
            THEN
                local_message_type :='DEBUG';
            END IF;
            
            p_session_step_number := p_session_step_number +1;
            INSERT INTO PRCCUSR.SZRAPPLOGTABLE VALUES( TO_TIMESTAMP(TO_CHAR(SYSDATE,'mm-dd-yyyy hh24:mi:ss'),'mm.dd.yyyy hh24:mi:ss'),p_app_name,p_package_name,p_procedure_name,p_session_step_number,p_message,local_message_type);
            COMMIT;
        END IF;
      
    END;
    
    
    
    PROCEDURE P_displayDropBoxMonth (p_enabled                  IN BOOLEAN DEFAULT FALSE,
                                     p_attendance_date           VARCHAR2 DEFAULT NULL)
    IS 
         in_month  VARCHAR2(2);
    BEGIN
        
        IF p_enabled  = TRUE
        THEN
           htp.formSelectOpen('p_month',NULL,NULL,'id="id_month" onchange="enableYear()"');  
        ELSE
           htp.formSelectOpen('p_month',NULL,NULL,'id="id_month" disabled onchange="enableYear()"');   
        END IF;
        
        --htp.formSelectOpen('p_month',NULL,NULL,'id="id_month disabled onchange="enableYear()"'); 
               
        twbkwbis.p_formSelectOption('Choose','Choose');
        twbkwbis.p_formSelectOption('Jan','0',f_get_selected_value('JAN',p_attendance_date,'MONTH'));
        twbkwbis.p_formSelectOption('Feb','1',f_get_selected_value('FEB',p_attendance_date,'MONTH') );
        twbkwbis.p_formSelectOption('Mar','2',f_get_selected_value('MAR',p_attendance_date,'MONTH'));
        twbkwbis.p_formSelectOption('Apr','3',f_get_selected_value('APR',p_attendance_date,'MONTH'));
        twbkwbis.p_formSelectOption('May','4',f_get_selected_value('MAY',p_attendance_date,'MONTH'));
        twbkwbis.p_formSelectOption('Jun','5',f_get_selected_value('JUN',p_attendance_date,'MONTH'));
        twbkwbis.p_formSelectOption('Jul','6',f_get_selected_value('JUL',p_attendance_date,'MONTH'));
        twbkwbis.p_formSelectOption('Aug','7',f_get_selected_value('AUG',p_attendance_date,'MONTH'));
        twbkwbis.p_formSelectOption('Sep','8',f_get_selected_value('SEP',p_attendance_date,'MONTH'));
        twbkwbis.p_formSelectOption('Oct','9',f_get_selected_value('OCT',p_attendance_date,'MONTH'));
        twbkwbis.p_formSelectOption('Nov','10',f_get_selected_value('NOV',p_attendance_date,'MONTH'));
        twbkwbis.p_formSelectOption('Dec','11',f_get_selected_value('DEC',p_attendance_date,'MONTH'));
       
        htp.formSelectClose;
    END P_displayDropBoxMonth;
          -----------------------------------------------------------------------------


--FUNCTIONS
    -- Get the student's full name or ID
    FUNCTION f_get_spriden(p_pidm_in in varchar2, option_in in varchar2 default 'N') return varchar2
    IS
	CURSOR cu_spriden(p_pidm_in in number)
	IS
            select	spriden_first_name
            ||		decode(spriden_mi, null, ' ', ' ' || spriden_mi || ' ')
            ||		spriden_last_name full_name
            ,		spriden_id id
            from	saturn.spriden
            where	spriden_pidm = p_pidm_in
            and		spriden_change_ind is null
            ;
        lv_name		varchar2(200) default 'Name not found';
        lv_id		varchar2(20) default null;
    BEGIN
        for lv_rec in cu_spriden(p_pidm_in)
        loop
            lv_name := lv_rec.full_name;
            lv_id := lv_rec.id;
        end loop;
        if option_in = 'I'
        then
            return lv_id;
        else
            return lv_name;
        end if;
        exception when others then
            if option_in = 'I'
            then
                return 'No ID found';
            else
                return 'Name not found';
            end if;
    END f_get_spriden;


    



    FUNCTION f_get_term_description(term_in IN VARCHAR2)
        RETURN VARCHAR2
    IS
        lv_answer		VARCHAR2(30);
    BEGIN
        IF term_in IS NULL
        THEN
            RETURN '';
        end if;

        FOR lv_rec IN cu_terms
        LOOP
            IF lv_rec.term_code = term_in
            THEN
                lv_answer := lv_rec.term_desc;
            END IF;
        END LOOP;
	--
        RETURN lv_answer;
    END f_get_term_description;
    
    

    
FUNCTION f_get_selected_value      ( p_value   VARCHAR2 DEFAULT NULL,  p_attendance_date IN DATE, p_value_to_search  VARCHAR2) 
        RETURN VARCHAR2
    IS
        local_month_name VARCHAR2(5);
        local_value      VARCHAR2(10):='';
       
    BEGIN
        IF p_value_to_search ='MONTH'
        THEN
            SELECT SUBSTR(UPPER(TO_CHAR(p_attendance_date,'Mon-dd-yyyy')),0,3) 
            INTO local_month_name
            FROM DUAL;
        
            IF p_value = local_month_name
            THEN
                P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','f_get_selected_value',global_pidm ||'- local_month_name:' || local_month_name ||' p_value:' || p_value  ,MESSAGE_LEVEL_DEBUG,gv_session_step_number) ;
                local_value:= 'SELECTED';
               
            END IF;
        END IF;
        
        IF p_value_to_search ='DAY'
        THEN
            SELECT SUBSTR(UPPER(TO_CHAR(p_attendance_date,'Mon-dd-yyyy')),5,2) 
            INTO local_value
            FROM DUAL;
           
        END IF;
        
        IF p_value_to_search ='YEAR'
        THEN
            --P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','f_get_selected_value',global_pidm ||'- Searching year. p_value:' || p_value  ,MESSAGE_LEVEL_DEBUG,gv_session_step_number) ;
            SELECT SUBSTR(UPPER(TO_CHAR(p_attendance_date,'Mon-dd-yyyy')),8,4) 
            INTO local_value
            FROM DUAL;
            --P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','f_get_selected_value',global_pidm ||'- Searching year. p_value:' || p_value ||' local Value:'||local_value   ,MESSAGE_LEVEL_DEBUG,gv_session_step_number) ;
            IF p_value = local_value
            THEN
                P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','f_get_selected_value',global_pidm ||'-Year Found. local_value:' || local_value ||' p_value:' || p_value  ,MESSAGE_LEVEL_DEBUG,gv_session_step_number) ;
                local_value:= 'SELECTED';
            ELSE
                local_value:=NULL;
            END IF; 
        END IF;
        
        RETURN local_value;
    END f_get_selected_value; 
    
    
    
   

    

    FUNCTION f_get_spriden_id(p_student_id IN VARCHAR2) RETURN VARCHAR2 
    IS
    	lv_id		SATURN.SPRIDEN.SPRIDEN_ID%TYPE DEFAULT NULL;
    BEGIN
        BEGIN
            SELECT	spriden_id id
              INTO  lv_id
              FROM	saturn.spriden
             WHERE	spriden_id = p_student_id
               AND	spriden_change_ind IS NULL;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                lv_id:=NULL;
        END;
       RETURN lv_id;
    END f_get_spriden_id;


    FUNCTION f_get_conf_grad_date(p_term_in IN VARCHAR2)
        RETURN DATE
    IS
        lv_date		   VARCHAR2(30);
        lv_last        DATE;
    BEGIN
        IF p_term_in IS NULL
        THEN
            RETURN '';
        END IF;

        -- P_Insert_Log('P_askInformation:Entering Method','TRAN_TYPE UPDATE') ;

        FOR lv_rec IN cu_terms
        LOOP
            IF lv_rec.term_code = p_term_in
            THEN
                lv_date  := SUBSTR(lv_rec.external_code,5,2)  ||'-01-'||SUBSTR(lv_rec.external_code,1,4);
                lv_last  := LAST_DAY(TO_DATE(lv_date,'mm-dd-yyyy'));

            END IF;
        END LOOP;
	--
        RETURN lv_last;
    END f_get_conf_grad_date;


    FUNCTION f_has_only_letters(p_major_in IN VARCHAR2)
        RETURN BOOLEAN
    IS
        lv_count                 VARCHAR2(5):=' ';
        lv_has_only_letters      BOOLEAN :=FALSE;
    BEGIN
        IF p_major_in IS NULL
        THEN
            RETURN FALSE;
        END IF;

        SELECT LENGTH(TRIM(TRANSLATE(p_major_in,'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',' ')))
          INTO lv_count
         FROM DUAL;

        IF lv_count IS NULL THEN
            lv_has_only_letters := TRUE; --p_major has only letters
        ELSE
            lv_has_only_letters := FALSE; --p_major has numbers
        END IF;

        RETURN lv_has_only_letters;
    END f_has_only_letters;


     FUNCTION f_get_completed_date(p_date IN VARCHAR2)
        RETURN VARCHAR2
    IS
        lv_completed_date        VARCHAR2(10);
    BEGIN

        lv_completed_date := '01-'||p_date;

        RETURN lv_completed_date;
    END f_get_completed_date;
    
    
    FUNCTION f_validate_parameters (       p_pidm                    IN PRCCUSR.SZRATTENDANCE.SZRATTENDANCE_SPRIDEN_ID%TYPE,
                                           p_term_code_in            IN general.gtvsdax.gtvsdax_external_code%TYPE,
                                           p_crn                     IN ssbsect.ssbsect_crn%TYPE,
                                           p_year                    IN NUMBER,
                                           p_month                   IN NUMBER,
                                           p_day                     IN NUMBER
											 ) RETURN BOOLEAN
    IS
        is_valid                BOOLEAN:=TRUE;
        message                 VARCHAR2(50);
    BEGIN
        P_Insert_Log('Attendance App','BANINST1.bwskattendanceapp','f_validate_parameters',global_pidm || '-Validating parameters. pidm:' || p_pidm|| ' term_code:'|| p_term_code_in ||  ' crn: ' || p_crn ||' year:' ||p_year || ' month:' ||p_month ||' day: '|| p_day,MESSAGE_LEVEL_DEBUG,gv_session_step_number) ;
        IF p_term_code_in IS NULL 
        THEN
            is_valid:=FALSE;
            message:='Invalid term_code';
        END IF;
        
        IF p_crn IS NULL
        THEN
            is_valid:=FALSE;
             message:='Invalid crn';
        END IF;
        
        IF p_year IS NULL
        THEN
            is_valid:=FALSE;
            message:='Invalid year';
        END IF;

        IF p_month IS NULL
        THEN
            is_valid:=FALSE;
            message:='Invalid month';
        END IF;
        
        IF p_day IS NULL
        THEN
            is_valid:=FALSE;
            message:='Invalid day';
        END IF;
        
        IF is_valid = TRUE
        THEN
            P_Insert_Log('Attendance App','BANINST1.bwskattendanceapp','f_validate_parameters',global_pidm || '-Validating parameters. TRUE ' ,MESSAGE_LEVEL_INFO,gv_session_step_number) ;
        ELSE
            P_Insert_Log('Attendance App','BANINST1.bwskattendanceapp','f_validate_parameters',global_pidm || '-Validating parameters. FALSE ' || message ,MESSAGE_LEVEL_INFO,gv_session_step_number) ;
        END IF;
        
        RETURN is_valid;
    END f_validate_parameters;
    
    FUNCTION f_get_formatted_date      (   p_attendance_date         IN date) RETURN VARCHAR2
    IS
        formatted_date         VARCHAR2(60);
        day_name               VARCHAR2(10);
        month_name             VARCHAR2(20);
    BEGIN
       
        SELECT TRIM(TO_CHAR(TO_DATE(p_attendance_date,'dd/mm/yyyy'),'Day'))
        INTO day_name
        FROM dual;
       
        
        SELECT TO_CHAR(TO_DATE(p_attendance_date,'dd/mm/yyyy'),'Month dd yyyy') 
        INTO month_name
        FROM dual;
        
        formatted_date := day_name || ', ' || month_name;
        RETURN formatted_date;
        
    END f_get_formatted_date;
    
    
    FUNCTION f_local_validTerm(term_in IN VARCHAR2)
        RETURN BOOLEAN
    IS
        lv_answer		BOOLEAN DEFAULT FALSE;
    BEGIN
        IF term_in IS NULL
        THEN
            RETURN FALSE;
        end if;

        FOR lv_rec IN cu_terms_by_spriden_id
        LOOP
            IF lv_rec.term_code = term_in
            THEN
                lv_answer := TRUE;
            END IF;
        END LOOP;
	
        RETURN lv_answer;
    END f_local_validTerm;
    
    
    
    FUNCTION f_is_valid_withdraw_date( p_term_code               IN general.gtvsdax.gtvsdax_external_code%TYPE,
                                       p_crn                     IN sfrstcr.sfrstcr_crn%TYPE,
                                       p_pidm                    IN sfrstca.sfrstca_pidm%TYPE,                                  
                                       p_attendant_date          DATE) RETURN BOOLEAN
    IS
        withdraw_date               DATE;
        is_valid_withdraw_date      BOOLEAN:=TRUE;
    BEGIN
        withdraw_date:=f_get_withdraw_date (p_term_code,p_crn,p_pidm);
        P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','f_is_valid_withdraw_date','Retrieving WITHDRAWDATE.'|| withdraw_date ,MESSAGE_LEVEL_DEBUG,gv_session_step_number) ;
        IF withdraw_date IS NOT NULL
        THEN 
            --DBMS_OUTPUT.PUT_LINE('WithdrawDate:' ||withdraw_date );
            --DBMS_OUTPUT.PUT_LINE('p_attendant_date:' ||p_attendant_date );
            P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','f_is_valid_withdraw_date','WithDraw date is not null.'|| withdraw_date ,MESSAGE_LEVEL_DEBUG,gv_session_step_number) ;
            IF withdraw_date>p_attendant_date   
            THEN
                is_valid_withdraw_date:=TRUE;
                P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','f_is_valid_withdraw_date','WithDraw bigger than attendant date.'|| withdraw_date ,MESSAGE_LEVEL_DEBUG,gv_session_step_number) ;
            ELSE
                is_valid_withdraw_date:=FALSE;
                P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','f_retrieve_l_message','WithDraw Invalid.'|| withdraw_date ,MESSAGE_LEVEL_INFO,gv_session_step_number) ;
            END IF;
        ELSE
            is_valid_withdraw_date:=TRUE;
        END IF;
        
        
        /*
        IF is_valid_withdraw_date = TRUE
        THEN
            P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','f_retrieve_l_message','Value: TRUE' ,MESSAGE_LEVEL_INFO,gv_session_step_number) ;
        ELSE
            P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','f_retrieve_l_message','Value: FALSE' ,MESSAGE_LEVEL_INFO,gv_session_step_number) ;
        END IF;    
        */
        RETURN is_valid_withdraw_date;
    END;  
                                        
                                        
    FUNCTION f_is_valid_unscheduled_date  (    p_term_code               IN general.gtvsdax.gtvsdax_external_code%TYPE,
                                               p_crn                     IN sfrstcr.sfrstcr_crn%TYPE,
                                               p_attendant_date          DATE)RETURN BOOLEAN
    IS
        start_date                  DATE;
        end_date                    DATE;
        is_valid_unscheduled_date   BOOLEAN:= FALSE;
    BEGIN
        BEGIN
            SELECT SSRMEET_START_DATE,SSRMEET_END_DATE
              INTO start_date,end_date
              FROM SSRMEET
             WHERE SSRMEET_TERM_CODE=p_term_code
               AND SSRMEET_CRN=p_crn;
            IF(p_attendant_date>=start_date)
            THEN
               IF(p_attendant_date<=end_date)
               THEN
                    is_valid_unscheduled_date:=TRUE;
               END IF;
            END IF;
            RETURN is_valid_unscheduled_date;
        EXCEPTION WHEN NO_DATA_FOUND 
        THEN
            P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','f_is_valid_unscheduled_date','Retrievind end an start date. NO_DATA_FOUND term_code: '|| p_term_code ||' CRN:|| ' ||p_crn ,MESSAGE_LEVEL_ERROR,gv_session_step_number) ;
            RETURN FALSE;  
        END; 
        
    END f_is_valid_unscheduled_date;
    
                                          
    FUNCTION f_get_course_start_date  (    p_term_code               IN general.gtvsdax.gtvsdax_external_code%TYPE,
                                               p_crn                     IN sfrstcr.sfrstcr_crn%TYPE
                                        )RETURN VARCHAR2
    IS
        start_date                  VARCHAR2(10);
       
    BEGIN
        BEGIN
            SELECT TO_CHAR(SSRMEET_START_DATE,'mm-dd-yyyy')
              INTO start_date
              FROM SSRMEET
             WHERE SSRMEET_TERM_CODE=p_term_code
               AND SSRMEET_CRN=p_crn;
            
            RETURN start_date;
        EXCEPTION WHEN NO_DATA_FOUND 
        THEN
            P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','f_get_course_start_date','Retrievind start date. NO_DATA_FOUND term_code: '|| p_term_code ||' CRN:|| ' ||p_crn ,MESSAGE_LEVEL_ERROR,gv_session_step_number) ;
            RETURN start_date;  
        END; 
        
    END f_get_course_start_date;    
    
    
    FUNCTION f_get_course_end_date  (    p_term_code               IN general.gtvsdax.gtvsdax_external_code%TYPE,
                                               p_crn                     IN sfrstcr.sfrstcr_crn%TYPE
                                         )RETURN VARCHAR2
    IS
        end_date                  VARCHAR2(10);
       
    BEGIN
        BEGIN
            SELECT TO_CHAR(SSRMEET_END_DATE,'mm-dd-yyyy')
              INTO end_date
              FROM SSRMEET
             WHERE SSRMEET_TERM_CODE=p_term_code
               AND SSRMEET_CRN=p_crn;
            
            RETURN end_date;
        EXCEPTION WHEN NO_DATA_FOUND 
        THEN
            P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','f_get_course_end_date','Retrievind end date. NO_DATA_FOUND term_code: '|| p_term_code ||' CRN:|| ' ||p_crn ,MESSAGE_LEVEL_ERROR,gv_session_step_number) ;
            RETURN end_date;  
        END; 
        
    END f_get_course_end_date;                                     
   
                          
                                                                                 
    
    FUNCTION f_get_withdraw_date (    p_term_code               IN general.gtvsdax.gtvsdax_external_code%TYPE,
                                      p_crn                     IN sfrstcr.sfrstcr_crn%TYPE,
                                      p_pidm                    IN sfrstca.sfrstca_pidm%TYPE
                                     
                                   ) RETURN DATE
	IS
        withdraw_date               DATE:=NULL;  -- TO_DATE('01-01-1988','mm-dd-yyyy');
	BEGIN
        BEGIN
             SELECT TO_DATE(to_char(TRUNC(SFRSTCA_RSTS_DATE),'mm-dd-yyyy'),'mm-dd-yyyy')
               INTO withdraw_date
               FROM SFRSTCA
               JOIN SSBSECT ON SSBSECT_CRN = SFRSTCA_CRN AND SSBSECT_TERM_CODE = SFRSTCA_TERM_CODE
               JOIN SCBCRSE A ON A.SCBCRSE_SUBJ_CODE = SSBSECT_SUBJ_CODE
                AND A.SCBCRSE_CRSE_NUMB = SSBSECT_CRSE_NUMB
                AND A.SCBCRSE_EFF_TERM = (SELECT MAX(B.SCBCRSE_EFF_TERM)
                                          FROM SCBCRSE B
                                          WHERE B.SCBCRSE_SUBJ_CODE = A.SCBCRSE_SUBJ_CODE
                                          AND B.SCBCRSE_CRSE_NUMB = A.SCBCRSE_CRSE_NUMB)
              WHERE SFRSTCA_TERM_CODE = p_term_code
                AND SFRSTCA_SOURCE_CDE = 'BASE'
                AND SFRSTCA_RSTS_CODE IN ('W')
                AND SFRSTCA_PIDM = p_pidm
                AND sfrstca_crn=p_crn;
             RETURN withdraw_date;    
        EXCEPTION WHEN NO_DATA_FOUND 
        THEN
            P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','f_get_withdraw_date','Retrieving WithDraw Date. NO_DATA_FOUND term_code: '|| p_term_code ||' CRN:|| ' ||p_crn||  ' PIDM: '|| p_pidm ,MESSAGE_LEVEL_ERROR,gv_session_step_number) ;
            RETURN withdraw_date;  
        END; 
         
	END;
	
    
    FUNCTION f_retrieve_adm_status (  p_term_code               IN general.gtvsdax.gtvsdax_external_code%TYPE,
                                      p_pidm                    IN sfrstca.sfrstca_pidm%TYPE,
                                      p_crn                     IN sfrstcr.sfrstcr_crn%TYPE
                                   ) RETURN VARCHAR2
	IS
        l_message     VARCHAR2(50):='';
	BEGIN
        BEGIN
            SELECT MESSAGE 
              INTO l_message
              --WITHDRAWN STUDENT
            FROM (SELECT CASE WHEN SFRSTCA_RSTS_CODE = 'W' THEN 'WITHDRAWN STUDENT' ||'-'|| TO_CHAR(TRUNC(SFRSTCA_RSTS_DATE),'mm-dd-yyyy')
                                    --WHEN SFRSTCA_RSTS_CODE = 'DD' THEN 'NO-SHOW STUDENT' ||'-'|| TRUNC(SFRSTCA_RSTS_DATE)
                                     WHEN SFRSTCA_RSTS_CODE <> 'W' THEN '' END MESSAGE 
                               FROM SFRSTCA
                               JOIN SSBSECT ON SSBSECT_CRN = SFRSTCA_CRN AND SSBSECT_TERM_CODE = SFRSTCA_TERM_CODE
                               JOIN SCBCRSE A ON A.SCBCRSE_SUBJ_CODE = SSBSECT_SUBJ_CODE
                                AND A.SCBCRSE_CRSE_NUMB = SSBSECT_CRSE_NUMB
                                AND A.SCBCRSE_EFF_TERM = (SELECT MAX(B.SCBCRSE_EFF_TERM)
                                                          FROM SCBCRSE B
                                                          WHERE B.SCBCRSE_SUBJ_CODE = A.SCBCRSE_SUBJ_CODE
                                                          AND B.SCBCRSE_CRSE_NUMB = A.SCBCRSE_CRSE_NUMB)
                              WHERE SFRSTCA_TERM_CODE = p_term_code
                                AND SFRSTCA_SOURCE_CDE = 'BASE'
                                AND SFRSTCA_RSTS_CODE IN ('W')
                                AND SFRSTCA_PIDM = p_pidm
                                AND sfrstca_crn=p_crn
                              )
                             ;
                     
        EXCEPTION WHEN NO_DATA_FOUND 
        THEN
            P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','f_retrieve_l_message','Retrieving l_message. term_code: '|| p_term_code ||  ' PIDM: '|| p_pidm ,MESSAGE_LEVEL_INFO,gv_session_step_number) ;
        END;
            
        RETURN l_message;   
            
	END;
									    
									    
    
    FUNCTION f_validate_attendant_date (   p_term_code               IN general.gtvsdax.gtvsdax_external_code%TYPE,
                                           p_crn                     IN ssbsect.ssbsect_crn%TYPE,
                                           p_attendant_date          IN DATE
                                           
									    ) RETURN BOOLEAN
    IS
        is_valid                BOOLEAN:=FALSE;
        day_name                VARCHAR2(20);
        CURSOR days_cursor
            IS
            SELECT  'YES',SSRMEET_START_DATE,SSRMEET_END_DATE,
                    NVL2(SSRMEET_SUN_DAY,'SUNDAY',NULL),NVL2(SSRMEET_MON_DAY,'MONDAY',NULL),NVL2(SSRMEET_TUE_DAY,'TUESDAY',NULL),
                    NVL2(SSRMEET_WED_DAY,'WEDNESDAY',NULL),NVL2(SSRMEET_THU_DAY,'THURSDAY',NULL),NVL2(SSRMEET_FRI_DAY,'FRIDAY',NULL),NVL2(SSRMEET_SAT_DAY,'SATURDAY',NULL)
              FROM SSRMEET
             WHERE p_attendant_date BETWEEN SSRMEET_START_DATE AND SSRMEET_END_DATE
               AND SSRMEET_TERM_CODE=p_term_code
               AND SSRMEET_CRN=p_crn;
    BEGIN
        P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','f_validate_attendant_date','Validating Attendant Date. term_code: '|| p_term_code ||  ' crn: ' || p_crn || ' Date: ' || p_attendant_date  ,MESSAGE_LEVEL_INFO,gv_session_step_number) ;
        
        SELECT UPPER(TO_CHAR(TO_DATE(p_attendant_date),'day'))
          INTO day_name
          FROM dual;
        P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','f_validate_attendant_date','Validating Attendant Date. term_code: '|| p_term_code ||  ' crn: ' || p_crn || ' Date: ' || p_attendant_date ||' Calculated Day: '|| day_name ,MESSAGE_LEVEL_DEBUG,gv_session_step_number) ;
        
        OPEN days_cursor;
        LOOP
            FETCH days_cursor INTO days_rec;
            EXIT WHEN days_cursor%NOTFOUND;
        
            IF days_cursor%NOTFOUND
            THEN
                P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','f_validate_attendant_date','No Records found.' ,MESSAGE_LEVEL_ERROR,gv_session_step_number) ;
            ELSE
                /*
                DBMS_OUTPUT.PUT_LINE(days_rec.day_name_SUNDAY ||'-'|| day_name);
                DBMS_OUTPUT.PUT_LINE(days_rec.day_name_MONDAY ||'-'|| day_name);
                DBMS_OUTPUT.PUT_LINE(days_rec.day_name_TUESDAY||'-'|| day_name);
                DBMS_OUTPUT.PUT_LINE(days_rec.day_name_WEDNESDAY ||'-'|| day_name);
                DBMS_OUTPUT.PUT_LINE(days_rec.day_name_THURSDAY ||'-'|| day_name);
                DBMS_OUTPUT.PUT_LINE(days_rec.day_name_FRIDAY ||'-'|| day_name);
                DBMS_OUTPUT.PUT_LINE(days_rec.day_name_SATURDAY ||'-'|| day_name);
                */
                
                IF TRIM(days_rec.day_name_sunday) =TRIM(day_name)
                THEN
                    is_valid:=true;
                    P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','f_validate_attendant_date','Valid Day SUNDAY' ,MESSAGE_LEVEL_DEBUG,gv_session_step_number) ;
                ELSE
                    P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','f_validate_attendant_date','Compared values SUNDAY: ' ||days_rec.day_name_sunday ||'-'|| day_name ,MESSAGE_LEVEL_DEBUG,gv_session_step_number) ;
                END IF;
                
                IF TRIM(days_rec.day_name_monday) = TRIM(day_name)
                THEN
                    is_valid:=true;
                    P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','f_validate_attendant_date','Valid Day MONDAY' ,MESSAGE_LEVEL_DEBUG,gv_session_step_number) ;
                ELSE
                    P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','f_validate_attendant_date','Compared values MONDAY: ' ||days_rec.day_name_monday ||'-'|| day_name ,MESSAGE_LEVEL_DEBUG,gv_session_step_number) ;
                END IF;
                
                IF TRIM(days_rec.day_name_tuesday) = TRIM(day_name)
                THEN
                    is_valid:=true;
                    P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','f_validate_attendant_date','Valid Day TUESDAY' ,MESSAGE_LEVEL_DEBUG,gv_session_step_number) ;
                ELSE
                    P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','f_validate_attendant_date','Compared values TUESDAY: ' ||days_rec.day_name_tuesday ||'-'|| day_name ,MESSAGE_LEVEL_DEBUG,gv_session_step_number) ;
                END IF;
                
                IF TRIM(days_rec.day_name_wednesday) = TRIM(day_name)
                THEN
                    is_valid:=true;
                    P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','f_validate_attendant_date','Valid Day WEDNESDAY' ,MESSAGE_LEVEL_DEBUG,gv_session_step_number) ;
                ELSE
                    P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','f_validate_attendant_date','Compared values WEDNESDAY: ' ||days_rec.day_name_wednesday ||'-'|| day_name ,MESSAGE_LEVEL_DEBUG,gv_session_step_number) ;
                END IF;
                
                IF TRIM(days_rec.day_name_thursday) = TRIM(day_name)
                THEN
                    is_valid:=true;
                    P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','f_validate_attendant_date','Valid Day THURSDAY' ,MESSAGE_LEVEL_DEBUG,gv_session_step_number) ;
                ELSE
                    P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','f_validate_attendant_date','Compared values THURSDAY: ' ||days_rec.day_name_thursday ||'-'|| day_name ,MESSAGE_LEVEL_DEBUG,gv_session_step_number) ;
                END IF;
                
                IF TRIM(days_rec.day_name_friday) = TRIM(day_name)
                THEN
                    is_valid:=true;
                    P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','f_validate_attendant_date','Valid Day FRIDAY' ,MESSAGE_LEVEL_DEBUG,gv_session_step_number) ;
                ELSE
                    P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','f_validate_attendant_date','Compared values FRIDAY: ' ||days_rec.day_name_friday ||'-'|| day_name ,MESSAGE_LEVEL_DEBUG,gv_session_step_number) ;
                END IF;
                
                IF TRIM(days_rec.day_name_saturday) = TRIM(day_name)
                THEN
                    is_valid:=true;
                    P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','f_validate_attendant_date','Valid Day SATURDAY' ,MESSAGE_LEVEL_DEBUG,gv_session_step_number) ;
                ELSE
                    P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','f_validate_attendant_date','Compared values SATURDAY: ' ||days_rec.day_name_saturday ||'-'|| day_name ,MESSAGE_LEVEL_DEBUG,gv_session_step_number) ;
                END IF;
            END IF;
                      
             
            IF is_valid = TRUE
            THEN
                P_Insert_Log('Attendance App','BANINST1.bwskattendanceapp','f_validate_attendant_date','Validating Attendant Date. TRUE ' ,MESSAGE_LEVEL_DEBUG,gv_session_step_number) ;
            ELSE
                P_Insert_Log('Attendance App','BANINST1.bwskattendanceapp','f_validate_parameters','Validating parameters. FALSE' ,MESSAGE_LEVEL_DEBUG,gv_session_step_number) ;
            END IF;
        END LOOP;    
        CLOSE days_cursor; 
        RETURN is_valid;
    END f_validate_attendant_date;


    FUNCTION f_get_course_title  (    p_term_code               IN general.gtvsdax.gtvsdax_external_code%TYPE,
                                               p_crn                     IN sfrstcr.sfrstcr_crn%TYPE
                                         )RETURN VARCHAR2
    IS
        crse_title                  VARCHAR2(50);
       
    BEGIN
        BEGIN
            SELECT DISTINCT NVL(ssbsect_crse_title,scbcrse_title) crse_title
              INTO crse_title
        	  FROM 	saturn.ssbsect
		       	   ,saturn.ssrmeet
			       ,saturn.scbcrse
			       ,saturn.sirasgn
			 WHERE	ssrmeet_crn = ssbsect_crn
	    	   AND 	ssrmeet_term_code = ssbsect_term_code
		       AND 	ssbsect_subj_code = scbcrse_subj_code
	    	   AND 	ssbsect_crse_numb = scbcrse_crse_numb
	    	   AND 	sirasgn_crn(+) = ssrmeet_crn
		       AND 	sirasgn_term_code(+) = ssrmeet_term_code
		       AND	sirasgn_category(+) = ssrmeet_catagory
		       AND 	scbcrse_eff_term =
				  (	SELECT	MAX(b.scbcrse_eff_term) 
					  FROM	saturn.scbcrse b 
                     WHERE	b.scbcrse_eff_term <= ssbsect_term_code
					   AND 	b.scbcrse_crse_numb = ssbsect_crse_numb 
					   AND	b.scbcrse_subj_code = ssbsect_subj_code
				  )
			   --		FILTERS
			   --AND	sirasgn_primary_ind(+) = 'Y'
			   AND 	ssbsect_term_code = p_term_code
			   AND  ssrmeet_crn       = p_crn
			   and  sirasgn_pidm      = global_pidm   --global_pidm
			   AND 	ssbsect_subj_code <> 'CEU'	-- exclude continuing education
			   AND	ssbsect_sess_code != 'E'	-- exclude dual enrollment
			   AND	ssbsect_ssts_code = 'A'		-- only list active section
			   ORDER BY crse_title;
            RETURN crse_title;
        EXCEPTION WHEN NO_DATA_FOUND 
        THEN
            P_Insert_Log(gv_app_name,'BANINST1.bwskattendanceapp','f_get_course_title','Retrieving course title. NO_DATA_FOUND term_code: '|| p_term_code ||' CRN:|| ' ||p_crn ,MESSAGE_LEVEL_ERROR,gv_session_step_number) ;
            RETURN crse_title;  
        END; 
        
    END f_get_course_title;     
    
    
    
       
    /*
    function f_get_button(cname       in varchar2 DEFAULT NULL,
                    cvalue      in varchar2 character set any_cs DEFAULT 'Submit',
                    cattributes in varchar2 DEFAULT NULL) return varchar2 character set cvalue%charset is
    begin return('<INPUT TYPE="submit"'||
              cname,' NAME="'||cname||'"'||
              cvalue,' VALUE="'||cvalue||'"'||
              cattributes,' '||cattributes||
             '>'); end;
             
    */  
   -----------------------------------------------------------------------------
    BEGIN
      DBMS_OUTPUT.PUT_LINE('compiled');
    END bwskattendanceapp;
/