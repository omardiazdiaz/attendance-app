CREATE OR REPLACE PACKAGE BANINST1.bwskattendanceapp IS
/******************************************************************************
   NAME:       bwskattendanceapp
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        11/20/2017      Odiaz         1. Created this package.
******************************************************************************/

-- DESCRIPTION:
-- This package contains PROCEDUREs and Functions reguired for Foundation scholarship application 
--
-- DESCRIPTION END
--
  /* Declare externally visible constant,type,cursor,variable and exception. */

-- CONSTANTS

	TYPE_UPDATE                 CONSTANT VARCHAR2(6):='UPDATE';
	TYPE_INSERT                 CONSTANT VARCHAR2(6):='INSERT';
	TYPE_VALIDATE               CONSTANT VARCHAR2(8):='VALIDATE';
	TYPE_SAVE                   CONSTANT VARCHAR2(8):='SAVE';

    MESSAGE_LEVEL_INFO          CONSTANT NUMBER:=0;
    MESSAGE_LEVEL_WARNING       CONSTANT NUMBER:=1;
    MESSAGE_LEVEL_ERROR         CONSTANT NUMBER:=2;
    MESSAGE_LEVEL_DEBUG         CONSTANT NUMBER:=3;
    
    
    STATUS_TYPE_TEMPORAL        CONSTANT VARCHAR2(1):='T';
    STATUS_TYPE_NOT_ATTENDANT   CONSTANT VARCHAR2(1):='N';
    STATUS_TYPE_ATTENDANT       CONSTANT VARCHAR2(1):='Y';
    STATUS_TYPE_WITHDRAW        CONSTANT VARCHAR2(1):='W';
    
    REGULAR_MEETING             CONSTANT VARCHAR2(1):='Y';
    NOT_REGULAR_MEETING         CONSTANT VARCHAR2(1):='N';
    gv_session_step_number      NUMBER:=0;


-- TYPES
	TYPE attendance_row IS RECORD
        (
            attendent           PRCCUSR.SZRATTENDANCE.SZRATTENDANCE_ATTENDED %TYPE, 
            full_name           VARCHAR2(100),
            spriden_id          SATURN.SPRIDEN.spriden_id %TYPE,
            lda                 VARCHAR2(10) --MM-DD-YYYY--PRCCUSR.SZRATTLDA.SZRATTLDA_ATTENDANCE_DATE%TYPE
        );
        
        
    TYPE detail_row IS RECORD
        (
            pidm                SFRSTCR.SFRSTCR_PIDM%TYPE,
            student_name        VARCHAR2(100),
            spriden_id          SPRIDEN.SPRIDEN_ID%TYPE,
            street1             VARCHAR2(100),
            street1_1           VARCHAR2(100),
            city                VARCHAR2(50),
            state_code          VARCHAR2(10),
            zip                 VARCHAR2(20),
            term_code           general.gtvsdax.gtvsdax_external_code%TYPE,
            ptrm_code           SSBSECT.SSBSECT_PTRM_CODE%TYPE,
            sfrstcr_crn         sfrstcr.sfrstcr_crn%TYPE,
            ssbsect_subj_code   ssbsect.ssbsect_subj_code%TYPE,
            ssbsect_crse_numb   ssbsect.ssbsect_crse_numb%TYPE,
            section             ssbsect.SSBSECT_SEQ_NUMB%TYPE,
            ssbsect_camp_code   ssbsect.ssbsect_camp_code%TYPE,
            title               VARCHAR2(100),
            credit              sfrstcr.sfrstcr_credit_hr%TYPE,
            level_code          sfrstcr.sfrstcr_levl_code%TYPE,
            status              sfrstcr.sfrstcr_rsts_code%TYPE,
            start_date          ssrmeet.ssrmeet_start_date%TYPE,
            end_date            ssrmeet.ssrmeet_end_date%TYPE,
            ssrmeet_begin_time  ssrmeet.ssrmeet_begin_time%TYPE,
            ssrmeet_end_time    ssrmeet.ssrmeet_end_time%TYPE,
            ssrmeet_days_code   ssrmeet.ssrmeet_days_code%TYPE,
            meet_days           VARCHAR2(20),
            ssrmeet_bldg_code   ssrmeet.ssrmeet_bldg_code%TYPE,
            ssrmeet_room_code   ssrmeet.ssrmeet_room_code%TYPE,
            instructor          VARCHAR2(50),
            cred_hrs            VARCHAR2(10),
            mtyp                ssrmeet.ssrmeet_mtyp_code%TYPE
        ); 
        
     TYPE days_row IS RECORD
        (
            is_valid            VARCHAR2(3),
            start_date          SSRMEET.SSRMEET_START_DATE%TYPE,
            end_date            SSRMEET.SSRMEET_END_DATE%TYPE,
            day_name_sunday     VARCHAR2(20),
            day_name_monday     VARCHAR2(20),
            day_name_tuesday    VARCHAR2(20),
            day_name_wednesday  VARCHAR2(20),
            day_name_thursday   VARCHAR2(20),
            day_name_friday     VARCHAR2(20),
            day_name_saturday   VARCHAR2(20)
        );
        
    TYPE dates_row IS RECORD
        (
           date_attendance      SZRATTENDANCE.SZRATTENDANCE_ATTENDANCE_DATE%TYPE, --VARCHAR2(20)--
           regular_meeting      SZRATTENDANCE.SZRATTENDANCE_REGULAR_MEETING%TYPE --VARCHAR2(1)
        );
        
    TYPE ids_row IS RECORD
        (
            student_id          SZRATTENDANCE.SZRATTENDANCE_SPRIDEN_ID%TYPE,
            pidm                SZRATTENDANCE.SZRATTENDANCE_PIDM%TYPE     
        );
        
     TYPE students_detail_row IS RECORD
        (
            student_id          SZRATTENDANCE.SZRATTENDANCE_SPRIDEN_ID%TYPE,
            student_lda         VARCHAR2(100) ,--SZRATTLDA.SZRATTLDA_ATTENDANCE_DATE%TYPE, --LAST DAY ATTENDANCE
            full_name           VARCHAR2(100) ,
            student_pidm        SPRIDEN.SPRIDEN_PIDM%TYPE
        );
        
        

-- VARIABLES

    global_pidm            BWSKUTILS.global_pidm%TYPE;    
    attendance_rec         attendance_row;
    detail_rec             detail_row;
    days_rec               days_row;
    dates_rec              dates_row;
    students_rec           ids_row;
    students_detail_rec    students_detail_row;


--PROCEDURES
    --Main Weeb Page
    PROCEDURE P_AttendanceRoster (p_term_code                     IN general.gtvsdax.gtvsdax_external_code%TYPE DEFAULT NULL,
                                  p_crn                           IN PRCCUSR.SZRATTENDANCE.SZRATTENDANCE_CRN%TYPE DEFAULT NULL,
                                  p_day                           IN NUMBER DEFAULT NULL,
                                  p_month                         IN NUMBER DEFAULT NULL,
                                  p_year                          IN NUMBER DEFAULT NULL,
                                  p_submit_button                 IN VARCHAR2 DEFAULT NULL,
                                  p_submit_save_roster            IN VARCHAR2 DEFAULT NULL,
                                  P_HIDE_COLUMNS_NUMBER           IN VARCHAR2 DEFAULT NULL,
                                  P_HIDE_COLUMNS_WEEK             IN VARCHAR2 DEFAULT NULL,
                                  p_LAD                           IN VARCHAR2 DEFAULT NULL,
                                  P_LIST_FOR_UPDATE               IN VARCHAR2 DEFAULT NULL,
                                  P_FIRST_REGULAR_MEETING_DATE    IN VARCHAR2 DEFAULT NULL);
    
    PROCEDURE P_AttendanceDetail (p_term_code IN general.gtvsdax.gtvsdax_external_code%TYPE,
                                  p_crn       IN PRCCUSR.SZRATTENDANCE.SZRATTENDANCE_CRN%TYPE,
                                  p_pidm      IN VARCHAR2 DEFAULT NULL);
                                  
                                  
    PROCEDURE P_AttendanceApplication    (   p_term_code	      IN GENERAL.GTVSDAX.gtvsdax_external_code%TYPE DEFAULT NULL,
                                             p_crn                IN SSBSECT.ssbsect_crn%TYPE DEFAULT NULL,
                                             p_submit_in          IN VARCHAR2 DEFAULT NULL,
                                             p_month              IN VARCHAR2 DEFAULT NULL,
                                             p_year               IN VARCHAR2 DEFAULT NULL,
                                             p_day                IN VARCHAR2 DEFAULT NULL,
                                             p_submit_button      IN VARCHAR2 DEFAULT NULL,
                                             p_term_code_selected IN GENERAL.GTVSDAX.GTVSDAX_EXTERNAL_CODE%TYPE DEFAULT NULL,
                                             p_not_regular_date   IN VARCHAR2 DEFAULT NULL,
                                             p_save_button        IN VARCHAR2 DEFAULT NULL,
                                             p_check              IN VARCHAR2 DEFAULT NULL,
                                             p_button_check       IN VARCHAR2 DEFAULT NULL,
                                             p_button_uncheck     IN VARCHAR2 DEFAULT NULL,
                                             p_spriden_list       IN VARCHAR2 DEFAULT NULL,
                                             p_date               IN DATE DEFAULT NULL);

    --Main Table
                                               
                                        


    --  PROCEDURE to display the user's records in a webPage
    
 
    PROCEDURE P_CreateRoster   (p_term_code	      IN GENERAL.GTVSDAX.gtvsdax_external_code%TYPE ,
                                p_crn             IN SSBSECT.ssbsect_crn%TYPE  
                                );


	--Just for testing purposes
    PROCEDURE P_BasicHTML;
    
    PROCEDURE p_create_attendance_table;
    
     PROCEDURE p_create_not_attendance_table;

    PROCEDURE p_update_rows           (   p_spriden_list            IN VARCHAR2,
                                          p_term_code	            IN GENERAL.GTVSDAX.gtvsdax_external_code%TYPE,
                                          p_crn                     IN SSBSECT.ssbsect_crn%TYPE,
                                          p_date                    IN DATE
                                      );

    PROCEDURE p_update_LDA             (  p_spriden_list            IN VARCHAR2,
                                          p_term_code               IN GENERAL.GTVSDAX.gtvsdax_external_code%TYPE,
                                          p_crn                     IN SSBSECT.ssbsect_crn%TYPE);

       
    PROCEDURE p_displaySelectTerm     (   p_term_code               IN general.gtvsdax.gtvsdax_external_code%TYPE DEFAULT NULL); 

    --PROCEDURE SHOW the available CRN for the selected TERM_CODE
    PROCEDURE P_displaySelectCRN      (   p_term_code               IN general.gtvsdax.gtvsdax_external_code%TYPE DEFAULT NULL,
                                          p_crn                     IN SSBSECT.ssbsect_crn%TYPE DEFAULT NULL); 

    PROCEDURE p_displayBoxes          (  p_term_code                IN GENERAL.GTVSDAX.GTVSDAX_EXTERNAL_CODE%TYPE DEFAULT NULL,
                                         p_crn                      IN SSBSECT.ssbsect_crn%TYPE DEFAULT NULL,
                                         p_error_date               IN BOOLEAN DEFAULT FALSE ,
                                         p_attendance_date          IN DATE DEFAULT NULL
                                        -- p_form_origin              IN VARCHAR2 DEFAULT NULL 
                                         );
                                         
    PROCEDURE p_displayBoxesRoster (p_term_code           IN GENERAL.GTVSDAX.GTVSDAX_EXTERNAL_CODE%TYPE DEFAULT NULL,
                                          p_crn                 IN SSBSECT.ssbsect_crn%TYPE DEFAULT NULL,
                                          p_error_date          IN BOOLEAN DEFAULT FALSE,
                                          p_attendance_date     IN DATE DEFAULT NULL,
                                          p_form_origin         IN VARCHAR2 DEFAULT NULL
                                    );
    
    
    PROCEDURE P_Display_Meet_Error;
     
    PROCEDURE p_displayDateBox         ( p_enabled                  IN BOOLEAN DEFAULT NULL,
                                        p_attendance_date        IN DATE DEFAULT NULL);
    
    PROCEDURE p_create_table_params     ( p_term_code                  IN general.gtvsdax.gtvsdax_external_code%TYPE,
                                        p_crn                        IN PRCCUSR.SZRATTENDANCE.SZRATTENDANCE_CRN%TYPE,
                                        p_date                       IN DATE);  

	PROCEDURE p_displayListForUpdate    (doc_name VARCHAR2 DEFAULT NULL,
                                        p_term_code                  IN general.gtvsdax.gtvsdax_external_code%TYPE,
                                        p_crn                        IN PRCCUSR.SZRATTENDANCE.SZRATTENDANCE_CRN%TYPE,
                                        p_date                       IN DATE);
                                        
    PROCEDURE p_displayListAttendance   (doc_name VARCHAR2 DEFAULT NULL,
                                        p_term_code                  IN general.gtvsdax.gtvsdax_external_code%TYPE,
                                        p_crn                        IN PRCCUSR.SZRATTENDANCE.SZRATTENDANCE_CRN%TYPE,
                                        p_date      IN DATE);


	PROCEDURE p_dispInfoText			(  header_text_in            IN VARCHAR2 ,
										   label_in                  IN VARCHAR2,	
										   scrollable_in             BOOLEAN DEFAULT FALSE,
										   extraWhiteSpace_in        IN VARCHAR2 DEFAULT NULL);

	PROCEDURE p_dispInfoTextRoster			(  header_text_in            IN VARCHAR2 ,
										   label_in                  IN VARCHAR2,	
										   scrollable_in             BOOLEAN DEFAULT FALSE,
										   extraWhiteSpace_in        IN VARCHAR2 DEFAULT NULL);


    PROCEDURE P_Insert_Attendance(  p_term_code              IN general.gtvsdax.gtvsdax_external_code%TYPE,
                                    p_crn                    IN PRCCUSR.SZRATTENDANCE.SZRATTENDANCE_CRN%TYPE, 
                                    p_attendance_date        IN PRCCUSR.SZRATTENDANCE.SZRATTENDANCE_ATTENDANCE_DATE%TYPE );
                                    
    PROCEDURE p_create_table;    
    
     PROCEDURE p_create_table_student_details   (p_term_code                  IN general.gtvsdax.gtvsdax_external_code%TYPE,
                                                 p_student_pidm               VARCHAR2,
                                                 p_student_name               VARCHAR2,
                                                 p_spriden_id                 VARCHAR2,
                                                 p_address                    VARCHAR2,
                                                 p_cred_hours                 VARCHAR2 );
    
    PROCEDURE P_ShowRosterPage          (p_term_code                 IN general.gtvsdax.gtvsdax_external_code%TYPE,
                                         p_crn                       IN PRCCUSR.SZRATTENDANCE.SZRATTENDANCE_CRN%TYPE,
                                         p_after_update              IN BOOLEAN);                           


    PROCEDURE P_Insert_Log              (   p_app_name                VARCHAR2,
                                            p_package_name            VARCHAR2,
                                            p_procedure_name 		  VARCHAR2,
										    p_message				  VARCHAR2,
                                            p_message_level           NUMBER,
                                            p_session_step_number     IN OUT NUMBER
                                          );
                                          
    PROCEDURE P_displayDropBoxMonth     (p_enabled                  IN BOOLEAN DEFAULT FALSE,
                                         p_attendance_date          VARCHAR2 DEFAULT NULL);   
                                         
    PROCEDURE P_displayDropBoxDay       ( p_attendance_date         IN DATE DEFAULT NULL); 
    
     PROCEDURE P_displayDropBoxYear      (p_attendance_date        IN DATE DEFAULT NULL);                                       
--FUNCTIONS

	FUNCTION f_get_selected_value      (p_value    VARCHAR2 DEFAULT NULL,   p_attendance_date                 IN DATE,p_value_to_search  VARCHAR2) RETURN VARCHAR2; 

	FUNCTION f_get_term_description    (   term_in                   IN VARCHAR2) RETURN VARCHAR2;

	FUNCTION f_get_spriden             (   p_pidm_in                 IN VARCHAR2, 
	                                       option_in                 IN VARCHAR2 DEFAULT 'N') RETURN VARCHAR2;
	/*
	RETURN TRUE if the information is valid to insert or update in szrfoundapp table
	*/
	
	FUNCTION f_get_formatted_date      (   p_attendance_date         IN date) RETURN VARCHAR2;
	


	FUNCTION f_get_spriden_id          (   p_student_id              IN VARCHAR2) RETURN VARCHAR2 ;

    /**
    This function should return the date based on the GTVSDAX.external_code_value on the table GTVSDAX
    */
    FUNCTION f_get_conf_grad_date      (   p_term_in                 IN VARCHAR2) RETURN DATE;

    FUNCTION f_has_only_letters        (   p_major_in                IN VARCHAR2) RETURN BOOLEAN;

    FUNCTION f_get_completed_date      (   p_date                    IN VARCHAR2) RETURN VARCHAR2;
    
    FUNCTION f_validate_parameters     (   p_pidm                    IN PRCCUSR.SZRATTENDANCE.SZRATTENDANCE_SPRIDEN_ID%TYPE,
                                           p_term_code_in            IN general.gtvsdax.gtvsdax_external_code%TYPE,
                                           p_crn                     IN ssbsect.ssbsect_crn%TYPE,
                                           p_year                    IN NUMBER,
                                           p_month                   IN NUMBER,
                                           p_day                     IN NUMBER ) RETURN BOOLEAN;
                                           
                                           
    FUNCTION f_validate_attendant_date (p_term_code                  IN general.gtvsdax.gtvsdax_external_code%TYPE,
                                        p_crn                        IN ssbsect.ssbsect_crn%TYPE,
                                        p_attendant_date             IN DATE) RETURN BOOLEAN;
                                        
    FUNCTION f_get_withdraw_date       (p_term_code               IN general.gtvsdax.gtvsdax_external_code%TYPE,
                                        p_crn                     IN sfrstcr.sfrstcr_crn%TYPE,
                                        p_pidm                    IN sfrstca.sfrstca_pidm%TYPE
                                        ) RETURN DATE;  
                                                                          
                                       
    FUNCTION f_is_valid_withdraw_date( p_term_code               IN general.gtvsdax.gtvsdax_external_code%TYPE,
                                       p_crn                     IN sfrstcr.sfrstcr_crn%TYPE,
                                       p_pidm                    IN sfrstca.sfrstca_pidm%TYPE,
                                       p_attendant_date          DATE) RETURN BOOLEAN;  
                                                                           
    FUNCTION f_local_validTerm          (term_in IN VARCHAR2) RETURN BOOLEAN;  
    
    FUNCTION f_retrieve_adm_status      ( p_term_code               IN general.gtvsdax.gtvsdax_external_code%TYPE,
                                          p_pidm                    IN sfrstca.sfrstca_pidm%TYPE,
                                          p_crn                     IN sfrstcr.sfrstcr_crn%TYPE
                                        ) RETURN VARCHAR2 ;    
                                        
    FUNCTION f_is_valid_unscheduled_date   (    p_term_code               IN general.gtvsdax.gtvsdax_external_code%TYPE,
                                               p_crn                     IN sfrstcr.sfrstcr_crn%TYPE,
                                               p_attendant_date          DATE)RETURN BOOLEAN;
                                          
    FUNCTION f_get_course_start_date     (    p_term_code               IN general.gtvsdax.gtvsdax_external_code%TYPE,
                                               p_crn                     IN sfrstcr.sfrstcr_crn%TYPE) RETURN VARCHAR2;  
                                          
    FUNCTION f_get_course_end_date     (    p_term_code               IN general.gtvsdax.gtvsdax_external_code%TYPE,
                                               p_crn                     IN sfrstcr.sfrstcr_crn%TYPE) RETURN VARCHAR2;       
                                               
    FUNCTION f_get_course_title     (    p_term_code               IN general.gtvsdax.gtvsdax_external_code%TYPE,
                                               p_crn                     IN sfrstcr.sfrstcr_crn%TYPE) RETURN VARCHAR2;                                                                                                        
    /*
    FUNCTION f_get_button(cname       in varchar2 DEFAULT NULL,
                    cvalue      in varchar2 character set any_cs DEFAULT 'Button',
                    cattributes in varchar2 DEFAULT NULL) return varchar2 ;
                    
     */
                    
END bwskattendanceapp;
/